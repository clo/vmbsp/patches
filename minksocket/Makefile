ifneq ($(KERNELRELEASE),)

include $(srctree)/techpack/minksocket/config/minkipc.conf

#Rules for libminktransport
ccflags-y := -I$(srctree)/techpack/minksocket/include -DVNDR_VSOCK -D__KERNEL__ -DMEMORY_ALLOC_CONTIGUOUS -Wno-gcc-compat -Wno-error -Wno-declaration-after-statement
ccflags-y += -I$(srctree)/techpack/minksocket/kernel
ccflags-y += -I$(srctree)/techpack/minksocket/sock
ccflags-y += -I$(srctree)/techpack/vmsocket/vmsocket
ccflags-y += -I$(srctree)/techpack/vmosal


minkipc-$(CONFIG_MINKTRANSPORT) += sock/bbuf.o
minkipc-$(CONFIG_MINKTRANSPORT) += sock/minkipc.o
minkipc-$(CONFIG_MINKTRANSPORT) += sock/minksocket.o
minkipc-$(CONFIG_MINKTRANSPORT) += sock/msforwarder.o
minkipc-$(CONFIG_MINKTRANSPORT) += sock/fdwrapper.o
minkipc-$(CONFIG_MINKTRANSPORT) += sock/threadpool.o
#minkipc-$(CONFIG_MINKTRANSPORT) += sock/vm_osal.o

#Rules to make test module
minkipc-$(CONFIG_MINKTRANSPORT_TEST) += sock/minkipc_vsock_q.o

obj-$(CONFIG_MINKTRANSPORT_BUILD) += minkipc.o

else # Userspace make

proj := minkipc
proj-major := 1
proj-minor := 0
proj-version := $(proj-major).$(proj-minor)

CFLAGS := -Wall -g -DVNDR_VSOCK
LDFLAGS := -L. -L../vmsocket -L../vmosal -lvmsocket -lpthread -lvmosal

prefix := /usr/local
bindir := $(prefix)/bin
libdir := $(prefix)/lib
includedir := $(prefix)/include

libminksocket.so-srcs := \
    sock/bbuf.c \
    sock/minkipc.c \
    sock/minksocket.c \
    sock/msforwarder.c \
    sock/fdwrapper.c \
    sock/threadpool.c 
#    sock/vm_osal.c

libminksocket.so-cflags := -fPIC -Iinclude -Ikernel -Isock -I../vmsocket/vmsocket -I../vmosal

targets := libminksocket.so

ifneq ($(CROSS_COMPILE),)
CC := $(CROSS_COMPILE)gcc
endif
SFLAGS := -I$(shell $(CC) -print-file-name=include) -Wno-non-pointer-null


out := out
src_to_obj = $(patsubst %.c,$(out)/obj/%.o,$(1))
src_to_dep = $(patsubst %.c,$(out)/dep/%.d,$(1))

all-srcs :=
all-objs :=
all-deps := vmsocket vmosal
all-clean := $(out)
all-install :=

all: $(targets)

$(out)/obj/%.o: %.c
ifneq ($C,)
	@echo "CHECK	$<"
	@sparse $< $(patsubst -iquote=%,-I%,$(CFLAGS)) $(SFLAGS)
endif
	@echo "CC	$<"
	@$(CC) -MM -MF $(call src_to_dep,$<) -MP -MT "$@ $(call src_to_dep,$<)" $(CFLAGS) $(_CFLAGS) $<
	@$(CC) -o $@ -c $< $(CFLAGS) $(_CFLAGS)

define add-inc-target
$(DESTDIR)$(includedir)/$2: $1/$2
	@echo "INSTALL	$$<"
	@install -D -m 755 $$< $$@

all-install += $(DESTDIR)$(includedir)/$2
endef

define add-target-deps
all-srcs += $($1-srcs)
all-objs += $(call src_to_obj,$($1-srcs))
all-deps += $(call src_to_dep,$($1-srcs))
all-clean += $1
$(call src_to_obj,$($1-srcs)): _CFLAGS := $($1-cflags)
endef

define add-bin-target
$(call add-target-deps,$1)

$1: $(call src_to_obj,$($1-srcs))
	@echo "LD	$$@"
	$$(CC) -o $$@ $$(filter %.o,$$^) -Wl,-rpath=. $(LDFLAGS) $($1-ldflags)

$(DESTDIR)$(bindir)/$1: $1
	@echo "INSTALL	$$<"
	@install -D -m 755 $$< $$@

all-install += $(DESTDIR)$(bindir)/$1
endef

define add-lib-target
$(call add-target-deps,$1)

$1: $(call src_to_obj,$($1-srcs))
	@echo "LD	$$@"
	$$(CC) -o $$@ $$(filter %.o,$$^) $(LDFLAGS) -shared -Wl,-soname,$1

$(DESTDIR)$(libdir)/$1.$(proj-version): $1
	@echo "INSTALL	$$<"
	@install -D -m 755 $$< $$@
	@ln -sf $1.$(proj-version) $(DESTDIR)$(libdir)/$1.$(proj-major)
	@ln -sf $1.$(proj-major) $(DESTDIR)$(libdir)/$1

all-install += $(DESTDIR)$(libdir)/$1.$(proj-version)
endef

$(foreach v,$(filter %.so,$(targets)),$(eval $(call add-lib-target,$v)))
$(foreach v,$(filter-out %.so,$(targets)),$(eval $(call add-bin-target,$v)))

install: $(all-install)

clean:
	@echo CLEAN
	@$(RM) -r $(all-clean)

$(call src_to_obj,$(all-srcs)): Makefile

ifneq ("$(MAKECMDGOALS)","clean")
cmd-goal-1 := $(shell mkdir -p $(sort $(dir $(all-objs) $(all-deps))))
-include $(all-deps)
endif

endif