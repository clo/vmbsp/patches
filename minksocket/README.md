MINKIPC framework ported over the VMSOCKET for OEM delivery under BSD license
Depends on the vmsocket changes to be present at the same hierarchy as this folder.
By default vmalloc APIs are used in linux kernel and to enable kzalloc APIs define the macro for compilation - LINUX_MEMORY_ALLOC_CONTIGUOUS.
Kernel space compilation(__KERNEL__) is enabled by default in Makefile

MINKIPC
Linux Kernel compilation
Clone this project under <src_tree>/techpack as "minksocket"
cd techpack
git clone minksocket.git minksocket
