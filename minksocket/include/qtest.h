// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
// qtest supplies utilities for unit testing.

#ifndef __QTEST_H
#define __QTEST_H

#include "vm_osal.h"


#define qt_fail(xfmt, ...)    LOG("%s" xfmt, __func__, __VA_ARGS__)


#ifdef QTEST_VERBOSE
#  define qt__showab(a, b, fmt)                                 \
  LOG("test A: " fmt "\n     B: " fmt "\n", (a), (b))
#else
#  define qt__showab(a, b, fmt)  0
#endif


#define qt__assert_eq(aa, bb, isEQ, ty, fmt)                      \
  do {                                                          \
    ty _a = (ty) (aa);                                           \
    ty _b = (ty) (bb);                                           \
    (void) qt__showab(_a, _b, fmt);                              \
    if (!isEQ(_a, _b)) {                                        \
      qt_fail("values not equal:\n"                             \
               "A: " fmt "\n"                                   \
               "B: " fmt "\n", _a, _b);                         \
    }                                                           \
  } while (0)


#define qt__isEQ(a, b)  ((a) == (b))


#define qt_assert(x)                                    \
  if (!(x)) {LOG("assertion failed\n");}


#define qt_eqb(a, b)   qt__assert_eq(a, b, qt__isEQ, bool, "%d")

#define qt_eqi(a, b)   qt__assert_eq(a, b, qt__isEQ, int, "%d")

#define qt_equ(a, b)   qt__assert_eq(a, b, qt__isEQ, uint64_t, "%u")

#define qt_eqx(a, b)   qt__assert_eq(a, b, qt__isEQ, uint64_t, "%u" )

#define qt_eqp(a, b)   qt__assert_eq(a, b, qt__isEQ, const void *, "%p")

#define qt_eqfp(a, b)  qt__assert_eq((intptr_t)a, (intptr_t)b, qt__isEQ, intptr_t, "%p" )

#define qt_eqs(a, b)   qt__assert_eq(a, b, !strcmp, const char *, "\"%s\"")


#endif /* __QTEST_H */
