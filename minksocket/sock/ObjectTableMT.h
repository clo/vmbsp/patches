// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause

#ifndef __OBJECTTABLEMT_H
#define __OBJECTTABLEMT_H

#include "object.h"
#include "vm_osal.h"
#include "heap.h"
#include "logging.h"

typedef struct {
  // An array of objects held by a remote domain.
  Object *objects;

  // An array of reference counts managed by the remote domain.
  int32_t *objectRefs;

  // Size of the objects[] and objectRefs[] arrays.
  size_t objectsLen;

  //Mutex
  VM_SOCKET_OSAL_MUTEX mutex;
} ObjectTableMT;

// Add an object to the table, assigning it a handle.  Set its 'ref' value
// to 1.
//
// On success, return the handle.
// On failure, return -1.
//
static inline int ObjectTableMT_addObject(ObjectTableMT *me, Object obj)
{
  int n;
  vm_osal_mutex_lock(&me->mutex);
  for ( n = 0; n < (int) me->objectsLen; ++n) {
    if (Object_isNull(me->objects[n])) {
      me->objectRefs[n] = 1;
      me->objects[n] = obj;
      vm_osal_mutex_unlock(&me->mutex);
      Object_retain(obj);
      return n;
    }
  }
  vm_osal_mutex_unlock(&me->mutex);
  return -1;
}


// Return the kernel object to which an outbound object forwards invokes.
// If there is no object at that slot, return Object_NULL.  Otherwise, the
// returned object has been retained, and the caller is repsonsible for
// releasing it.
//
static inline Object ObjectTableMT_recoverObject(ObjectTableMT *me, int h)
{
  vm_osal_mutex_lock(&me->mutex);
  if (h >= 0 && h < (int) me->objectsLen) {
    Object o = me->objects[h];
    if (!Object_isNull(o)) {
      vm_osal_mutex_unlock(&me->mutex);
      Object_retain(o);
      return o;
    }
  }
  vm_osal_mutex_unlock(&me->mutex);
  return Object_NULL;
}

// Empty the object table entry and release the object.
//
static inline void ObjectTableMT_closeHandle(ObjectTableMT *me, int h)
{
  vm_osal_mutex_lock(&me->mutex);
  if (!Object_isNull(me->objects[h])) {
    Object o = me->objects[h];
    me->objects[h] = Object_NULL;
    vm_osal_mutex_unlock(&me->mutex);
    Object_release(o);
    return;
  }
  else {
    LOG("%s:%u OOPS: h=%d already NULL\n", __func__, __LINE__, h);
  }
  vm_osal_mutex_unlock(&me->mutex);
}


// Increment the count in the references table.
//
static inline void ObjectTableMT_retainHandle(ObjectTableMT *me, int h)
{
  vm_osal_mutex_lock(&me->mutex);
  if (h >= 0 && h < (int) me->objectsLen) {
    ++me->objectRefs[h];
  }
  vm_osal_mutex_unlock(&me->mutex);
}

// Decrement the count in the references table, and release the associated
// object when it reaches zero.
//
static inline void ObjectTableMT_releaseHandle(ObjectTableMT *me, int h)
{
  int ref;
  vm_osal_mutex_lock(&me->mutex);
  if (h >=0 && h < (int) me->objectsLen) {
      ref = --me->objectRefs[h];
      if (ref == 0) {
        vm_osal_mutex_unlock(&me->mutex);
        ObjectTableMT_closeHandle(me, h);
        return;
      }
      if (ref < 0) {
        LOG("%s:%u OOPS: h=%d ref count: %d\n", __func__, __LINE__, h, ref);
      }
  }
  vm_osal_mutex_unlock(&me->mutex);
}


static inline void ObjectTableMT_closeAllHandles(ObjectTableMT *me)
{
  int h;
  vm_osal_mutex_lock(&me->mutex);
  for (h = 0; h < (int) me->objectsLen; ++h) {
    // _closeHandle
    if (!Object_isNull(me->objects[h])) {
      Object o = me->objects[h];
      me->objects[h].invoke = NULL;
      Object_release(o);
    }
  }
  me->objectsLen = 0;
  vm_osal_mutex_unlock(&me->mutex);
}

static inline void ObjectTableMT_destruct(ObjectTableMT *me)
{
  for (size_t h = 0; h < me->objectsLen; h++) {
    if (!Object_isNull(me->objects[h])) {
      LOG("%s:%u WARN: possible object leak, [%zu] refs=%d\n", __func__, __LINE__, h, me->objectRefs[h]);
    }
  }
  ObjectTableMT_closeAllHandles(me);
  HEAP_FREE_PTR(me->objects);
  HEAP_FREE_PTR(me->objectRefs);
  vm_osal_mutex_deinit(&me->mutex);
}


static inline int ObjectTableMT_construct(ObjectTableMT *me, uint32_t size)
{
  me->objects = HEAP_ZALLOC_ARRAY(Object, size);
  me->objectRefs = HEAP_ZALLOC_ARRAY(int32_t, size);
  if (me->objects == NULL || me->objectRefs == NULL) {
    me->objectsLen = 0;
    return Object_ERROR;
  }
  me->objectsLen = size;
  vm_osal_mutex_init(&me->mutex, NULL);
  return Object_OK;
}


#endif // __OBJECTTABLEMT_H
