// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#ifndef __FDWRAPPER_H
#define __FDWRAPPER_H

#if defined (__cplusplus)
extern "C" {
#endif

typedef struct FdWrapper {
  int refs;
  int handle;
  int descriptor;
  Object dependency;
} FdWrapper;

Object FdWrapper_new(int fd);
FdWrapper *FdWrapperFromObject(Object obj);

#if defined (__cplusplus)
}
#endif

#endif // __FdWrapper_H