// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#ifndef __HEAP_PORT_H
#define __HEAP_PORT_H

#include "vm_osal.h"

#if defined (__cplusplus)
extern "C" {
#endif

#if defined(__KERNEL__)

#if defined(MEMORY_ALLOC_CONTIGUOUS)
#define heap_zalloc(x)   kzalloc(x, GFP_KERNEL)
#define heap_malloc(x)   kmalloc(x, GFP_KERNEL)
#define heap_calloc(x,y) kcalloc(x, y, GFP_KERNEL)
#define heap_free(x)     kfree(x)
#else  //MEMORY_ALLOC_CONTIGUOUS
#define heap_zalloc(x)   vzalloc(x)
#define heap_malloc(x)   vmalloc(x)
#define heap_calloc(x,y) vzalloc(x*y)
#define heap_free(x)     vfree(x)
#endif //MEMORY_ALLOC_CONTIGUOUS

#else  //__KERNEL__

static inline void *zalloc(size_t size) {
  void *ptr = malloc(size);
  if (ptr) {
    memset(ptr, '\0', size);
  }
  return ptr;
}

#define heap_zalloc(x)           zalloc(x)
#define heap_calloc(num, size)   calloc(num, size)
#define heap_malloc(size)        malloc(size)
#define heap_free(x)             free(x)
#define heap_memdup(ptr, size)   memdup(ptr, size)

#endif //__KERNEL__

#if defined (__cplusplus)
}
#endif

#endif // __HEAP_PORT_H

