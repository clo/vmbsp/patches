// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause

#ifndef __LOGGING_H
#define __LOGGING_H

#include "vm_osal.h"

#ifdef __KERNEL__
#define LOG(...)   printk(__VA_ARGS__)
#define LOGE(...)   pr_err(__VA_ARGS__)
#define LOGD(...)   printk(__VA_ARGS__)
#define VM_OSAL_PRINT_DEBUG pr_err
#define VM_OSAL_PRINT_ERROR pr_err
#define VM_OSAL_PRINT_TX pr_err
#define VM_OSAL_PRINT_RX pr_err

#else //__KERNEL__
#define LOG(...)   printf(__VA_ARGS__)
#define LOGE(...)   printf(__VA_ARGS__)
#define LOGD(...)   printf(__VA_ARGS__)

#define VM_OSAL_PRINT_DEBUG LOGD
#define VM_OSAL_PRINT_ERROR LOGE
#define VM_OSAL_PRINT_TX LOGD
#define VM_OSAL_PRINT_RX LOGD
#endif // __KERNEL__

#endif // __LOGGING_H


