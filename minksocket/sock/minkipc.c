// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#include "vm_osal.h"
#include "vm_socket.h"
#include "qlist.h"
#include "check.h"
#include "minkipc.h"
#include "minksocket.h"
#include "msforwarder.h"
#include "fdwrapper.h"
#include "logging.h"
#include "heap.h"

#define MAX_QUEUE_LENGTH 5
#define CID_PEER         3
#define CHECK_CLEAN(expr) \
  do { if (!(expr)) { CHECK_LOG(); goto cleanup; } } while (0)

static inline int __atomic_add(int *pn, int n) {
  return __sync_add_and_fetch(pn, n);  // GCC builtin
}

typedef union {
    struct vm_sockaddr vaddr;
} sockaddr_t;

struct MinkIPC {
  int refs;
  bool bServer;
  bool bReady;
  sockaddr_t sockaddr;
  int sock;
  bool bDone;
  bool bServerDone;
  Object opener;
  VM_SOCKET_OSAL_THREAD listenerThread;
  MinkSocket *conn;
  QList servedConnectionsList;
  VM_SOCKET_OSAL_MUTEX mutex;
  VM_SOCKET_OSAL_SIG cond;
};

typedef struct {
  QNode qn;
  MinkSocket *conn;
  uint32_t node; // In case of Vsock node is same as cid
  uint32_t port;
} ServerNode;

uintptr_t gMinkPeerUIDTLSKey = (uintptr_t) MinkIPC_startService;
uintptr_t gMinkPeerGIDTLSKey = (uintptr_t) MinkIPC_startServiceOnSocket;

static void MinikIPC_cleanupConnections(MinkIPC *me, bool deadOnly)
{
  QNode * pqn = NULL;
  QNode * pqn_next = NULL;
  QLIST_NEXTSAFE_FOR_ALL(&me->servedConnectionsList, pqn, pqn_next) {
    ServerNode* node = container_of(pqn, ServerNode, qn);
    if (node) {
      if (!MinkSocket_isConnected(node->conn) || !deadOnly) {
        QNode_dequeue(pqn);
        MinkSocket_close(node->conn, Object_ERROR_UNAVAIL);
        MinkSocket_release(node->conn);
        heap_free(node);
      }
    }
  }
}

#if defined(VNDR_VSOCK)
static ServerNode *ServerNode_remote_new(Object opener, uint32_t node, uint32_t port)
{
  MinkSocket *sock = NULL;
  ServerNode *connection = (ServerNode *)HEAP_ZALLOC_REC(ServerNode);
  if (!connection) {
    return NULL;
  }

  sock = MinkSocket_remote_new(opener, -1, node, port);
  if (!sock) {
    heap_free(connection);
    return NULL;
  }

  connection->conn = sock;
  connection->node = node;
  connection->port= port;
  return connection;
}

static ServerNode *get_server_node(MinkIPC *me, uint32_t c_node, uint32_t c_port, bool noCreate)
{
  QNode * pqn = NULL;
  ServerNode* node = NULL;
  QLIST_FOR_ALL(&me->servedConnectionsList, pqn) {
    node = container_of(pqn, ServerNode, qn);
    if (node) {
        if (node->node == c_node && node->port == c_port) {
        return node;
      }
    }
  }

  /* this will be set when client has been killed and we need to do cleanup */
  if (noCreate)
    return NULL;

  node = ServerNode_remote_new(me->opener, c_node, c_port);
  if (!node) {
    return node;
  }
  QList_appendNode(&me->servedConnectionsList, &node->qn);
  return node;
}

static void *MinkIPC_remote_Service(void *pv)
{
  MinkSocket *tsock;
  char *buf = NULL;
  uint32_t tmaxpayload = 0; 
  sockaddr_t sq/* sq_server*/;
  socklen_t sl; 
  int len;
  MinkIPC *me = (MinkIPC *)pv;

  me->bReady = true;
  vm_osal_sig_set(&me->cond);

  tmaxpayload = MAX_VSOCK_PAYLOAD;  
  buf = (char *)heap_malloc(tmaxpayload);
  if (!buf) {
    LOGE("Mem allocation failure for UDP packet\n");
    return NULL;
  }

  if (fetch_socket_type(me->sock) == VSOCK) {
      sl = sizeof(struct vm_sockaddr);
  }
  else {
    LOGE("Socket type unknown, Client thread terminated\n");
      return NULL;
  }

  do {
    len = vm_recvfrom(me->sock, (void*)buf, tmaxpayload, 0, (void*)&sq, &sl);
    if (len < 0) {
      heap_free(buf);
      return NULL;
    }

    tsock = me->conn;
    if (me->bServer) {
      ServerNode *node = get_server_node(me, sq.vaddr.vm_port.node_id, sq.vaddr.vm_port.port_id, false);
      if (!node) {
        heap_free(buf);
        return NULL;
      }
      tsock = node->conn;
    }
    process_message(tsock, me->sock, buf, len);
  } while(!me->bDone);
  MinikIPC_cleanupConnections(me, true);
  heap_free(buf);

  me->bServerDone = true;
  vm_osal_sig_set(&me->cond);
  return NULL;
}
#else
static void *MinkIPC_remote_Service(void *pv)
{
  return NULL;
}
#endif
   
#if defined(VNDR_VSOCK)
int MinkIPC_vsock_new(MinkIPC *me, int service, int sock)
{
    int fd, ret;
    struct vm_sockaddr addr;

    fd = vm_socket(AF_VSOCK, SOCK_DGRAM, 0); 
    if (fd < 0) {
        LOGE("%s: vsock_open failed:\n", __func__);
        return -1;
    }
    me->sockaddr.vaddr.svm_family = AF_VSOCK;
    me->sockaddr.vaddr.vm_port.node_id = CID_PEER; // Not used by underlying QCOM transport layer
    me->sockaddr.vaddr.vm_port.port_id = service; // Is service can be used for port on client or hardcoded?

    // TODO : underlying transport layer has to bind the port details and this has to be removed
    addr.svm_family = AF_VSOCK;
    addr.vm_port.node_id = VMADDR_CID_LOCAL;
    addr.vm_port.port_id = VM_SOCKET_MIN_PORT_ID;
    ret = vm_bind(fd, (struct sockaddr *)&addr, sizeof(addr));
    if (ret) {
        LOGE("Failed to bind to addr:[0x%x:0x%x]\n", addr.vm_port.node_id, addr.vm_port.port_id);
        return -1;
    }

    me->sock = fd; 
    return 0;
}
#else
int MinkIPC_vsock_new(MinkIPC *me, int service, int sock)
{
   return 0;
}
#endif

#if defined(VNDR_VSOCK)
static MinkIPC *
MinkIPC_remote_new(int service, int sock, Object opener, int sock_type)
{
  MinkIPC *me = (MinkIPC *)HEAP_ZALLOC_REC(MinkIPC);
  int ret  = -1;
  if (!me) {
    return NULL;
  }

  me->refs = 1;
  me->bDone = false;
  me->conn = NULL;

  if (sock < 0)
  {
    if (sock_type == VSOCK)
    {
        ret = MinkIPC_vsock_new(me, service, sock);
    }
    if (ret != 0)
    {
      goto cleanup;
    }
  }
  else
  {
    me->sock = sock;
  }
  QList_construct(&me->servedConnectionsList);
  Object_ASSIGN(me->opener, opener);
  me->bServer =  !(Object_isNull(opener));
  CHECK_CLEAN(me->sock != -1);
  CHECK_CLEAN(!vm_osal_mutex_init(&me->mutex, NULL));
  CHECK_CLEAN(!vm_osal_sig_init(&me->cond, NULL));

  return me;

cleanup:
  LOGE("%s: release minkIPC = %p\n", __func__, me);
  MinkIPC_release(me);
  heap_free(me);
  return NULL;
}
#else
static MinkIPC *
MinkIPC_remote_new(int service, int sock, Object opener, int sock_type)
{
  return NULL;
}

#endif


static MinkIPC *MinkIPC_beginService(const char *service, int sock, Object opener)
{
  MinkIPC *me = NULL;
  struct vm_sockaddr svm = {
       .svm_family = AF_VSOCK,
       .vm_port.port_id = VOSK_SERVER_PORT_NO,
       .vm_port.node_id = VMADDR_CID_LOCAL,
  };
  char threadname[32] = {0};

  if (fetch_socket_type(sock) == VSOCK) {
    me = MinkIPC_remote_new(0, sock, opener, VSOCK);
  } else {
    LOGE ("%s: Invalid SOCK Type passed\n", __func__);
    return NULL;
  }

  if (!me) {
    LOGE ("%s: Failed to create new MinkIPC...returning NULL\n", __func__);
    return NULL;
  }

  vm_osal_create_TLS_key((VM_KEY_TYPE*) &gMinkPeerUIDTLSKey, NULL);
  vm_osal_create_TLS_key((VM_KEY_TYPE*) &gMinkPeerGIDTLSKey, NULL);

  CHECK_CLEAN (!vm_bind(me->sock, (struct sockaddr*)&svm,
                   sizeof(svm)));

  sprintf(threadname,"ServerThread%d",sock);
  CHECK_CLEAN (!vm_osal_thread_create(&me->listenerThread, MinkIPC_remote_Service, me, threadname));

  vm_osal_mutex_lock(&me->mutex);
  while (!me->bReady) {
    vm_osal_sig_wait((void*)&me->cond, (void*)&me->mutex, (int *)&me->bReady);
  }
  vm_osal_mutex_unlock(&me->mutex);
  return me;

 cleanup:
  MinkIPC_release(me);
  return NULL;
}

MinkIPC *MinkIPC_startService(const char *service, Object opener)
{
  return MinkIPC_beginService(service, -1, opener);
}

MinkIPC * MinkIPC_startServiceOnSocket(int sock, Object opener)
{
  return MinkIPC_beginService(NULL, sock, opener);
}

/**
   wait for the service to finish ..
   waits until stopped or the service dies
**/
void MinkIPC_join(MinkIPC *me) {
  if (me->bServer && me->listenerThread) {
    //wait for thread to die
    vm_osal_check_for_completion((void *)me->listenerThread);
  }
}


MinkIPC* MinkIPC_connect_remote(int service, Object *obj, int sock_type)
{
  struct sockaddr *paddr = NULL;
  uint32_t size = 0, node = 0, port = 0;
  char threadname[32] = {0};
  MinkIPC *me = MinkIPC_remote_new(service, -1, Object_NULL, sock_type);
  if (!me) {
    return NULL;
  }

  if (fetch_socket_type(me->sock) == VSOCK)
  {
    paddr = (struct sockaddr *)&me->sockaddr.vaddr;
    size = sizeof(me->sockaddr.vaddr);
    node = me->sockaddr.vaddr.vm_port.node_id;
    port = me->sockaddr.vaddr.vm_port.port_id;
  }
  else
  {
    LOGE("%s: Socket type Invalid:0x%x\n", __func__, me->sock);
      goto cleanup;
  }
  
  if (vm_connect(me->sock, paddr,size) == -1) {
    LOGE("vm_connected failed for Node : 0x%x Port:0x%x\n", node, port);
    MinkIPC_release(me);
    return NULL;
  }

  //create a domain
  me->conn = MinkSocket_remote_new(Object_NULL, me->sock, node, port);
  if (me->conn) {
    sprintf(threadname,"ClientThread%d",me->sock);
    CHECK_CLEAN (!vm_osal_thread_create(&me->listenerThread, MinkIPC_remote_Service, me, threadname));
    *obj = MSForwarder_new(me->conn, 0);
    vm_osal_mutex_lock(&me->mutex);
    while (!me->bReady) {
      vm_osal_sig_wait(&me->cond, &me->mutex, (int *)&me->bReady);
    }
    vm_osal_mutex_unlock(&me->mutex);
  }
  return me;
cleanup:
  LOGE("%s: ERROR releasing minkIPC\n", __func__);
  MinkIPC_release(me);
  return NULL;
}

#if defined(VNDR_VSOCK) 
MinkIPC* MinkIPC_connect_vsock(int serviceId, Object *obj)
{
  return MinkIPC_connect_remote(serviceId,obj, VSOCK);
}
#else
MinkIPC* MinkIPC_connect_vsock(int service, Object *obj)
{
  return NULL;
}
#endif

int MinkIPC_getClientInfo(uid_t* uid, gid_t* gid)
{
  gid_t* pgid = (gid_t*) vm_osal_retrieve_TLS_key((VM_KEY_TYPE)gMinkPeerGIDTLSKey);
  uid_t* puid = (uid_t*) vm_osal_retrieve_TLS_key((VM_KEY_TYPE)gMinkPeerUIDTLSKey);

  if (pgid == NULL || puid == NULL)
    return -1;

  *uid = *puid;
  *gid = *pgid;

  return 0;
}

static void MinkIPC_stop(MinkIPC *me)
{
  vm_osal_mutex_lock(&me->mutex);
  Object_ASSIGN_NULL(me->opener);
  me->bDone = true;
  MinikIPC_cleanupConnections(me, false);
  if (me->sock != -1) {
    if (VSOCK == fetch_socket_type(me->sock)) {
      vm_close(me->sock);
      me->sock = -1;
    }
  }
  if (me->conn) {
    MinkSocket_release(me->conn);
    me->conn = NULL;
  }
  vm_osal_mutex_unlock(&me->mutex);
  if (me->listenerThread) {
    if (me->bServer)  {
      //wait for thread to die
      vm_osal_mutex_lock(&me->mutex);
      while (!me->bServerDone) {
        vm_osal_sig_wait((void*)&me->cond, (void*)&me->mutex, (int *)&me->bServerDone);
      }
      vm_osal_mutex_unlock(&me->mutex);
    } else {
      //wait for thread to die
      vm_osal_check_for_completion((void*)me->listenerThread);
    }
  }
}

void MinkIPC_retain(MinkIPC *me)
{
  __atomic_add(&me->refs, 1);
}

void MinkIPC_release(MinkIPC *me)
{
  if (__atomic_add(&me->refs, -1) == 0) {
    MinkIPC_stop(me);
    vm_osal_mutex_deinit(&me->mutex);
    vm_osal_sig_deinit(&me->cond);
    heap_free(me);
  }
}

void MinkIPC_wrapFd(int fd, Object *obj) {
  *obj = FdWrapper_new(fd);
}
