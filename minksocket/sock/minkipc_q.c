// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#include "minkipc_q.h"

int main(void)
{
  Object opener = Object_NULL;
  Object opener_svc = Opener_new();
  MinkIPC *server = MinkIPC_startService("/tmp/testfile", opener_svc);
  MinkIPC *client = MinkIPC_connect("/tmp/testfile", &opener);

  //Shall get OO from server
  Object remoteFoo;
  qt_eqi(0, IOpener_open(opener, 0, &remoteFoo));
  qt_remote(remoteFoo);

  //Shall get BO from server
  uint32_t id=0;
  Foo_getId(remoteFoo, &id);
  qt_eqi(FOO_MAGIC_ID, id);

  //Shall send BO and get back as BI
  Foo_setId(remoteFoo, 93499);
  Foo_getId(remoteFoo, &id);
  qt_eqi(93499, id);

  // Shall send OI (local) and get back as OO
  Object localFoo = Foo_new();
  qt_local(localFoo);
  qt_eqi(0, Foo_setObject(remoteFoo, localFoo));
  Object localFoo2 = Object_NULL;
  qt_eqi(0, Foo_getObject(remoteFoo, &localFoo2));
  qt_local(localFoo2);
  Object_release(localFoo2);

  localFoo2 = Object_NULL;
  qt_eqi(0, Foo_getObject(remoteFoo, &localFoo2));
  qt_local(localFoo2);

  Foo_setId(localFoo2, 923);
  Foo_getId(localFoo2, &id);
  qt_eqi(923, id);
  Object_release(localFoo2);

  localFoo2 = Object_NULL;
  qt_eqi(0, Foo_getObject(remoteFoo, &localFoo2));
  qt_local(localFoo2);

  Foo_setId(localFoo2, 923);
  Foo_getId(localFoo2, &id);
  qt_eqi(923, id);
  Object_release(localFoo2);

  //Shall send OI (remote) and get back as OO
  qt_eqi(0, Foo_setObject(remoteFoo, remoteFoo));
  Object remoteFoo2 = Object_NULL;
  qt_eqi(0, Foo_getObject(remoteFoo, &remoteFoo2));
  qt_remote(remoteFoo2);
  Foo_setId(remoteFoo2, 923);
  Foo_getId(remoteFoo2, &id);
  qt_eqi(923, id);
  Object_release(remoteFoo2);
  remoteFoo2 = Object_NULL;

  qt_eqi(0, Foo_getObject(remoteFoo, &remoteFoo2));
  qt_remote(remoteFoo2);
  Object_release(remoteFoo2);

  //Shall send OI (local Fd) and get back as OO
  Object localFd = Foo_newFd();
  qt_fd(localFd);
  qt_eqi(0, Foo_setObject(remoteFoo, localFd));
  int ix;
  qt_eqi(Object_OK, Object_unwrapFd(localFd, &ix));
  qt_assert(ix != -1);
  Object_release(localFd);
  qt_assert(fcntl(ix, F_GETFD) == -1);
  ix = -1;
  localFd = Object_NULL;
  qt_eqi(0, Foo_getObject(remoteFoo, &localFd));
  qt_eqi(Object_OK, Object_unwrapFd(localFd, &ix));
  qt_assert(ix > 0);
  qt_assert(fcntl(ix, F_GETFD) != -1);
  qt_fd(localFd);
  Object_release(localFd);

  ix = -1;
  localFd = Object_NULL;
  qt_eqi(0, Foo_getObject(remoteFoo, &localFd));
  qt_eqi(Object_OK, Object_unwrapFd(localFd, &ix));
  qt_assert(ix > 0);
  qt_assert(fcntl(ix, F_GETFD) != -1);
  qt_fd(localFd);

  Object_release(localFd);

  //Max arg test make sure 60 args over minksocket works
  ObjectArg maxargs[LXCOM_MAX_ARGS];
  ObjectCounts counts = ObjectCounts_pack(15,15,15,15);
  char outbuf[15][4];
  Object testObj = Foo_new();
  memset(outbuf, 0, sizeof(outbuf));
  memset(maxargs, 0, sizeof(maxargs));

  FOR_ARGS(i, counts, BI) {
    maxargs[i].bi = (ObjectBufIn) { TEST_BUFFER, 4 };
  }
  FOR_ARGS(i, counts, BO) {
    maxargs[i].b = (ObjectBuf) { outbuf[i - ObjectCounts_numBI(counts)], 4 };
  }
  FOR_ARGS(i, counts, OI) {
    maxargs[i].o = testObj;
  }
  FOR_ARGS(i, counts, OO) {
    maxargs[i].o = Object_NULL;
  }

  qt_eqi(0, Object_invoke(remoteFoo, Foo_OP_maxArgs, maxargs, counts));

  FOR_ARGS(i, counts, BO) {
    qt_eqi(0, memcmp(maxargs[i].b.ptr,
                     maxargs[i - ObjectCounts_numBI(counts)].b.ptr,
                     maxargs[i].b.size));
  }
  FOR_ARGS(i, counts, OO) {
    qt_assert(!Object_isNull(maxargs[i].o));
    qt_assert(maxargs[i].o.invoke == Foo_invoke);
    Object_release(testObj);
  }
  Object_release(testObj);

  //Test that NULL out objs preserve their order in the
  //out objs list
  Object nullOut = Foo_new();
  Object backup  = nullOut;
  Object realObj = Object_NULL;
  qt_eqi(0, Foo_ObjectONull(remoteFoo, &realObj, &nullOut));
  qt_assert(Object_isNull(nullOut));
  Object_release(backup);
  Object_release(realObj);



  // Create a 1byte and 4byte BIs and make sure
  // the received side buffer is aligned correctly
  uint32_t pval = 0;
  qt_eqi(0, Foo_unalignedSet(remoteFoo, &pval));
  qt_eqi(0, pval%4);

  //Shall roundtrip BI/BO upto 512K
  {
    ObjectArg a[2];

    for (int i=1; i<=16; i++) {
      setupArgs(a, 1<<i, i);
      qt_eqi(0, Object_invoke(remoteFoo, Foo_OP_echo,
                              a, ObjectCounts_pack(1,1,0,0)));
      qt_eqi(0, memcmp(a[0].bi.ptr, a[1].b.ptr, a[0].bi.size));

      setupArgs(a, MAX_BUF_SIZE, 1);
      qt_eqi(0, Object_invoke(remoteFoo, Foo_OP_echo,
                                a, ObjectCounts_pack(1,1,0,0)));
      qt_eqi(0, memcmp(a[0].bi.ptr, a[1].b.ptr, a[0].bi.size));
    }
  }

  //nullptr for a nonzero buffer size is not a valid usecase
  qt_eqi(Object_ERROR_INVALID, Foo_bufferONull(remoteFoo, 4));
  //expected size of 0 and nullptr is a valid usecase
  qt_eqi(0, Foo_bufferONull(remoteFoo, 0));

  //Release objects
  Object_release(localFoo);
  Object_release(remoteFoo);
  Object_release(opener);
  Object_release(opener_svc);

  MinkIPC_release(client);
  MinkIPC_release(server);

  return 0;
}
