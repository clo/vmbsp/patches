// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#ifndef _MINKIPC_Q_H_
#define _MINKIPC_Q_H_

#include "vm_osal.h"
#include "vm_socket.h"
#include "qlist.h"
#include "check.h"
#include "minkipc.h"
#include "minksocket.h"
#include "msforwarder.h"
#include "fdwrapper.h"
#include "logging.h"
#include "heap.h"

/*********************************************************************
 * MACRO DEFINITIONS                                                 *
 *********************************************************************/
#define FOR_ARGS(ndxvar, counts, section)                       \
  for (size_t ndxvar = ObjectCounts_index##section(counts);     \
       ndxvar < (ObjectCounts_index##section(counts)            \
                 + ObjectCounts_num##section(counts));          \
       ++ndxvar)

#define IOpener_OP_open 0
#define IID_Foo 0

#define Foo_OP_noop 0
#define Foo_OP_getId 1
#define Foo_OP_setId 2
#define Foo_OP_getObject 3
#define Foo_OP_setObject 4
#define Foo_OP_unalignedSet 5
#define Foo_OP_echo 6
#define Foo_OP_bufferONull 7
#define Foo_OP_maxArgs 8
#define Foo_OP_ObjectONull 9

#define FOO_MAGIC_ID 894848
#define TEST_BUFFER "TEST"
#define MAXLINE 1024

#define qt_local(o) qt_assert(MSForwarderFromObject(o) == NULL)
#define qt_remote(o) qt_assert(MSForwarderFromObject(o) != NULL)
#define qt_fd(o) qt_assert(FdWrapperFromObject(o) != NULL)

/*********************************************************************
 * TYPE DEFINITIONS                                                  *
 *********************************************************************/
typedef struct {
  int refs;
} Opener;

typedef struct {
  int refs;
  int id;
  Object obj;
} Foo;

/*********************************************************************
 * FUNCTION DECLARATIONS                                             *
 *********************************************************************/

#define MAX_BUF_SIZE 1024*512
static char req[MAX_BUF_SIZE];
static char resp[MAX_BUF_SIZE];

static inline Object Foo_new(void);

static inline uint32_t readNum(const void *p) {
  return *((const uint32_t *)p);
}

static inline void writeNum(void *p, uint32_t i) {
  *((uint32_t *)p) = i;
}

static inline int __atomic_add(int *pn, int n) {
  return __sync_add_and_fetch(pn, n);  // GCC builtin
}


static int32_t IOpener_invoke(ObjectCxt cxt, ObjectOp op,
                              ObjectArg *args, ObjectCounts k)
{
  Opener *me = (Opener *)cxt;

  switch (ObjectOp_methodID(op)) {
  case Object_OP_retain:
    __atomic_add(&me->refs, 1);
    return Object_OK;

  case Object_OP_release:
    if (__atomic_add(&me->refs, -1) == 0) {
      heap_free(me);
    }
    return Object_OK;

  case IOpener_OP_open:
    {
      int guid = readNum(args[0].bi.ptr);
      if (guid == IID_Foo) {
        args[1].o = Foo_new();
      } else {
        return Object_ERROR_BADOBJ;
      }
      break;
    }
    case Object_OP_unwrapFd:
      if (k != ObjectCounts_pack(0, 1, 0, 0)) {
        break;
      }
      int fd = -1;
      memscpy(args[0].b.ptr, args[0].b.size, &fd, sizeof(fd));
      return Object_OK;
  }
  return Object_OK;
}

static inline Object Opener_new(void)
{
  Opener *me = heap_zalloc(sizeof(Opener));
  me->refs = 1;
  return (Object){ IOpener_invoke, me};
}

static int32_t IOpener_open(Object me, int openid, Object *po)
{
  ObjectArg a[2];
  a[0].bi = (ObjectBufIn) { &openid, sizeof(int32_t) };

  int32_t res = Object_invoke(me, IOpener_OP_open, a,
                              ObjectCounts_pack(1,0,0,1));
  *po = a[1].o;
  return res;
}

static int32_t Foo_invoke(ObjectCxt cxt, ObjectOp op,
                          ObjectArg *args, ObjectCounts k)
{
  Foo *me = (Foo *)cxt;

  switch (ObjectOp_methodID(op)) {
  case Object_OP_retain:
    __atomic_add(&me->refs, 1);
    return Object_OK;

  case Object_OP_release:
    if (__atomic_add(&me->refs, -1) == 0) {
      Object_ASSIGN_NULL(me->obj);
      heap_free(me);
    }
    return Object_OK;

  case Object_OP_unwrapFd:
    if (k != ObjectCounts_pack(0, 1, 0, 0)) {
      break;
    }
    int fd = -1;
    memscpy(args[0].b.ptr, args[0].b.size, &fd, sizeof(fd));
    return Object_OK;

  case Foo_OP_getId:
    writeNum(args[0].b.ptr, me->id);
    break;

  case Foo_OP_setId:
    me->id = readNum(args[0].bi.ptr);
    break;

  case Foo_OP_getObject:
    args[0].o = me->obj;
    Object_retain(me->obj);
    break;

  case Foo_OP_setObject:
    Object_replace(&me->obj, args[0].o);
    break;

  case Foo_OP_unalignedSet:
    writeNum(args[2].b.ptr, (uint32_t)(uintptr_t)args[1].bi.ptr);
    break;

  case Foo_OP_echo:
    memcpy(args[1].b.ptr, args[0].b.ptr, args[1].b.size);
    break;

  case Foo_OP_bufferONull:
    args[0].b = (ObjectBuf) { TEST_BUFFER, args[0].b.size };
    break;

  case Foo_OP_maxArgs:
    FOR_ARGS(i, k, BO) {
      ObjectBuf* src = &args[i - ObjectCounts_numBI(k)].b;
      memscpy(args[i].b.ptr, args[i].b.size, src->ptr, src->size);
    }
    FOR_ARGS(j, k, OO) {
      Object_replace(&args[j].o, args[j - ObjectCounts_numOI(k)].o);
      args[j].o = args[j - ObjectCounts_numOI(k)].o;
    }
    break;

  case Foo_OP_ObjectONull:
    args[0].o = Object_NULL;
    args[1].o = Foo_new();
    break;
  }

  return 0;
}

static inline Object Foo_new(void)
{
  Foo *me = heap_zalloc(sizeof(Foo));
  me->refs = 1;
  me->id = FOO_MAGIC_ID;
  return (Object){ Foo_invoke, me};
}

static inline int Foo_getId(Object me, uint32_t *pid)
{
  ObjectArg a[1];
  a[0].b = (ObjectBuf) { pid, sizeof(uint32_t) };
  return Object_invoke(me, Foo_OP_getId, a, ObjectCounts_pack(0,1,0,0));
}

static inline int Foo_setId(Object me, uint32_t id)
{
  ObjectArg a[1];
  a[0].bi = (ObjectBufIn) { &id, sizeof(uint32_t) };
  return Object_invoke(me, Foo_OP_setId, a, ObjectCounts_pack(1,0,0,0));
}

static inline int Foo_setObject(Object me, Object obj)
{
  ObjectArg a[1];
  a[0].o = obj;
  return Object_invoke(me, Foo_OP_setObject, a, ObjectCounts_pack(0,0,1,0));
}

static inline int Foo_getObject(Object me, Object *obj)
{
  ObjectArg a[1];
  int32_t err = Object_invoke(me, Foo_OP_getObject, a, ObjectCounts_pack(0,0,0,1));
  *obj = a[0].o;
  return err;
}

static inline int Foo_unalignedSet(Object me, uint32_t *pid)
{
  ObjectArg a[3];
  uint32_t id = FOO_MAGIC_ID;
  a[0].bi = (ObjectBufIn) { &id, 1 };
  a[1].bi = (ObjectBufIn) { &id, sizeof(uint32_t) };
  a[2].b = (ObjectBuf) { pid, sizeof(uint32_t) };
  return Object_invoke(me, Foo_OP_unalignedSet, a, ObjectCounts_pack(2,1,0,0));
}

static inline int Foo_bufferONull(Object me, uint32_t size)
{
  ObjectArg a[1];
  a[0].b = (ObjectBuf) { NULL, size * 1 };
  return Object_invoke(me, Foo_OP_bufferONull, a, ObjectCounts_pack(0,1,0,0));
}

static inline int Foo_ObjectONull(Object me, Object* obj_real, Object* obj_null)
{
  ObjectArg a[2];
  int ret_val = Object_invoke(me, Foo_OP_ObjectONull, a, ObjectCounts_pack(0,0,0,2));
  *obj_null = a[0].o;
  *obj_real = a[1].o;
  return ret_val;
}

static inline void setupArgs(ObjectArg* args, size_t size, int init_val)
{

  args[0].bi.ptr = req;
  args[0].bi.size = size;
  memset(req, init_val, args[0].bi.size);
  args[1].b.ptr = resp;
  args[1].b.size = args[0].bi.size;
  memset(resp, 0, args[0].bi.size);

}

#endif // _MINKIPC_Q_H_