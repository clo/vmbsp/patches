// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#include "vm_osal.h"
#include "minkipc.h"
#include "msforwarder.h"
#include "fdwrapper.h"
#include "lxcom_sock.h"
#include "memscpy.h"
#include "qtest.h"
#include "minkipc_q.h"
//#include <linux/vm_sockets.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/mutex.h>
#include <linux/platform_device.h>
#include <linux/mod_devicetable.h>

/** Server and client speaks to same port */
#define PORT     0x5555
#define VSOCK_TEST_CLIENT 1
#define VSOCK_TEST_SERVER 1
#define DEBUG_LOG pr_err

#if defined(VSOCK_TEST_SERVER)
int server_main(int a)
{
  int serv_fd;

  // Creating socket file descriptor
  if ((serv_fd = vm_socket(AF_VSOCK, SOCK_DGRAM, 0)) < 0) {
    LOGE("socket creation failed");
    return -1;
  }

  struct vm_sockaddr addr;
  addr.svm_family = AF_VSOCK;
  addr.vm_port.node_id = VMADDR_CID_LOCAL;
  addr.vm_port.port_id = VM_SOCKET_MIN_PORT_ID;
  if (0 != vm_bind(serv_fd, (struct sockaddr*)&addr,
      sizeof(addr))) {
    LOGE("Sock Bind failed");
    return -1;
  }

  Object opener_svc = Opener_new();

  MinkIPC *server = MinkIPC_startServiceOnSocket(serv_fd, opener_svc);

  if (server != NULL)
  {
    LOGE(" MINK Server started on Socket: fd: %d\n", serv_fd);
    MinkIPC_join(server);
    MinkIPC_release(server);
  }
  else
  {
    LOGE(" MINK Server Failed on Socket: fd: %d\n", serv_fd);
  }

  return 0;
}
#endif // VSOCK_TEST_SERVER

#if defined(VSOCK_TEST_CLIENT)
// Driver code
int client_main(int b)
{
  int i;
  Object opener = Object_NULL;
	
  DEBUG_LOG(" MINK Client before invoking connect \n");
  MinkIPC *client = MinkIPC_connect_vsock(PORT, &opener);
  
  //Shall get OO from server
  Object remoteFoo;
  DEBUG_LOG(" MINK Client IOpener_open \n");
  qt_eqi(0, IOpener_open(opener, 0, &remoteFoo));
  qt_remote(remoteFoo);

  //Shall get BO from server
  uint32_t id=0;
  DEBUG_LOG(" MINK Client Get ID from server (BO) \n");
  Foo_getId(remoteFoo, &id);
  qt_eqi(FOO_MAGIC_ID, id);

  //Shall send BO and get back as BI
  DEBUG_LOG(" MINK Client Set ID in server (BI) \n");
  Foo_setId(remoteFoo, 93499);
  DEBUG_LOG(" MINK Client get ID in server (BO) \n");
  Foo_getId(remoteFoo, &id);
  qt_eqi(93499, id);

  // Shall send OI (local) and get back as OO
  DEBUG_LOG(" MINK localFoo creation \n");
  Object localFoo = Foo_new();
  qt_local(localFoo);
  DEBUG_LOG(" MINK Foo_setObject OI in server (local) \n");
  qt_eqi(0, Foo_setObject(remoteFoo, localFoo));
  Object localFoo2 = Object_NULL;
  DEBUG_LOG(" MINK Foo_getObject OO (local) from server \n");
  qt_eqi(0, Foo_getObject(remoteFoo, &localFoo2));
  qt_local(localFoo2);
  Object_release(localFoo2);

  localFoo2 = Object_NULL;
  DEBUG_LOG(" MINK Foo_getObject OO (local1) from server \n");
  qt_eqi(0, Foo_getObject(remoteFoo, &localFoo2));
  qt_local(localFoo2);

  DEBUG_LOG(" MINK Foo_setId on (local1) obtained from server(object created by client) \n");
  Foo_setId(localFoo2, 923);
  DEBUG_LOG(" MINK Foo_getId on (local1) obtained from server(object created by client) \n");
  Foo_getId(localFoo2, &id);
  qt_eqi(923, id);
  Object_release(localFoo2);

  localFoo2 = Object_NULL;
  DEBUG_LOG(" MINK Foo_getObject OO (local2) from server \n");
  qt_eqi(0, Foo_getObject(remoteFoo, &localFoo2));
  qt_local(localFoo2);

  Foo_setId(localFoo2, 923);
  DEBUG_LOG(" MINK Foo_getId on (local2) obtained from server(object created by client) \n");
  Foo_getId(localFoo2, &id);
  qt_eqi(923, id);
  Object_release(localFoo2);

  //Shall send OI (remote) and get back as OO
  DEBUG_LOG(" MINK Foo_setObject on remoteFoo as remoteFoo \n");
  qt_eqi(0, Foo_setObject(remoteFoo, remoteFoo));
  Object remoteFoo2 = Object_NULL;
  DEBUG_LOG(" MINK Foo_getObject on remoteFoo set\n");
  qt_eqi(0, Foo_getObject(remoteFoo, &remoteFoo2));
  qt_remote(remoteFoo2);
  DEBUG_LOG(" MINK Foo_setId on remoteFoo2 set\n");
  Foo_setId(remoteFoo2, 923);
  DEBUG_LOG(" MINK Foo_getId on remoteFoo2 set\n");
  Foo_getId(remoteFoo2, &id);
  qt_eqi(923, id);
  Object_release(remoteFoo2);
  remoteFoo2 = Object_NULL;

  DEBUG_LOG(" MINK Foo_getObject on remoteFoo2 set\n");
  qt_eqi(0, Foo_getObject(remoteFoo, &remoteFoo2));
  qt_remote(remoteFoo2);
  Object_release(remoteFoo2);
  
  //Max arg test make sure 60 args over minksocket works
  ObjectArg maxargs[LXCOM_MAX_ARGS];
  ObjectCounts counts = ObjectCounts_pack(15,15,15,15);
  char outbuf[15][4];
  Object testObj = Foo_new();
  memset(outbuf, 0, sizeof(outbuf));
  memset(maxargs, 0, sizeof(maxargs));

  FOR_ARGS(j, counts, BI) {
    maxargs[j].bi = (ObjectBufIn) { TEST_BUFFER, 4 };
  }
  FOR_ARGS(k, counts, BO) {
    maxargs[k].b = (ObjectBuf) { outbuf[k - ObjectCounts_numBI(counts)], 4 };
  }
  FOR_ARGS(l, counts, OI) {
    maxargs[l].o = testObj;
  }
  FOR_ARGS(m, counts, OO) {
    maxargs[m].o = Object_NULL;
  }

  DEBUG_LOG(" MINK Foo_OP_maxArgs on remote object\n");
  qt_eqi(0, Object_invoke(remoteFoo, Foo_OP_maxArgs, maxargs, counts));

  FOR_ARGS(o, counts, BO) {
    qt_eqi(0, memcmp(maxargs[o].b.ptr,
                     maxargs[o - ObjectCounts_numBI(counts)].b.ptr,
                     maxargs[o].b.size));
  }
  FOR_ARGS(p, counts, OO) {
    if(!Object_isNull(maxargs[p].o)) {;} else { LOG("OBJ Null");}
    if(maxargs[p].o.invoke == Foo_invoke) {;} else { LOG("Foo Obj mismatch");}
    Object_release(testObj);
  }
  Object_release(testObj);

  //Test that NULL out objs preserve their order in the
  //out objs list
  DEBUG_LOG(" MINK Foo_ObjectONull on remote object\n");
  Object nullOut = Foo_new();
  Object backup  = nullOut;
  Object realObj = Object_NULL;
  qt_eqi(0, Foo_ObjectONull(remoteFoo, &realObj, &nullOut));
  if(Object_isNull(nullOut))  {;} else { LOG("NULL Object");}
  Object_release(backup);
  Object_release(realObj);



  // Create a 1byte and 4byte BIs and make sure
  // the received side buffer is aligned correctly
  uint32_t pval = 0;
  qt_eqi(0, Foo_unalignedSet(remoteFoo, &pval));
  qt_eqi(0, pval%4);
  DEBUG_LOG(" MINK roundtrip BI/BO upto 60K*9\n");
  //Shall roundtrip BI/BO upto 512K
  {
    ObjectArg a[2];

    for (i=1; i<=8; i++) {
      setupArgs(a, 1<<i, i);
      qt_eqi(0, Object_invoke(remoteFoo, Foo_OP_echo,
                              a, ObjectCounts_pack(1,1,0,0)));
      qt_eqi(0, memcmp(a[0].bi.ptr, a[1].b.ptr, a[0].bi.size));

      setupArgs(a, MAX_BUF_SIZE/8 - 0x1000, 1);
      qt_eqi(0, Object_invoke(remoteFoo, Foo_OP_echo,
                                a, ObjectCounts_pack(1,1,0,0)));
      qt_eqi(0, memcmp(a[0].bi.ptr, a[1].b.ptr, a[0].bi.size));
    }
  }

  //nullptr for a nonzero buffer size is not a valid usecase
  //qt_eqi(Object_ERROR_INVALID, Foo_bufferONull(remoteFoo, 4));
  //expected size of 0 and nullptr is a valid usecase
  //qt_eqi(0, Foo_bufferONull(remoteFoo, 0));
  //Release objects
  DEBUG_LOG(" Release objects \n");
  Object_release(localFoo);
  Object_release(remoteFoo);
  Object_release(opener);

  MinkIPC_release(client);
  
  DEBUG_LOG(" MINKIPC test ends \n");
  return 0;
}

// Driver code
int client_start_end(int b)
{
  Object opener = Object_NULL;

  DEBUG_LOG(" MINK Client before invoking connect \n");
  MinkIPC *client = MinkIPC_connect_vsock(PORT, &opener);
  
  //Shall get OO from server
  Object remoteFoo;
  DEBUG_LOG(" MINK Client IOpener_open \n");
  qt_eqi(0, IOpener_open(opener, 0, &remoteFoo));
  qt_remote(remoteFoo);

  //nullptr for a nonzero buffer size is not a valid usecase
  //qt_eqi(Object_ERROR_INVALID, Foo_bufferONull(remoteFoo, 4));
  //expected size of 0 and nullptr is a valid usecase
  //qt_eqi(0, Foo_bufferONull(remoteFoo, 0));
  //Release objects
  DEBUG_LOG(" Release objects \n");
  //Object_release(localFoo);
  Object_release(remoteFoo);
  Object_release(opener);

  MinkIPC_release(client);
  
  DEBUG_LOG(" MINKIPC test ends \n");
  DEBUG_LOG(" MINKIPC test ends \n");
  return 0;
}

#endif //VSOCK_TEST_CLIENT


static int (*test_cases[])(int) = {
	server_main,
	client_main,
    client_start_end
};
static int num_tests = sizeof(test_cases) / sizeof(*test_cases);

static ssize_t test_case_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int i;

	for (i = 0; i < num_tests; i++) {
		pr_err("Testcase %d: %ps\n", i, test_cases[i]);
	}
	pr_err("\n");

	return 0;
}

static ssize_t test_case_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t len)
{
	u32 cmd;
	int ret;

	if (kstrtou32(buf, 0, &cmd)) {
		pr_err("%s: failed to read cmd from string\n", __func__);
		return len;
	}

	if (cmd >= num_tests) {
		pr_err("Invalid testcase entered\n");
		return len;
	}

	ret = (*test_cases[cmd])(cmd);
	if (ret)
		pr_err("Testcase %d: %ps: Fail!\n", cmd, test_cases[cmd]);
	else
		pr_err("Testcase %d: %ps: Success!\n", cmd, test_cases[cmd]);

	return len;
}
static DEVICE_ATTR_RW(test_case);

static int minkipc_test_probe(struct platform_device *pdev)
{
	int ret;

	vm_socket_init();

	ret = device_create_file(&pdev->dev, &dev_attr_test_case);
	if (ret)
		dev_err(&pdev->dev, "Couldn't create sysfs attribute\n");

	return 0;
}

static const struct of_device_id match_tbl[] = {
	{.compatible = "qcom,minkipc-test"},
	{},
};

static struct platform_driver minkipc_test_driver = {
	.probe = minkipc_test_probe,
	.driver = {
		.name = "minkipc-test",
		.suppress_bind_attrs = true,
		.of_match_table = match_tbl,
	},
};
builtin_platform_driver(minkipc_test_driver);

MODULE_LICENSE("Dual BSD/GPL");