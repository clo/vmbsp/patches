// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause

#if defined(USE_GLIB) && !defined (__aarch64__)
#define __USE_MISC 1
#endif

#include "vm_osal.h"
#include "vm_socket.h"
#include "minksocket.h"
#include "check.h"
#include "lxcom_sock.h"
#include "msforwarder.h"
#include "bbuf.h"
#include "ObjectTableMT.h"
#include "threadpool.h"
#include "fdwrapper.h"
#include "logging.h"
#include "heap.h"

#define BEGIN_INVOKE_ID 1
#define MAX_OBJECT_COUNT 1024
#define MSG_PREALLOCED_SIZE 1024
#define MAX_BUFFER_ALLOCATION 4*1024*1024 // 4MB: Max page size in linux kernel

#ifndef SO_PEERCRED
#define SO_PEERCRED 17
#endif

#ifdef OE
struct ucred {
  uint32_t pid;
  uint32_t uid;
  uint32_t gid;
};
#endif

// Memparcel handles are 64 bits, arg handles are 16 bits, so we need 4
#define NUM_EXTRA_ARGS_PER_MEMPARCEL 4

#define COUNT_NUM_EXTRA_ARGS_OUTBOUND(k, a, section)   ({          \
  size_t __numExtra = 0;                                           \
  FOR_ARGS(i, k, section) {                                        \
    int fd;                                                        \
    if (isWrappedFd(a[i].o, &fd) &&                                \
        is_remote_sock(me->sock)) {                                \
      __numExtra += NUM_EXTRA_ARGS_PER_MEMPARCEL;                  \
    }                                                              \
  }                                                                \
  __numExtra; })

#define COUNT_NUM_EXTRA_ARGS_INBOUND(k, a, section, start) ({      \
  size_t __numExtra = 0;                                           \
  size_t __ii = start;                                             \
  FOR_ARGS(i, k, section) {                                        \
    uint16_t flags = a[__ii].o.flags;                              \
    if (flags & LXCOM_MEMPARCEL_OBJECT) {                          \
      __numExtra += NUM_EXTRA_ARGS_PER_MEMPARCEL;                  \
      __ii += NUM_EXTRA_ARGS_PER_MEMPARCEL;                        \
    }                                                              \
    __ii++;                                                        \
  }                                                                \
  __numExtra; })

#define CHECK_MAX_ARGS(k, dir, numExtra)                           \
  do {                                                             \
    int numArgs = ObjectCounts_num##dir(k);                        \
    int numTotalArgs = numArgs + numExtraArgs;                     \
    if (numTotalArgs > LXCOM_MAX_ARGS) {                           \
      LOGE("%s: Too many args: %d (args : %d, extra args: %d)\n",  \
        __func__, (uint32_t)numTotalArgs,                          \
        (uint32_t)numArgs, (uint32_t)numExtraArgs);                \
      return Object_ERROR_MAXARGS;                                 \
    }                                                              \
  } while(0)

struct MinkSocket {
  int refs;
  int32_t dispatchErr;
  bool bDone;
  ObjectTableMT table;
  int sock;
  int sockPair[2];
  uint32_t invoke_id;
  VM_SOCKET_OSAL_MUTEX mutex;
  VM_SOCKET_OSAL_SIG cond;
  QList qlInvokes;
  ThreadPool *pool;
  uid_t peer_uid;
  gid_t peer_gid;
  bool peer_available;
  int msForwarderCount;
  uint32_t node;
  uint32_t port;
};

static inline int __atomic_add(int *pn, int n) {
  return __sync_add_and_fetch(pn, n);  // GCC builtin
}

//return how much to add to the alignment
#define PADDED(x) ((__typeof__(x))((x) + (((uint64_t)(~(x))+1) & (LXCOM_MSG_ALIGNMENT-1))))

// Return true if sock is a remote socket
static bool is_remote_sock(int sock) {
  return !(UNIX == fetch_socket_type(sock));
}

// Return true if obj is an FdWrapper with a valid fd
// On success, the output parameter "fd" is populated
static bool isWrappedFd(Object obj, int* fd) {
  *fd = -1;
  return (!Object_isNull(obj) &&
          FdWrapperFromObject(obj) != NULL &&
          Object_isOK(Object_unwrapFd(obj, fd)) &&
          *fd > 0);
}

// ----------------------------------------------------------------------------
// Implement WrappedMemparcel
// ----------------------------------------------------------------------------

typedef struct WrappedMemparcel {
  int refs;
  int64_t memparcelHandle;
  Object fdWrapper;
} WrappedMemparcel;

static int32_t
WrappedMemparcel_delete(WrappedMemparcel* me)
{
  int fd;

  // Get fd
  if (!isWrappedFd(me->fdWrapper, &fd)) {
    return Object_ERROR;
  }

  vm_osal_mem_reclaim(fd, me->memparcelHandle);

  // Delete the WPM object:
  Object_ASSIGN_NULL(me->fdWrapper);
  heap_free(me);

  return Object_OK;
}

static int32_t
WrappedMemparcel_release(WrappedMemparcel* me)
{
  if (__atomic_add(&me->refs, -1) == 0) {
    return WrappedMemparcel_delete(me);
  }
  return Object_OK;
}

static int32_t
WrappedMemparcel_invoke(void *cxt, ObjectOp op, ObjectArg *args, ObjectCounts k)
{
  WrappedMemparcel *me = (WrappedMemparcel*) cxt;
  ObjectOp method = ObjectOp_methodID(op);

  switch (method) {
  case Object_OP_retain:
    __atomic_add(&me->refs, 1);
    return Object_OK;

  case Object_OP_release:
    return WrappedMemparcel_release(me);
  }
  return Object_ERROR;
}

static Object WrappedMemparcel_asObject(WrappedMemparcel* me)
{
  return (Object) { WrappedMemparcel_invoke, me };
}

static WrappedMemparcel* WrappedMemparcel_fromObject(Object obj)
{
  return (obj.invoke == WrappedMemparcel_invoke ?
         (WrappedMemparcel*) obj.context : NULL);
}

static Object WrappedMemparcel_getFdWrapper(WrappedMemparcel* me)
{
  if (!Object_isNull(me->fdWrapper)) {
    Object_retain(me->fdWrapper);
    return me->fdWrapper;
  }
  return Object_NULL;
}

static WrappedMemparcel* WrappedMemparcel_new(Object fdWrapper)
{
  int ret;
  int fd;

  WrappedMemparcel* me = HEAP_ZALLOC_REC(WrappedMemparcel);
  if (!me) {
    return NULL;
  }

  if (!isWrappedFd(fdWrapper, &fd)) {
    goto bail;
  }

  ret = vm_osal_mem_share(fd, &me->memparcelHandle);
  if (ret) {
    goto bail;
  }

  me->refs = 1;
  Object_INIT(me->fdWrapper, fdWrapper);
  return me;

bail:
  heap_free(me);
  return NULL;
}

typedef union {
  lxcom_msg msg;
  lxcom_hdr hdr;
  uint8_t buf[MSG_PREALLOCED_SIZE];
  uint64_t _aligned_unused;
} msgbuf_t;

typedef struct InvokeInfo {
  QNode qn;
  uint32_t invoke_id;
  int handle;
  ObjectOp op;
  ObjectArg *args;
  ObjectCounts k;
  int result;
  bool bComplete;
  VM_SOCKET_OSAL_SIG cond;
} InvokeInfo;

static void InvokeInfo_init(InvokeInfo *me, int32_t h,
                ObjectOp op, ObjectArg *args, ObjectCounts k)
{
  C_ZERO(*me);
  me->handle = h;
  me->op = op;
  me->args = args;
  me->k = k;
}

static inline void
InvokeInfo_setResult(InvokeInfo *me, int32_t result) {
  me->bComplete = true;
  me->result = result;
  vm_osal_sig_set(&me->cond);
}


#define FOR_ARGS(ndxvar, counts, section)                       \
  for (ndxvar = ObjectCounts_index##section(counts);            \
       ndxvar < (ObjectCounts_index##section(counts)            \
                 + ObjectCounts_num##section(counts));          \
       ++ndxvar)

/*
 * Return -1 on error, 0 on success
 */
#define IO_REPEAT(func, fd, ptr, size)                         \
  while (size > 0) {                                           \
    ssize_t cb = func(fd, ptr, size, MSG_NOSIGNAL);            \
    if (cb <= 0) {                                             \
      return -1;                                               \
    }                                                          \
    if (cb <= (ssize_t) size) {                                \
      ptr = (cb + (char *) ptr);                               \
      size -= (size_t) cb;                                     \
    }                                                          \
  }                                                            \
  return 0;

#if defined(VNDR_VSOCK)
static int sendv_all_remote(MinkSocket *me, struct iovec *iov, size_t iovLen, int *fds, int num_fds)
{
  size_t buf_len = 0;
  uint32_t max_payload = 0;
  char *buf ;
  char *ptr;
  struct vm_sockaddr client;
  
  fds = NULL;
  num_fds = 0;
  max_payload = MAX_VSOCK_PAYLOAD;
  buf = (char *)heap_malloc(max_payload);
  if (buf == NULL) {
    LOGE("%s: Error allocating 64KB buffer\n", __func__);
    return -1;
  }
  ptr = buf;

  while (iovLen > 0) {
    memcpy(ptr, iov->iov_base, iov->iov_len);
    buf_len += iov->iov_len;
    ptr = ptr + (int)iov->iov_len;
    --iovLen;
    ++iov;
  }
  
  VM_SOCK_ADDR_POPULATE(client, me->port, me->node);
  if (vm_sendto(me->sock, buf, buf_len, 0, (struct sockaddr *)&client, sizeof(struct vm_sockaddr)) < 0) {
      LOGE("%s: vsock_sendto failed:\n", __func__);
      heap_free(buf);
      return -1;
  }
  heap_free(buf);
  return 0;
}
#else
static int sendv_all_remote(MinkSocket *me, struct iovec *iov, size_t iovLen, int *fds, int num_fds)
{
  return 1;
}
#endif

/*
 * Return -1 on error, 0 on success
 */
static int sendv_all(MinkSocket *me, struct iovec *iov, size_t iovLen, int *fds, int num_fds)
{
  if (fetch_socket_type(me->sock) == VSOCK)
    return sendv_all_remote(me, iov, iovLen, fds, num_fds);
  else
    return -1;
}



#define ERR_CLEAN(e) \
  do {                                         \
    if(!Object_isOK(err = (e))) {              \
      CHECK_LOG(); goto cleanup; }             \
  } while(0)

#define CHECK_CLEAN(expr) \
  do {                                           \
    if (!(expr)) { CHECK_LOG(); goto cleanup; }  \
  } while (0)

#define ObjectCounts_numObjects(k)  (ObjectCounts_numOI(k) + \
                                     ObjectCounts_numOO(k))

#define ObjectCounts_indexObjects(k) \
  ObjectCounts_indexOI(k)

#define ObjectCounts_indexBUFFERS(k) \
  ObjectCounts_indexBI(k)

#define ObjectCounts_numBUFFERS(k) \
  (ObjectCounts_numBI(k) + ObjectCounts_numBO(k))

#define ObjectCounts_numIn(k) \
  (ObjectCounts_numBUFFERS(k) + ObjectCounts_numOI(k))

#define ObjectCounts_numOut(k) \
  (ObjectCounts_numBO(k) + ObjectCounts_numOO(k))

#define ObjectCounts_sizeofInvReq(k, extraOI) \
  (c_offsetof(lxcom_inv_req, a) + (ObjectCounts_numIn(k) + extraOI) * sizeof(lxcom_arg))

#define ObjectCounts_sizeofInvSucc(k, extraOO) \
  (c_offsetof(lxcom_inv_succ, a) + (ObjectCounts_numOut(k) + extraOO) * sizeof(lxcom_arg))

#if defined(VNDR_VSOCK)
MinkSocket *MinkSocket_remote_new(Object opener,  int sock, uint32_t node, uint32_t port)
{
  MinkSocket *me = HEAP_ZALLOC_REC(MinkSocket);
  if (!me) {
    return NULL;
  }

  me->refs = 1;
  me->sock = sock;
  me->bDone = false;
  me->msForwarderCount = 0;
  me->invoke_id = BEGIN_INVOKE_ID;
  me->node = node;
  me->port = port;
  QList_construct(&me->qlInvokes);
  me->sockPair[0] = -1;
  me->sockPair[1] = -1;

  vm_osal_mutex_init(&me->mutex, NULL);
  vm_osal_sig_init(&me->cond, NULL);
  me->pool = ThreadPool_new();
  CHECK_CLEAN(me->pool);

  CHECK_CLEAN(!ObjectTableMT_construct(&me->table, MAX_OBJECT_COUNT));
  if (!Object_isNull(opener)) {
    CHECK_CLEAN(ObjectTableMT_addObject(&me->table, opener) != -1);
  }
  return me;

 cleanup:
  MinkSocket_release(me);
  return NULL;
}
#else
MinkSocket *MinkSocket_remote_new(Object opener, int sock, uint32_t node, uint32_t port)
{
  return NULL;
}
#endif


// TODO : This function has to be updated to return socket difference between VSOCK(vmsock) dynamically
SOCK_TYPE fetch_socket_type(int sock) {
  #if defined(VNDR_VSOCK)
  return VSOCK;
  #endif
  return UNIX;
}

static inline bool MinkSocket_isReady(MinkSocket *me)
{
  return (me->sock != -1);
}

void MinkSocket_retain(MinkSocket *me)
{
  __atomic_add(&me->refs, 1);
}

void MinkSocket_delete(MinkSocket *me)
{
  if (me->pool) {
    ThreadPool_release(me->pool);
  }

  vm_osal_mutex_deinit(&me->mutex);
  vm_osal_sig_deinit(&me->cond);
  ObjectTableMT_destruct(&me->table);
  heap_free(me);
}

void MinkSocket_close(MinkSocket *me, int32_t err)
{
  QNode *pqn;

  vm_osal_mutex_lock(&me->mutex);

  me->bDone = true;
  if (me->sock != -1) {
    if (VSOCK == fetch_socket_type(me->sock)) {
      vm_close(me->sock);
      me->sock = -1;
    }
  }

  while ((pqn = QList_pop(&me->qlInvokes))) {
    InvokeInfo_setResult(container_of(pqn, InvokeInfo, qn), err);
  }

  vm_osal_mutex_unlock(&me->mutex);
}

bool MinkSocket_isConnected(MinkSocket *me) {
  vm_osal_mutex_lock(&me->mutex);
  if (me->bDone && me->sock == -1) {
    vm_osal_mutex_unlock(&me->mutex);
    return false;
  }
  vm_osal_mutex_unlock(&me->mutex);
  return true;
}

void MinkSocket_registerForwarder(MinkSocket *me)
{
  __atomic_add(&me->msForwarderCount, 1);
  MinkSocket_retain(me);
}


void MinkSocket_unregisterForwarder(MinkSocket *me)
{
  __atomic_add(&me->msForwarderCount, -1);
  if(__atomic_add(&me->refs, 0) == 1) {
  }
  MinkSocket_release(me);
}

void MinkSocket_release(MinkSocket *me)
{
  if (__atomic_add(&me->refs, -1) == 0) {
    MinkSocket_close(me, Object_ERROR_UNAVAIL);
    MinkSocket_delete(me);
  }
}


// TODO : Check detach is needed
int MinkSocket_detach(MinkSocket *me)
{
  if (me->msForwarderCount != 0 || me->sock == -1) return -1;

  MinkSocket_close(me, Object_ERROR_UNAVAIL);
  return 0;
}

int MinkSocket_detachObject(Object *obj)
{
  MSForwarder *forwarder = MSForwarderFromObject(*obj);
  if (forwarder) {
    return MSForwarder_detach(forwarder);
  }
  return -1;
}

void MinkSocket_attachObject(MinkSocket *me, int handle, Object *obj)
{
  *obj = MSForwarder_new(me, handle);
}

static uint8_t PadBuf[8] = {0xF};
static int32_t insertIOV(struct iovec *iov, int32_t nMaxIov,
                         int32_t *pnUsed,
                         void *ptr, size_t size,
                         uint32_t *szOnWire)
{
  int32_t pad;
  if (*pnUsed >= nMaxIov) {
    return Object_ERROR_MAXARGS;
  }

  pad = PADDED(*szOnWire) - *szOnWire;
  if(pad) {
    iov[(*pnUsed)++] = (struct iovec) { PadBuf, pad };
    *szOnWire += pad;
  }

  if (*pnUsed >= nMaxIov) {
    return Object_ERROR_MAXARGS;
  }

  iov[(*pnUsed)++] = (struct iovec) { ptr, size };
  *szOnWire += size;
  return Object_OK;
}

static int
MinkSocket_createArgs(MinkSocket *me, const InvokeInfo *pii,
                     lxcom_inv_req *req,
                     struct iovec *iov, int32_t szIov,
                     int32_t *nIov,
                     int *fds, int num_fds)
{
  int32_t err;
  int fd_index = 0, i, h;
  size_t numExtraArgs, iOI;
  C_ZERO(*req);
  req->hdr.type = LXCOM_REQUEST;
  req->hdr.invoke_id = pii->invoke_id;
  req->op = pii->op;
  req->handle = pii->handle;
  req->k = pii->k;
  

  for(i = 0; i < num_fds; i++) {
    fds[i] = -1; //unset all the fds
  }

  // Count the extra args from memparcels
  numExtraArgs = COUNT_NUM_EXTRA_ARGS_OUTBOUND(pii->k, pii->args, OI);

  // Check for too many args:
  CHECK_MAX_ARGS(pii->k, In, numExtraArgs);

  *nIov = 0;
  err = insertIOV(iov, szIov, nIov,
                  req, ObjectCounts_sizeofInvReq(pii->k, numExtraArgs),
                  &req->hdr.size);
  if (Object_OK != err) {
    if (Object_ERROR_MAXARGS == err) {
      return err;
    }
    return Object_ERROR_INVALID;
  }

  FOR_ARGS(i, pii->k, BO) {
    if (!pii->args[i].b.ptr && pii->args[i].b.size != 0) {
      return Object_ERROR_INVALID;
    }
  }

  // Req Header for:
  // HDR | BI Sizes | BO Sizes | OI handles
  FOR_ARGS(i, pii->k, BI) {
    err = insertIOV(iov, szIov, nIov,
                    pii->args[i].b.ptr, pii->args[i].b.size,
                    &req->hdr.size);
    if (Object_OK != err) {
      return Object_ERROR_INVALID;
    }
  }

  //Copy buffer-sizes into header
  FOR_ARGS(i, pii->k, BUFFERS) {
    req->a[i].size = pii->args[i].b.size;
  }

  iOI = ObjectCounts_indexOI(pii->k);

  //Copy object handles into header
  FOR_ARGS(i, pii->k, OI) {
    if (Object_isNull(pii->args[i].o)) {
      // nothing to do
    } else {

      MSForwarder *msf = MSForwarderFromObject(pii->args[i].o);
      int fd = -1;

      if (msf && msf->conn == me) {
        // The obj is an MSForwarder
        req->a[iOI].o.flags = LXCOM_CALLEE_OBJECT;
        req->a[iOI].o.handle = msf->handle;

      } else if (isWrappedFd(pii->args[i].o, &fd)) {
        // The obj is a wrapped fd
        if (!is_remote_sock(me->sock)) {
          // On a local domain socket, we just send the plain fd
          fds[fd_index] = fd;
          fd_index++;
          req->a[iOI].o.flags = LXCOM_DESCRIPTOR_OBJECT;
          req->a[iOI].o.handle = 0;
          LOGE("The fd is marshalled into the trans buffer \n");
        } else {
          // On a remote socket, we need to share the memory via mem-buf
          // Creating new WrappedMemparcel shares the memory with the destination VM.
          WrappedMemparcel* wmp = WrappedMemparcel_new(pii->args[i].o);
          if (!wmp) {
            return Object_ERROR_UNAVAIL;
          }

          // Add this to the OT
          h = ObjectTableMT_addObject(&me->table, WrappedMemparcel_asObject(wmp));
          if (h == -1) {
            WrappedMemparcel_release(wmp);
            return Object_ERROR_KMEM;
          }

          // OT should hold the only reference to wmp
          WrappedMemparcel_release(wmp);

          req->a[iOI].o.flags = LXCOM_CALLER_OBJECT | LXCOM_MEMPARCEL_OBJECT;
          req->a[iOI].o.handle = h;

          // Add memparcel handle information to args array
          // The next four args are used to hold the handle information
          int64_t memparcelHandle = wmp->memparcelHandle;
          for (int ii=0; ii<NUM_EXTRA_ARGS_PER_MEMPARCEL; ii++) {
            iOI++;
            req->a[iOI].o.flags = LXCOM_MEMPARCEL_INFO;
            req->a[iOI].o.handle = (uint16_t) (memparcelHandle >> ii*16);
          }
        }
      } else {
        //New object, need entry in table
        h = ObjectTableMT_addObject(&me->table, pii->args[i].o);
        if (h == -1) {
          return Object_ERROR_KMEM;
        }

        req->a[iOI].o.flags = LXCOM_CALLER_OBJECT;
        req->a[iOI].o.handle = h;
      }
    }
    iOI++;
  }

  return Object_OK;
}

/*
 * Process arguments returned from the kernel.  For buffer arguments there
 * is nothing to do.  For returned objects, the `inv` structure holds newly
 * allocated descriptors, around which we construct new forwarder instances.
 */
static int32_t
MinkSocket_marshalOut(MinkSocket *me, lxcom_inv_succ *succ, InvokeInfo *pii,
                      int *fds, int num_fds)
{
  int err, fd_index = 0;
  size_t i, iOO;

  // In the succ args array, OOs start right after BOs
  iOO = ObjectCounts_numBO(pii->k);

  FOR_ARGS(i, pii->k, OO) {
    uint16_t flags = succ->a[iOO].o.flags;
    uint16_t handle = succ->a[iOO].o.handle;
    if (flags & LXCOM_CALLEE_OBJECT) {

      if (flags & LXCOM_MEMPARCEL_OBJECT) {
        int64_t memparcelHandle = 0;

        // Assemble the memparcel handle from the next four args entries
        for (int ii=0; ii<NUM_EXTRA_ARGS_PER_MEMPARCEL; ii++) {
          iOO++;
          if (!(succ->a[iOO].o.flags & LXCOM_MEMPARCEL_INFO)) {
            ERR_CLEAN(Object_ERROR_UNAVAIL);
          }
          uint16_t handleChunk = succ->a[iOO].o.handle;
          memparcelHandle |= ((int64_t)handleChunk << ii*16);
        }

        int fd = -1;
        int ret = vm_osal_mem_accept(memparcelHandle, &fd);
        if (ret) {
          ERR_CLEAN(Object_ERROR_UNAVAIL);
        }

        // Create new FdWrapper and MSF. Set msf as the FdWrapper's dependency.
        // This allows msf to be released when the FdWrapper is deleted,
        // which sends the close operation to the other domain.
        Object obj = FdWrapper_new(fd);
        if (Object_isNull(obj)) {
          fd_close(fd);
          ERR_CLEAN(Object_ERROR_UNAVAIL);
        }

        Object msf = MSForwarder_new(me, handle);
        if (Object_isNull(msf)) {
          ERR_CLEAN(Object_ERROR_UNAVAIL);
        }

        // The fdwrapper (obj) holds the only reference to msf
        FdWrapperFromObject(obj)->dependency = msf;

        // Give the wrapped fd to caller
        pii->args[i].o = obj;

      } else {
        // Normal callee object, create new outbound object
        pii->args[i].o = MSForwarder_new(me, succ->a[iOO].o.handle);
        if (Object_isNull(pii->args[i].o)) {
          ERR_CLEAN(Object_ERROR_KMEM);
        }
      }

    } else if (flags & LXCOM_CALLER_OBJECT) {
      // Familiar object
      Object obj = ObjectTableMT_recoverObject(&me->table,
                                               succ->a[iOO].o.handle);
      if (Object_isNull(obj)) {
        ERR_CLEAN(Object_ERROR_BADOBJ);
      }

      if (flags & LXCOM_MEMPARCEL_OBJECT) {
        // It's a WMP, need to give back the FdWarpper
        WrappedMemparcel* wmp = WrappedMemparcel_fromObject(obj);
        if (!wmp) {
          Object_release(obj);
          ERR_CLEAN(Object_ERROR_BADOBJ);
        }
        pii->args[i].o = WrappedMemparcel_getFdWrapper(wmp);
        WrappedMemparcel_release(wmp);
      } else {
        // Normal object
        pii->args[i].o = obj;
      }
    } else if(flags & LXCOM_DESCRIPTOR_OBJECT) {
      if (fd_index >= num_fds) {
        //We didn't receive enough Fds to give out
        ERR_CLEAN(Object_ERROR_UNAVAIL);
      }
      pii->args[i].o = FdWrapper_new(fds[fd_index]);
      if (Object_isNull(pii->args[i].o)) {
        ERR_CLEAN(Object_ERROR_KMEM);
      }

      //so we don't double close if an error occurs
      fds[fd_index] = -1;
      fd_index++;
    } else {
      pii->args[i].o = Object_NULL;
    }
    ++iOO;
  }

  return Object_OK;

  /*
   * If marshaling of any object has failed, we will be returning an error.
   * In that case the caller will not receive output objects so we must
   * heap_free any allocated object data (and release the descriptors they hold).
   */

 cleanup:

  FOR_ARGS(i, pii->k, OO) {
    Object_ASSIGN_NULL(pii->args[i].o);
  }
  /** FDs can be supported by the kernel and it is dummy layer */
  for (i = 0; i < num_fds; i++) {
    if (fds[i] != -1) {
      fd_close(fds[i]);
    }
  }
  return Object_ERROR_UNAVAIL;
}



//Called from invoker thread...
int32_t
MinkSocket_invoke(MinkSocket *me, int32_t h,
                ObjectOp op, ObjectArg *args, ObjectCounts k)
{

  int32_t err = Object_OK;
  lxcom_inv_req *req = NULL;
  struct iovec *iov = NULL;
  int32_t nIov = 0, i;
  InvokeInfo ii;
  int fds[ObjectCounts_maxOI];

  if (!MinkSocket_isReady(me)) {
    return Object_ERROR_UNAVAIL;
  }

  req = HEAP_ZALLOC_REC(lxcom_inv_req);
  CHECK_CLEAN(req);
  /** Kernel stack is small so memory dynamically allocatd */
  iov = heap_calloc(sizeof(struct iovec), LXCOM_MAX_ARGS*2);
  CHECK_CLEAN(iov);

  InvokeInfo_init(&ii, h, op, args, k);

  if (0 != vm_osal_sig_init(&ii.cond, NULL)) {
    return Object_ERROR_KMEM;
  }

  ERR_CLEAN(MinkSocket_createArgs(me, &ii, req, iov,
                                  sizeof(struct iovec)* LXCOM_MAX_ARGS*2, &nIov,
                                  fds, C_LENGTHOF(fds)));
  int num_fds = 0;
  for (i = 0; i < (size_t) C_LENGTHOF(fds); i++) {
    if (fds[i] != -1) {
      num_fds++;
    }
  }

  vm_osal_mutex_lock(&me->mutex);

  //Allocate an ID for this invocation
  ii.invoke_id = req->hdr.invoke_id = me->invoke_id++;
  QList_appendNode(&me->qlInvokes, &ii.qn);
  if (-1 == sendv_all(me, iov, nIov, fds, num_fds)) {
    QNode_dequeue(&ii.qn);
    err = Object_ERROR_UNAVAIL;
  }
  //wait for the response
  if (Object_OK == err) {
    while (!ii.bComplete) {
      vm_osal_sig_wait(&ii.cond, &me->mutex, (int*)&ii.bComplete);
    }

    QNode_dequeueIf(&ii.qn);
    err = ii.result;
  }
  vm_osal_mutex_unlock(&me->mutex);

cleanup:

  vm_osal_sig_deinit(&ii.cond);
  if(req) heap_free(req);
  if(iov) heap_free(iov);
  return err;
}

static InvokeInfo *
MinkSocket_getInvokeInfo(MinkSocket *me, uint32_t id)
{
  vm_osal_mutex_lock(&me->mutex);

  QNode *pqn;
  QLIST_FOR_ALL(&me->qlInvokes, pqn) {
    InvokeInfo *pii = container_of(pqn, InvokeInfo, qn);
    if (pii->invoke_id == id) {
      vm_osal_mutex_unlock(&me->mutex);
      return pii;
    }
  }
  vm_osal_mutex_unlock(&me->mutex);
  return NULL;
}

int32_t
MinkSocket_sendClose(MinkSocket *me, int handle)
{
  int32_t ret;
  struct vm_sockaddr client;
  int sz = sizeof(lxcom_inv_close);
  if (!MinkSocket_isReady(me)) {
    return Object_ERROR_UNAVAIL;
  }

  lxcom_inv_close cls = (lxcom_inv_close) { sz, LXCOM_CLOSE, handle};

  if (fetch_socket_type(me->sock) != VSOCK)
    return Object_ERROR_UNAVAIL;

  vm_osal_mutex_lock(&me->mutex);  
  
  VM_SOCK_ADDR_POPULATE(client, me->port, me->node);
  ret = vm_sendto(me->sock, &cls, sz, 0, (struct sockaddr *)&client, sizeof(struct vm_sockaddr));
  vm_osal_mutex_unlock(&me->mutex);
  return (ret == -1) ? Object_ERROR_UNAVAIL : Object_OK;
}


/* Handles Invoke Success messages and marshals out arguments
 *
 * The Success buffer (mb->msg.succ of type lxcom_inv_succ) is
 * used to send information about out buffers and out objects.
 * MinkSocket_SendInvokeSuccess populates this with output argument
 * details.
 * Indicies from 0-numBO pertain to out Buffers and
 * Indicies from numBo-numOO pertain to out Objects
*/
static int32_t
MinkSocket_recvInvocationSuccess(MinkSocket *me, msgbuf_t *mb,
                                 ThreadWork *work, int *fds, int num_fds)
{
  int32_t err;
  size_t size;
  int iBO, i;
  InvokeInfo *pii = MinkSocket_getInvokeInfo(me, mb->hdr.invoke_id);
  if (!pii) {
    ERR_CLEAN(Object_ERROR_UNAVAIL);
  }

  // Count the "extra" args not represented in the ObjectCounts
  size_t numExtraArgs = COUNT_NUM_EXTRA_ARGS_INBOUND(
                          pii->k,
                          mb->msg.succ.a, OO,
                          ObjectCounts_numBO(pii->k));

  size = ObjectCounts_sizeofInvSucc(pii->k, numExtraArgs);
  if (size > mb->hdr.size) {
    LOGE("%s: failed hdr size check, size %d, hdr.size %d, numExtraArgs %d, k %x \n ",
         __func__, (uint32_t)size, (uint32_t)mb->hdr.size, (uint32_t)numExtraArgs, pii->k);
    ERR_CLEAN(Object_ERROR_MAXARGS);
  }

  if (ObjectCounts_total(pii->k) > C_LENGTHOF(mb->msg.succ.a)) {
    ERR_CLEAN(Object_ERROR_MAXARGS);
  }

  iBO = 0;
  FOR_ARGS(i, pii->k, BO) {
    size = PADDED(size);
    if (size > mb->hdr.size) {
      ERR_CLEAN(Object_ERROR_MAXARGS);
    }

    if (pii->args[i].b.size < mb->msg.succ.a[iBO].size) {
      ERR_CLEAN(Object_ERROR_INVALID);
    }

    memcpy(pii->args[i].b.ptr,
           mb->buf + size, mb->msg.succ.a[iBO].size);
    pii->args[i].b.size = mb->msg.succ.a[iBO].size;
    size += mb->msg.succ.a[iBO].size;
    if (size > mb->hdr.size) {
      ERR_CLEAN(Object_ERROR_MAXARGS);
    }
    iBO++;
  }

  ERR_CLEAN( MinkSocket_marshalOut(me, &mb->msg.succ, pii, fds, num_fds) );
  if (work) {
   ThreadPool_queue(me->pool, work);
   work = NULL;
  }

  InvokeInfo_setResult(pii, Object_OK);

 cleanup:
  if (work) {
   ThreadPool_queue(me->pool, work);
  }
  return err;
}


static int32_t
MinkSocket_recvInvocationError(MinkSocket *me, msgbuf_t *mb)
{
  int32_t err;
  if (mb->hdr.size != sizeof(lxcom_inv_err)) {
    return Object_ERROR_INVALID;
  }

  InvokeInfo *pii = MinkSocket_getInvokeInfo(me, mb->hdr.invoke_id);
  if (pii) {
    InvokeInfo_setResult(pii, mb->msg.err.err);
    err = Object_OK;
  } else {
    err = Object_ERROR_UNAVAIL;
  }
  return err;
}

static int32_t MinkSocket_wireToBOArgs(MinkSocket *me, lxcom_inv_req *req,
                                       ObjectArg *args,
                                       void **ppvBuf, int32_t size)
{
  int32_t boSize = 0;
  int i;
  void *bo;

  FOR_ARGS(i, req->k, BO) {
    args[i].b.size = req->a[i].size;
    if (args[i].b.size != 0)
      boSize += PADDED(args[i].b.size);
    else
      boSize += LXCOM_MSG_ALIGNMENT;
  }

  if (boSize > size) {
    if (boSize > MAX_BUFFER_ALLOCATION - 4) {
      return Object_ERROR_INVALID;
    }
    bo = heap_zalloc(boSize+4);
    if (NULL == bo) {
      return Object_ERROR_INVALID;
    }
    *ppvBuf = bo;
  } else {
    bo = *ppvBuf;
  }

  BBuf bbuf;
  BBuf_construct(&bbuf, bo, boSize+4);
  FOR_ARGS(i, req->k, BO) {
    args[i].b.ptr = BBuf_alloc(&bbuf, req->a[i].size);
  }

  return Object_OK;
}


static int32_t
MinkSocket_recvClose(MinkSocket *me, msgbuf_t *mb)
{
  if (mb->hdr.size != sizeof(lxcom_inv_close)) {
    return Object_ERROR_INVALID;
  }

  ObjectTableMT_releaseHandle(&me->table, mb->msg.close.handle);
  return Object_OK;
}

static int32_t
MinkSocket_sendInvokeSuccess(MinkSocket *me, lxcom_inv_req *req,
                             ObjectArg *args)
{

  int32_t err = -1;
  int32_t nIov = 0;
  struct iovec *iov = NULL;
  size_t numBO, numExtraArgs;
  int argi, iOO, i;
  int fds[ObjectCounts_maxOO];
  int fd_index = 0;
  
  lxcom_inv_succ succ;
  C_ZERO(succ);
  
  succ.hdr.type = LXCOM_SUCCESS;
  succ.hdr.invoke_id = req->hdr.invoke_id;

  // Count the extra args from memparcels
  numExtraArgs = COUNT_NUM_EXTRA_ARGS_OUTBOUND(req->k, args, OO);

  // Check for too many args:
  CHECK_MAX_ARGS(req->k, Out, numExtraArgs);

  iov = heap_calloc(sizeof(struct iovec), LXCOM_MAX_ARGS*2);
  CHECK_CLEAN(iov);

  err = insertIOV(iov, sizeof(struct iovec)*LXCOM_MAX_ARGS*2, &nIov,
                  &succ, ObjectCounts_sizeofInvSucc(req->k, numExtraArgs),
                  &succ.hdr.size);
  if (Object_OK != err) {
    LOGE("%s: err %x from insertIOV 1\n", __func__, err);
    heap_free(iov);
    return err;
  }

  numBO = ObjectCounts_numBO(req->k);
  //Copy BO sizes into header
  for (i=0; i<numBO; ++i) {
    argi = ObjectCounts_indexBO(req->k)+i;
    succ.a[i].size = args[argi].b.size;

    err = insertIOV(iov, sizeof(struct iovec)*LXCOM_MAX_ARGS*2, &nIov,
                  args[argi].b.ptr, args[argi].b.size,
                  &succ.hdr.size);
    if (Object_OK != err) {
      LOGE("%s: err %x from insertIOV 2\n", __func__, err);
      heap_free(iov);
      return err;
    }
  }


  for (i=0; i < ObjectCounts_maxOO; i++) {
    fds[i] = -1;
  }

  iOO = numBO;
  FOR_ARGS(i, req->k, OO) {
    if (Object_isNull(args[i].o)) {
      succ.a[iOO].o.flags = 0;
      succ.a[iOO].o.handle = 0;
    } else {
      MSForwarder *po = MSForwarderFromObject(args[i].o);
      int fd;
      if (Object_OK != Object_unwrapFd(args[i].o, &fd)) {
        fd = -1;
      }
      if (po && po->conn == me) {
        //Forwarder already.. peel and send the handle
        succ.a[iOO].o.flags = LXCOM_CALLER_OBJECT;
        succ.a[iOO].o.handle = po->handle;
      } else if (fd > 0) {
        // It's a wrapped fd
        if (!is_remote_sock(me->sock)) {
          succ.a[iOO].o.flags = LXCOM_DESCRIPTOR_OBJECT;
          succ.a[iOO].o.handle = 0;
          fds[fd_index] = fd;
          fd_index++;
        } else {
          // This FdWrapper holds a reference to the MSForwarder we want
          MSForwarder* poDep = MSForwarderFromObject(FdWrapperFromObject(args[i].o)->dependency);
          if (poDep && poDep->conn == me) {
            // This is the msf for a memory object we created
            //  during MinkSocket_recvInvocationRequest()
            succ.a[iOO].o.flags = LXCOM_CALLER_OBJECT | LXCOM_MEMPARCEL_OBJECT;
            succ.a[iOO].o.handle = poDep->handle;
          } else if (poDep) {
            // We have a dep object, but not on this socket.
            // Don't want to send it over, so do nothing
            LOGE("%s: Detected unknown mem obj \n", __func__);
          } else {
            // This is simply a wrapped fd the callee gave us,
            //  send it over as a memory object.

            // On a remote socket, we need to share the memory via mem-buf
            // Creating new WrappedMemparcel shares the memory with the destination VM.
            WrappedMemparcel* wmp = WrappedMemparcel_new(args[i].o);
            if (!wmp) {
              heap_free(iov);
              return Object_ERROR_UNAVAIL;
            }

            // Add this to the OT
            int h = ObjectTableMT_addObject(&me->table, WrappedMemparcel_asObject(wmp));
            if (h == -1) {
              WrappedMemparcel_release(wmp);
              heap_free(iov);
              return Object_ERROR_KMEM;
            }

            // OT should hold the only reference to wmp
            WrappedMemparcel_release(wmp);

            succ.a[iOO].o.flags = LXCOM_CALLEE_OBJECT | LXCOM_MEMPARCEL_OBJECT;
            succ.a[iOO].o.handle = h;

            // Add memparcel handle information to args array
            // The next four args are used to hold the handle information
            int64_t memparcelHandle = wmp->memparcelHandle;
            for (int ii=0; ii<NUM_EXTRA_ARGS_PER_MEMPARCEL; ii++) {
              iOO++;
              succ.a[iOO].o.flags = LXCOM_MEMPARCEL_INFO;
              succ.a[iOO].o.handle = (uint16_t) (memparcelHandle >> ii*16);
            }
          }
        }
      } else {
        //New object, need entry in table
        int h = ObjectTableMT_addObject(&me->table, args[i].o);
        if (h == -1) {
          LOGE("%s:  Error adding object to table\n", __func__);
          heap_free(iov);
          return Object_ERROR_KMEM;
        }
        succ.a[iOO].o.flags = LXCOM_CALLEE_OBJECT;
        succ.a[iOO].o.handle = h;
      }
    }
    ++iOO;
  }

  vm_osal_mutex_lock(&me->mutex);
  if (-1 == sendv_all(me, iov, nIov, fds, fd_index)) {
    LOGE("%s:  Error sending message: returning Object_ERROR_UNAVAIL \n", __func__);
    err = Object_ERROR_UNAVAIL;
  }
  vm_osal_mutex_unlock(&me->mutex);

cleanup:
  if(iov) heap_free(iov);
  return err;
}

static int32_t
MinkSocket_sendInvokeError(MinkSocket *me, lxcom_inv_req *req,
                           int32_t error)
{
  int32_t ret;
  struct vm_sockaddr client;
  lxcom_inv_err err;
  err.hdr.type = LXCOM_ERROR;
  err.hdr.size = sizeof(err);
  err.hdr.invoke_id = req->hdr.invoke_id;
  err.err = error;

  if (fetch_socket_type(me->sock) != VSOCK)
    return Object_ERROR_UNAVAIL;

  vm_osal_mutex_lock(&me->mutex);

  VM_SOCK_ADDR_POPULATE(client, me->port, me->node);
  ret = vm_sendto(me->sock, &err, err.hdr.size, 0, (struct sockaddr *)&client, sizeof(struct vm_sockaddr));
  vm_osal_mutex_unlock(&me->mutex);

  return (ret == -1) ? Object_ERROR_UNAVAIL : Object_OK;
}


static int32_t
MinkSocket_recvInvocationRequest(MinkSocket *me, msgbuf_t *mb,
                                 ThreadWork *work, int *fds, int num_fds)
{
  int32_t err = Object_OK, oiErr = Object_OK ;
  typedef struct {
    uint8_t buf[MSG_PREALLOCED_SIZE];
    uint64_t _aligned_unused;
  } bufBO;
  void *pvBO = NULL;
  int fd_index = 0, i;
  size_t iOI, size, numExtraArgs;

  bufBO *ptemp = (bufBO*)heap_calloc(sizeof(bufBO), 1);
  CHECK_CLEAN(ptemp);

  pvBO = ptemp->buf;

  // Count the "extra" args not represented in the ObjectCounts
  numExtraArgs = COUNT_NUM_EXTRA_ARGS_INBOUND(
                   mb->msg.req.k,
                   mb->msg.req.a, OI,
                   ObjectCounts_indexOI(mb->msg.req.k));

  size = ObjectCounts_sizeofInvReq(mb->msg.req.k, numExtraArgs);
  if (size > mb->hdr.size) {
    ERR_CLEAN(Object_ERROR_INVALID);
  }

  ObjectArg args[LXCOM_MAX_ARGS];
  FOR_ARGS(i, mb->msg.req.k, BI) {
    size = PADDED(size);
    if (size > mb->hdr.size) {
      ERR_CLEAN(Object_ERROR_INVALID);
    }

    args[i].b.ptr = (uint8_t *)mb->buf + size;
    args[i].b.size = mb->msg.req.a[i].size;
    size += mb->msg.req.a[i].size;
    if (size > mb->hdr.size) {
      ERR_CLEAN(Object_ERROR_INVALID);
    }
  }

  if (0 != ObjectCounts_numBO(mb->msg.req.k)) {
    ERR_CLEAN(MinkSocket_wireToBOArgs(me, &mb->msg.req, args,
                                      &pvBO, MSG_PREALLOCED_SIZE));
  }

  iOI = ObjectCounts_indexOI(mb->msg.req.k);

  FOR_ARGS(i, mb->msg.req.k, OI) {
    uint16_t flags = mb->msg.req.a[iOI].o.flags;
    uint16_t handle = mb->msg.req.a[iOI].o.handle;

    if (flags & LXCOM_CALLER_OBJECT) {
      // new remote object

      if (flags & LXCOM_MEMPARCEL_OBJECT) {
        int64_t memparcelHandle = 0;

        // Assemble the memparcel handle from the next four args entries
        for (int ii=0; ii<NUM_EXTRA_ARGS_PER_MEMPARCEL; ii++) {
          iOI++;
          if (!(mb->msg.req.a[iOI].o.flags & LXCOM_MEMPARCEL_INFO)) {
            ERR_CLEAN(Object_ERROR_UNAVAIL);
          }
          uint16_t handleChunk = mb->msg.req.a[iOI].o.handle;
          memparcelHandle |= ((int64_t)handleChunk << ii*16);
        }

        int fd = -1;
        int ret = vm_osal_mem_accept(memparcelHandle, &fd);
        if (ret) {
          ERR_CLEAN(Object_ERROR_UNAVAIL);
        }

        // Create new FdWrapper and MSF. Set msf as the FdWrapper's dependency.
        // This allows msf to be released when the FdWrapper is deleted,
        // which sends the close operation to the other domain.
        Object obj = FdWrapper_new(fd);
        if (Object_isNull(obj)) {
          fd_close(fd);
          ERR_CLEAN(Object_ERROR_UNAVAIL);
        }

        Object msf = MSForwarder_new(me, handle);
        if (Object_isNull(msf)) {
          ERR_CLEAN(Object_ERROR_UNAVAIL);
        }

        // The fdwrapper (obj) holds the only reference to msf
        FdWrapperFromObject(obj)->dependency = msf;

        // Give the wrapped fd to destination process
        args[i].o = obj;

      } else {
        // Regular caller object, just hand out the MSForwarder
        Object msf = MSForwarder_new(me, handle);
        if (Object_isNull(msf)) {
          ERR_CLEAN(Object_ERROR_UNAVAIL);
        }
        args[i].o = msf;
      }
    } else if (flags & LXCOM_CALLEE_OBJECT) {
      args[i].o = ObjectTableMT_recoverObject(&me->table, handle);

      if (Object_isNull(args[i].o)) {
        ERR_CLEAN(Object_ERROR_UNAVAIL);
      }
    } else if (flags & LXCOM_DESCRIPTOR_OBJECT) {
      if (fd_index >= num_fds) {
        // The expected fd object count doesn't match the number of fds
        // actually received
        ERR_CLEAN(Object_ERROR_UNAVAIL);
      }
      args[i].o = FdWrapper_new(fds[fd_index]);
      if (Object_isNull(args[i].o)) {
        ERR_CLEAN(Object_ERROR_UNAVAIL);
      }

      //so we don't double close if an error occurs
      fds[fd_index] = -1;
      fd_index++;
    } else {
      LOGE("%s: Found an object with no flags \n", __func__);
      args[i].o = Object_NULL;
    }
    iOI++;
  }

  //get the object out
  Object o = ObjectTableMT_recoverObject(&me->table,
                                       mb->msg.req.handle);
  if (work) {
   ThreadPool_queue(me->pool, work); //queue up the next read
   work = NULL;
  }

  if (!Object_isNull(o)) {
    oiErr = Object_invoke(o, ObjectOp_methodID(mb->msg.req.op),
                        args, mb->msg.req.k);
    if (Object_OK == oiErr) {
      //send Success unless there is an internal error
      err = MinkSocket_sendInvokeSuccess(me, &mb->msg.req, args);
    } else {
      //send error
      //transport err SHOULD be Object_OK else fd will be closed
      err = MinkSocket_sendInvokeError(me, &mb->msg.req, oiErr);
    }

    //Release all Object references since we're done with them
    FOR_ARGS(i, mb->msg.req.k, OI) {
      Object_ASSIGN_NULL(args[i].o);
    }

    //Release all output Object references since we're done with them 	1227
    if (Object_isOK(oiErr)) {
      FOR_ARGS(i, mb->msg.req.k, OO) {
        Object_ASSIGN_NULL(args[i].o);
      }
	}
	
    Object_release(o);
  } else {
    LOGE("%s: target object %d is NULL !!\n", __func__, mb->msg.req.handle);
  }

 cleanup:
  
  if (work) {
   ThreadPool_queue(me->pool, work);
  }

  if (pvBO != ptemp->buf) {
    heap_free(pvBO);
  }
  
  if(ptemp) heap_free(ptemp);

  for (i = 0; i < num_fds; i++) {
    //close all fds found on input
    if (fds[i] != -1) {
      fd_close(fds[i]);
    }
  }

  return err;
}

#if defined(VNDR_VSOCK)
struct Mink_remote_Context {
  MinkSocket *conn;
  void *data;
  size_t data_len;
};

void* __process_message(void *data)
{
  int err;
  struct Mink_remote_Context *cxt = (struct Mink_remote_Context *)data;
  msgbuf_t *mb = (msgbuf_t *)cxt->data;
  ThreadWork *work = NULL;
  switch (mb->hdr.type) {
    case LXCOM_REQUEST:
      err = MinkSocket_recvInvocationRequest(cxt->conn, mb, work, NULL, 0);
      break;
    case LXCOM_SUCCESS:
      err = MinkSocket_recvInvocationSuccess(cxt->conn, mb, work, NULL, 0);
      break;
    case LXCOM_ERROR:
      err = MinkSocket_recvInvocationError(cxt->conn, mb);
      break;
    case LXCOM_CLOSE:
      err = MinkSocket_recvClose(cxt->conn, mb);
      break;
    default:
      err = Object_ERROR_UNAVAIL;
  }
  if (Object_OK != err) {
    cxt->conn->dispatchErr = err;
    MinkSocket_close(cxt->conn, err);
    // close all gateway handles as remote
    // cannot trigger a release after fd is closed
    ObjectTableMT_closeAllHandles(&cxt->conn->table);
  }
  heap_free(mb);
  heap_free(data);
  return NULL;
}

int32_t process_message(MinkSocket *me, int sock, void *data, size_t data_len)
{
  /* memory allocated here would be heap_freed in thread handling this req */
  struct Mink_remote_Context *cxt = HEAP_ZALLOC_REC(struct Mink_remote_Context);
  if (!cxt) {
    LOGE("%s: Memory allocation failure for Mink_VSOCK_Context\n", __func__);
    return Object_ERROR_KMEM;
  }
  cxt->data = (char *)heap_malloc(data_len);
  if (!cxt->data) {
    LOGE("%s: error memory allocation\n", __func__);
    return -1;
  }
  memcpy(cxt->data, data, data_len);
  me->sock = sock;
  cxt->conn = me;
  cxt->data_len = data_len;

  ThreadWork* work = NULL;
  if (!me->bDone && me->sock != -1) {
    work = HEAP_ZALLOC_REC(ThreadWork);
    if(!work) {
      heap_free(cxt);
      return Object_ERROR_KMEM;
    }
    ThreadWork_init(work, __process_message, cxt);
    ThreadPool_queue(me->pool, work);
  }
  return 0;
}
#else
int32_t process_message(MinkSocket *me, int sock, void *data, size_t data_len)
{
  return 0;
}
#endif
