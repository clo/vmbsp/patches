// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#ifndef __MINKSOCKET_H
#define __MINKSOCKET_H

#include <stdbool.h>
#include "object.h"


#if defined (__cplusplus)
extern "C" {
#endif

extern uintptr_t gMinkPeerUIDTLSKey;
extern uintptr_t gMinkPeerGIDTLSKey;

typedef struct MinkSocket MinkSocket;


typedef enum
{
    UNIX,
    QIPCRTR,
    VSOCK,
}SOCK_TYPE;


#define VM_SOCK_ADDR_POPULATE(data, port, cid) data.svm_family = AF_VSOCK; \
                                               data.vm_port.port_id = port;\
                                               data.vm_port.node_id = cid;

#define MAX_VSOCK_PAYLOAD  (64 * 1024)
#define VOSK_SERVER_PORT_NO 0x5555

SOCK_TYPE fetch_socket_type(int sock);
MinkSocket *MinkSocket_new(Object opener);
MinkSocket *MinkSocket_remote_new(Object opener, int sock, uint32_t node, uint32_t port);
int32_t process_message(MinkSocket *me, int sock, void *data, size_t data_len);
void MinkSocket_retain(MinkSocket *me);
void MinkSocket_release(MinkSocket *me);
void MinkSocket_registerForwarder(MinkSocket *me);
void MinkSocket_unregisterForwarder(MinkSocket *me);
int MinkSocket_detach(MinkSocket *me);
/** MinkSocket_attachObject attempts to attach an object
    with a specified handle much like MinkIPC_Connect does with an opener.
 **/
void MinkSocket_attachObject(MinkSocket *me, int handle, Object *obj);
int MinkSocket_detachObject(Object *obj);
int32_t MinkSocket_invoke(MinkSocket *me, int32_t h,
                  ObjectOp op, ObjectArg *args, ObjectCounts k);
int32_t MinkSocket_sendClose(MinkSocket *me, int handle);
void *MinkSocket_dispatch(void *me);
void MinkSocket_start(MinkSocket *me, int sock);
void MinkSocket_close(MinkSocket *me, int32_t err);
void MinkSocket_delete(MinkSocket *me);
bool MinkSocket_isConnected(MinkSocket *me);

#if defined (__cplusplus)
}
#endif

#endif //__MINKSOCKET_H
