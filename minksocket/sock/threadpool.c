// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#include "vm_osal.h"
#include "heap.h"
#include "threadpool.h"
#include "logging.h"

static inline int __atomic_add(int *pn, int n) {
  return __sync_add_and_fetch(pn, n);  // GCC builtin
}

struct  ThreadPool {
  int refs;
  VM_SOCKET_OSAL_THREAD aThreads[THREADPOOL_MAX_THREADS];
  QList qlWork;
  VM_SOCKET_OSAL_MUTEX qMtx;
  VM_SOCKET_OSAL_SIG qCnd;
  bool bDone;
  int nThreads;
  int nIdleThreads;
  bool bNeedsDelete;
};


static int QList_count(QList *pq)
{
  QNode *pn;
  int num = 0;

  QLIST_FOR_ALL(pq, pn) {
    ++num;
  }
  return num;
}

static void ThreadPool_delete(ThreadPool *me)
{
  vm_osal_mutex_deinit(&me->qMtx);
  vm_osal_sig_deinit(&me->qCnd);
  QNode *qn;
  QNode *qnn;
  QLIST_NEXTSAFE_FOR_ALL(&me->qlWork, qn, qnn) {
    QNode_dequeue(qn);
    heap_free(qn);
  }

  heap_free(me);
  //LOGE("%s pool =%p\n", __func__, me);
}

static void* thread_entrypoint(void *arg)
{
  ThreadPool *me = (ThreadPool *)arg;
  //LOGE("%s pool = %p\n", __func__, me);
  while (!me->bDone) {
    vm_osal_mutex_lock(&me->qMtx);

    while (!me->bDone && QList_isEmpty(&me->qlWork)) {
      ++me->nIdleThreads;
      vm_osal_sig_wait((void*)&me->qCnd, (void*)&me->qMtx, (int*)&me->nIdleThreads);
      --me->nIdleThreads;
    }

    if (me->bDone) {
      vm_osal_mutex_unlock(&me->qMtx);
      //LOGE("%s pool %p, thread done\n", __func__, me);
      return NULL;
    }

    ThreadWork *w = (ThreadWork *)QList_pop(&me->qlWork);
    if (w == NULL)
      return NULL;

    vm_osal_mutex_unlock(&me->qMtx);
    w->workFunc(w->args);
    heap_free(w);
  }

  vm_osal_mutex_unlock(&me->qMtx);
  if (me->bNeedsDelete) {
    ThreadPool_delete(me);
    vm_osal_thread_self_delete();
  }
  return NULL;
}

static void ThreadPool_createThread(ThreadPool *me)
{
  char threadname[32] = {0};
  for (int i=0; i < THREADPOOL_MAX_THREADS; ++i) {
    if (me->aThreads[i] == 0) {
  //LOGE("%s pool %p\n", __func__, me);
      sprintf(threadname, "ServiceThread%d", i);
      vm_osal_thread_create(&me->aThreads[i], &thread_entrypoint, me, threadname);
      ++me->nThreads;
      return;
    }
  }
}


ThreadPool *ThreadPool_new(void)
{
  ThreadPool *me = (ThreadPool *)HEAP_ZALLOC_TYPE(ThreadPool);
  if (me == NULL) {
    return NULL;
  }

  if (0 != vm_osal_mutex_init(&me->qMtx, NULL)) {
    goto bail;
  }

  if (0 != vm_osal_sig_init(&me->qCnd, NULL)) {
    goto bail;
  }
  //LOGE("%s pool %p\n", __func__, me);

  QList_construct(&me->qlWork);
  me->bDone = false;
  me->refs = 1;
  return me;

 bail:
  ThreadPool_release(me);
  return NULL;
}

void ThreadPool_retain(ThreadPool *me)
{
  __atomic_add(&me->refs, 1);
}

void ThreadPool_stop(ThreadPool *me)
{
  vm_osal_mutex_lock(&me->qMtx);
  me->bDone = true;
  vm_osal_sig_set(&me->qCnd);
  vm_osal_mutex_unlock(&me->qMtx);
  //LOGE("%s pool %p\n", __func__, me);

  for (int i=0; i < THREADPOOL_MAX_THREADS; ++i) {
    if (me->aThreads[i] != 0 && me->aThreads[i] != vm_osal_get_current_thread()) {
	  vm_osal_check_for_completion((void*)me->aThreads[i]);
      me->nThreads--;
    }
  }
}

void ThreadPool_wait(ThreadPool *me)
{
  while (!QList_isEmpty(&me->qlWork)  ||
         me->nThreads != me->nIdleThreads) {
    vm_osal_thread_sleep(0);
  }
}

void ThreadPool_release(ThreadPool *me)
{
  if (__atomic_add(&me->refs, -1) == 0) {
    ThreadPool_stop(me);
    if (me->nThreads > 0) {
      me->bNeedsDelete = true;
    } else {
      ThreadPool_delete(me);
    }
  }
}

void ThreadPool_queue(ThreadPool *me, ThreadWork *work)
{
  vm_osal_mutex_lock(&me->qMtx);
  if (me->bDone) {
    vm_osal_sig_set(&me->qCnd);
    vm_osal_mutex_unlock(&me->qMtx);
    heap_free(work);
    return;
  }

  QList_appendNode(&me->qlWork, &work->n);
  if (QList_count(&me->qlWork) > me->nIdleThreads) {
  //LOGE("%s pool %p\n", __func__, me);
    ThreadPool_createThread(me);
  }

  vm_osal_sig_set(&me->qCnd);
  vm_osal_mutex_unlock(&me->qMtx);
}
