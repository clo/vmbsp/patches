// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
//
// SPDX-License-Identifier: BSD-3-Clause

#include <psci.h>
#include <libfdt.h>
#include <fdt_utils.h>

static psci_method_t psci_method = invalid;
static uint32_t psci_suspend_param = 0;

static void psci_init_done_msg()
{
	char str_smc[] = "SMC";
	char str_hvc[] = "HVC";
	char *str_psci_method;
	if (psci_method == smc)
		str_psci_method = str_smc;
	else if (psci_method == hvc)
		str_psci_method = str_hvc;
	printf("PSCI: Using PSCI method %s\n\n", str_psci_method);
}

int psci_method_init()
{
	const void *fdt_base = (const void*)get_fdt_baddr();
	int psci_off, psci_method_len, err = -1;
	const void *psci_method_off;
	psci_off = fdt_path_offset(fdt_base, "/psci");
	if (psci_off) {
		psci_method_off = fdt_getprop(fdt_base, psci_off,
				"method", &psci_method_len);
		if (psci_method_off)
			err = 0;
	}

	if (!err) {
		if (!(strncmp(psci_method_off, "smc", psci_method_len)))
			psci_method = smc;
		else if (!(strncmp(psci_method_off, "hvc", psci_method_len)))
			psci_method = hvc;
		else
			err = -1;
	}

	if (err)
		psci_method = smc; // fallback if errors encountered

	psci_init_done_msg();

	return err;
}

int psci_suspend_init() {
	const void *fdt_buf = (const void*)get_fdt_baddr();
	int ret = -1;

	// Look for cpu-map in FDT
	int cpu_off, cpu_map_off;
	cpu_off = fdt_path_offset(fdt_buf, "/cpus");
	if (cpu_off < 0)
		return -1;
	cpu_map_off = fdt_subnode_offset(fdt_buf, cpu_off, "cpu-map");
	if (cpu_map_off < 0)
		return -1;

	// Find the cpu phandles in FDT
	uint32_t cpu_phandle_arr[PLAT_MAX_CPUS] = {0};
	int cpu_count = 0;
	find_cpu_phandles(fdt_buf, cpu_map_off, cpu_phandle_arr, PLAT_MAX_CPUS,
			&cpu_count);

	// Find offset of CPU0 node
	int node_off = fdt_node_offset_by_phandle(fdt_buf,
			cpu_phandle_arr[0]);
	if (node_off < 0)
		return -1;

	// Look for "cpu-idle-states" property of CPU0
	char *cpu_idle_ptr;
	int reg_len, j, idle_prop_len;
	cpu_idle_ptr = (char*)fdt_getprop(fdt_buf, node_off,
			"cpu-idle-states", &idle_prop_len);
	if (cpu_idle_ptr) {
		const char *status;
		int statlen;

		int deepest_state_idx = (idle_prop_len / 4) - 1;
		uint32_t phandle = be_to_le_32(*(uint32_t*)(cpu_idle_ptr +
					4 * deepest_state_idx));
		int deepest_idle_off = fdt_node_offset_by_phandle(fdt_buf,
				phandle);
		status = (char *)fdt_getprop(fdt_buf, deepest_idle_off, "status", &statlen);
		if (status && statlen > 0) {
			if (!strcmp(status, "disable") || !strcmp(status, "disabled"))
				return 0;
		}
		int param_len;
		char* psci_param_ptr = (char*)fdt_getprop(fdt_buf,
				deepest_idle_off,
				"arm,psci-suspend-param", &param_len);
		psci_suspend_param = psci_param_ptr ? be_to_le_32(*(uint32_t*)(psci_param_ptr)) : 0;
		ret = 0;
	}
	return ret;
}

void psci_init() {
	psci_method_init();
	psci_suspend_init();
}

size_t psci_call(size_t x0, size_t x1, size_t x2, size_t x3)
{
	if (psci_method == hvc)
		__asm__ volatile ("hvc #0"
			: "+r" (x0)
			: "r"  (x0), "r" (x1), "r" (x2), "r" (x3));
	else if (psci_method == smc)
		__asm__ volatile ("smc #0"
			: "+r" (x0)
			: "r"  (x0), "r" (x1), "r" (x2), "r" (x3));
	return x0;
}

uint32_t get_psci_suspend_param()
{
	return psci_suspend_param;
}
