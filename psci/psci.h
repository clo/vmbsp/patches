// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
//
// SPDX-License-Identifier: BSD-3-Clause

#pragma once

#include <stdio.h>
#include <stdint.h>
extern unsigned long get_vbase();
extern unsigned long get_reset_baddr();
extern unsigned long get_fdt_baddr();
extern uint64_t get_el1_va_base();
#define EL1_VA(x)       (get_el1_va_base() + x)

#define PLAT_MAX_CPUS                   8

typedef enum psci_method {
	hvc,
	smc,
	invalid
} psci_method_t;

void psci_init(void);
size_t psci_call(size_t x0, size_t x1, size_t x2, size_t x3);
uint32_t get_psci_suspend_param();

