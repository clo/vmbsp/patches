// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause

#include "vm_osal.h"

#if defined(__KERNEL__)

#if defined(MEMORY_ALLOC_CONTIGUOUS)
#define ZALLOC(x)   kzalloc(x, GFP_KERNEL)
#define FREE(x)     kfree(x)
#else
#define ZALLOC(x)   vzalloc(x)
#define FREE(x)     vfree(x)
#endif

int vm_osal_mutex_init(void* mutex_object, void* attr)
{
  mutex_init(mutex_object);
  return 0;
}

int vm_osal_mutex_deinit(void* mutex_object)
{
  mutex_destroy(mutex_object);
  return 0;
}

int vm_osal_mutex_lock(void* mutex_object)
{
  mutex_lock(mutex_object);
  return 0;
}

int vm_osal_mutex_unlock(void* mutex_object)
{
  mutex_unlock(mutex_object);
  return 0;
}

int vm_osal_sig_init(void* signal_object, void* attr)
{
  init_waitqueue_head(signal_object);
  return 0;
}

/** No deinit for wait queue */
int vm_osal_sig_deinit(void* signal_object)
{
  return 0;
}

int vm_osal_sig_wait(void* cond_wait, void* mutex, int *condition)
{
  if (mutex != NULL) vm_osal_mutex_unlock(mutex);
  wait_event(*(struct wait_queue_head *)cond_wait, *condition);
  if (mutex != NULL) vm_osal_mutex_lock(mutex);
  return 0;
}

int vm_osal_sig_set(void* sig)
{
  wake_up(sig);

  return 0;
}

void vm_osal_thread_sleep(unsigned int time_in_sec)
{
  ssleep(time_in_sec);
}

struct vm_osal_thread_ctxt {
  void* (*fn)(void *);
  void *data;
};

int vm_osal_thead_fn(void *data)
{
  struct vm_osal_thread_ctxt *ctxt = (struct vm_osal_thread_ctxt *)data;
  void* ret;
  ret = ctxt->fn(ctxt->data);
  FREE(ctxt);

  while(!kthread_should_stop()) {;}

  return (ret == NULL)?0:1;
}

int vm_osal_thread_create(VM_SOCKET_OSAL_THREAD *tid, void* (*fn)(void *data), void* data, char *thread_name)
{
  struct vm_osal_thread_ctxt *ctxt;

  ctxt = ZALLOC(sizeof(*ctxt));
  if (!ctxt)
    return -1;

  ctxt->data = data;
  ctxt->fn = fn;

  *tid = kthread_run(vm_osal_thead_fn, ctxt, thread_name);

  return 0;
}

int vm_osal_thread_self_delete()
{
  return kthread_stop(get_current());
}

VM_SOCKET_OSAL_THREAD vm_osal_get_current_thread()
{
  return get_current();
}

/** No equivalent of pthread_join in kernel - use STOP on the thread to make sure it is exited */
int vm_osal_check_for_completion(void *tid)
{
  return kthread_stop(tid);
}

/** Thread specific key store/restore is not needed for UDP/across VMs
This implementation should [TID][KEY] = DATA, retrieve the same way
TID is thread ID which has to be obtained from caller context */
int vm_osal_create_TLS_key(VM_KEY_TYPE *key, void (*destructor)(void*))
{
  *key = 0;
  return 0;
}

int vm_osal_store_TLS_key(VM_KEY_TYPE key, const void *value)
{
  return 0;
}

void *vm_osal_retrieve_TLS_key(VM_KEY_TYPE key)
{
  return NULL;
}

int vm_osal_mem_share(int fd, int64_t* memparcelHandle)
{
  return 0;
}

int vm_osal_mem_accept(int64_t memparcelHandle, int* fd)
{
  return 0;
}

int vm_osal_mem_reclaim(int fd, int64_t memparcelHandle)
{
  return 0;
}

int fd_close(int ufd)
{
  //struct fd f = fdget(ufd);
  return 0;
}

#else // ================= __KERNEL__ end ==============================

void* vm_osal_malloc(ssize_t x)
{
  return malloc(x);
}

void vm_osal_free(void* x)
{
  return free(x);
}

void *vm_osal_zalloc(ssize_t x)
{
  void *ptemp = malloc(x);
  if (ptemp != NULL)
    VM_OSAL_MEMSET(ptemp, 0x0, x);

  return ptemp;
}

void *vm_osal_calloc(ssize_t num, ssize_t size)
{
  void *ptemp = calloc(num, size);
  if (ptemp != NULL)
    VM_OSAL_MEMSET(ptemp, 0x0, (num*size));

  return ptemp;
}

int vm_osal_sig_init(void* signal_object, void* attr)
{
  return pthread_cond_init((pthread_cond_t*)signal_object,
                           (const pthread_condattr_t *)attr);
}


int vm_osal_sig_deinit(void* signal_object)
{
  return pthread_cond_destroy((pthread_cond_t*)signal_object);
}

int vm_osal_mutex_lock(void* mutex_object)
{
  return pthread_mutex_lock((pthread_mutex_t*)mutex_object);
}

int vm_osal_mutex_init(void* mutex_object, void* attr)
{
  return pthread_mutex_init((pthread_mutex_t*)mutex_object,
                            (const pthread_mutexattr_t *)attr);
}

int vm_osal_mutex_deinit(void* mutex_object)
{
  return pthread_mutex_destroy((pthread_mutex_t*)mutex_object);
}

int vm_osal_mutex_unlock(void* mutex_object)
{
  return pthread_mutex_unlock((pthread_mutex_t*)mutex_object);
}

int vm_osal_sig_wait(void* cond_wait, void* mutex, int* condition)
{
  return pthread_cond_wait((pthread_cond_t *)cond_wait, (pthread_mutex_t *)mutex);
}

int vm_osal_sig_set(void* sig)
{
  return pthread_cond_signal((pthread_cond_t *)sig);
}

void vm_osal_thread_sleep(unsigned int time_in_sec)
{
  sleep(time_in_sec);
  return;
}

struct vm_osal_thread_ctxt {
  void* (*fn)(void *);
  void *data;
};

void *vm_osal_thead_fn(void *data)
{
  struct vm_osal_thread_ctxt *ctxt = (struct vm_osal_thread_ctxt *)data;
  ctxt->fn(ctxt->data);
  free(ctxt);
  return 0;
}

int vm_osal_thread_create(VM_SOCKET_OSAL_THREAD *tid, void* (*fn)(void *data), void* data, char *thread_name)
{
  struct vm_osal_thread_ctxt *ctxt;

  ctxt = (struct vm_osal_thread_ctxt *)malloc(sizeof(*ctxt));
  if (!ctxt)
    return -1;

  ctxt->data = data;
  ctxt->fn = fn;

  return pthread_create(tid, NULL, vm_osal_thead_fn, ctxt);
}

int vm_osal_thread_self_delete()
{
  return pthread_detach(pthread_self());
}

VM_SOCKET_OSAL_THREAD vm_osal_get_current_thread()
{
  return pthread_self();
}

int vm_osal_check_for_completion(void *tid)
{
  return pthread_join((pthread_t)tid, NULL);
}

int vm_osal_create_TLS_key(VM_KEY_TYPE *key, void (*destructor)(void*))
{
  return pthread_key_create((pthread_key_t *)key, destructor);
}

int vm_osal_store_TLS_key(VM_KEY_TYPE key, const void *value)
{
  return pthread_setspecific(key, value);
}

void *vm_osal_retrieve_TLS_key(VM_KEY_TYPE key)
{
  return pthread_getspecific(key);
}

int vm_osal_mem_share(int fd, int64_t* memparcelHandle)
{
  return 0;
}

int vm_osal_mem_accept(int64_t memparcelHandle, int* fd)
{
  return 0;
}

int vm_osal_mem_reclaim(int fd, int64_t memparcelHandle)
{
  return 0;
}

int fd_close(int fd)
{
  return close(fd);
}
#endif /** __KERNEL__ */
