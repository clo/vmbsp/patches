// Copyright (c) 2021 The Linux Foundation. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause
#ifndef _VM_OSAL_H_
#define _VM_OSAL_H_

#ifdef __KERNEL__
#include <stddef.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/mutex.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/wait.h>
#include <linux/unistd.h>
#include <linux/uio.h>
#include <linux/stat.h>
#include <linux/errno.h>
#include <linux/poll.h>
#include <linux/string.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#else // __KERNEL__
#define _GNU_SOURCE
#include <stdio.h>    //std input output
#include <stdint.h>
#include <stddef.h>
#include <pthread.h> //thread related
#include <malloc.h> //for malloc
#include <string.h> // memcpy
#include <unistd.h>
#include <fcntl.h>
//#include <sys/socket.h>
#include <sys/uio.h>
//#include <sys/un.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <poll.h>
#include <stdbool.h>
#include <string.h>
#endif //__KERNEL__

#if defined(VNDR_VSOCK)
//#include "vm_socket.h"
#endif
#include "cdefs.h"

#ifdef __KERNEL__
typedef struct mutex VM_SOCKET_OSAL_MUTEX;
typedef struct wait_queue_head VM_SOCKET_OSAL_SIG;
typedef struct task_struct *VM_SOCKET_OSAL_THREAD;
typedef ssize_t  VM_KEY_TYPE;
#else // __KERNEL__
#define container_of c_containerof
typedef pthread_mutex_t VM_SOCKET_OSAL_MUTEX;
typedef pthread_cond_t VM_SOCKET_OSAL_SIG;
typedef pthread_t VM_SOCKET_OSAL_THREAD;
typedef pthread_key_t  VM_KEY_TYPE;
#endif // __KERNEL__

/*general defines*/
#define VM_OSAL_MEMCPY memcpy
#define VM_OSAL_MEMSET memset

/** APIs exposed by OSAL Layer */
/* Mutex related OS adaptation APIs*/
int vm_osal_mutex_init(void *mutex_object, void *attr);
int vm_osal_mutex_lock(void *mutex_object);
int vm_osal_mutex_unlock(void *mutex_object);
int vm_osal_mutex_deinit(void *mutex_object);

/* Sync/cond wait OS adaptation APIs*/
int vm_osal_sig_wait(void* cond_wait, void* mutex, int* condition);
int vm_osal_sig_init(void *signal_object, void *attr);
int vm_osal_sig_set(void *sig);
int vm_osal_sig_deinit(void *signal_object);

/* Thread related OS adaptation APIs*/
void vm_osal_thread_sleep(unsigned int time_in_sec);
int vm_osal_thread_create(VM_SOCKET_OSAL_THREAD *tid,void* (*fn)(void *data), void* data, char *thread_name);
int vm_osal_thread_self_delete(void);
VM_SOCKET_OSAL_THREAD vm_osal_get_current_thread(void);
int vm_osal_check_for_completion(void *tid);

/* Thread related data manipulation  - OS adaptation APIs*/
int vm_osal_create_TLS_key(VM_KEY_TYPE *key, void (*destructor)(void*));
int vm_osal_store_TLS_key(VM_KEY_TYPE key, const void *value);
void *vm_osal_retrieve_TLS_key(VM_KEY_TYPE key);

/* Memory sharing between VMs */
int vm_osal_mem_share(int fd, int64_t* memparcelHandle);
int vm_osal_mem_accept(int64_t memparcelHandle, int* fd);
int vm_osal_mem_reclaim(int fd, int64_t memparcelHandle);

/* Misc */
int fd_close(int fd);
#endif //_VM_OSAL_H_
