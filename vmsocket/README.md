# VMSOCKET
## Linux Kernel compilation
* Clone this project under <src_tree>/techpack as "vmsocket"

* Add devicetree nodes under soc dtsi to add msgq to vdevices and test module platform device.
  * Sample device tree under /devicetree

1. cd techpack
1. git clone vmsocket.git vmsocket

## Userspace Compilation
Makefile is setup to compile the gunyah transport by default.
To generate the binaries with the pipe transport, use
* make PIPE=1
