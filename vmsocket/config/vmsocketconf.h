// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#define CONFIG_VMSOCKET 1
#define CONFIG_VMSOCKET_TEST 1
#define CONFIG_GUEST_INTERFACE 1
