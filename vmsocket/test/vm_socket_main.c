// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#include "vm_socket.h"
#define DATA_BUF_LEN 8000

int fd1, fd2;

static int _vm_socket_main_rx_thread
(
  void* dummy
)
{
  char buf[8000];
  ssize_t got_bytes = 0;
  VM_SOCKET_OSAL_PRINT_RX("RX: Started socket rx thread \n");
  socklen_t addrlen = 0;
  struct vm_sockaddr src_addr;
  src_addr.vm_port.node_id = VM_SOCKETS_OSAL_INVALID;
  src_addr.vm_port.port_id = VM_SOCKETS_OSAL_INVALID;
  while(1)
  {
    vm_socket_osal_thread_sleep(10);
    got_bytes = vm_recvfrom(fd2,buf,1000,0,(struct sockaddr*)&src_addr,&addrlen);

    got_bytes = vm_recvfrom(fd2,buf,1000,0x20,(struct sockaddr*)&src_addr,&addrlen);

    VM_SOCKET_OSAL_PRINT_RX("RX: FD =%d...Got %ld bytes from %d:%u:%u\n",
                            fd2,
                            got_bytes,src_addr.svm_family,
                            src_addr.vm_port.node_id,
                            src_addr.vm_port.port_id);
    vm_socket_osal_thread_sleep(10);
  }

  return 0;
}



int main (void)
{

  char buf[DATA_BUF_LEN];
  unsigned int  spincounter = 0;

  /*kept for debug*/
  struct sockaddr temp;
  struct vm_sockaddr temp2;
  VM_SOCKET_OSAL_PRINT_DEBUG("DEBUG: size of sa_family = %lu, size of sa_data = %lu, sizeof full sockaddr = %lu\n",sizeof(temp.sa_family),sizeof(temp.sa_data),sizeof(temp));
  VM_SOCKET_OSAL_PRINT_DEBUG("DEBUG: size of svm_family = %lu, size of svm_reserved1 = %lu, sizeof vm_port = %lu sizeof zero = %lu full size = %lu\n",sizeof(temp2.svm_family),sizeof(temp2.svm_reserved1),sizeof(temp2.vm_port),sizeof(temp2.svm_zero),sizeof(temp2));

  struct vm_sockaddr bind_addr;
  bind_addr.svm_family = AF_VSOCK;
  bind_addr.vm_port.node_id = VMADDR_CID_ANY;
  bind_addr.vm_port.port_id = 1040;

  struct vm_sockaddr conn_addr2;
  conn_addr2.svm_family = AF_VSOCK;
  conn_addr2.vm_port.node_id = VMADDR_CID_HYPERVISOR;
  conn_addr2.vm_port.port_id = 1040;

  struct vm_sockaddr bind_addr2;
  bind_addr2.svm_family = AF_VSOCK;
  bind_addr2.vm_port.node_id = VMADDR_CID_ANY;
  bind_addr2.vm_port.port_id = VMADDR_PORT_ANY;


  vm_socket_init();

  vm_socket_diagnostics();
  fd1 = vm_socket(AF_VSOCK,SOCK_DGRAM,0);
  vm_socket_diagnostics();
  vm_bind(fd1,(struct sockaddr *)&bind_addr, sizeof(struct vm_sockaddr)); //bind to 40

  vm_socket_diagnostics();
  fd2 = vm_socket(AF_VSOCK,SOCK_DGRAM,0);
  vm_bind(fd2,(struct sockaddr *)&bind_addr2, sizeof(struct vm_sockaddr));  //bind to 30
  vm_socket_diagnostics();
  vm_connect(fd2,(struct sockaddr *)&conn_addr2, sizeof(struct vm_sockaddr)); //conn to 40
  vm_socket_diagnostics();
  vm_socket_osal_thread_create(&_vm_socket_main_rx_thread, NULL, "RX_Socket_Client_Thread");

  vm_socket_diagnostics();

  vm_socket_osal_thread_sleep(1);

  struct vm_sockaddr dest_addr;
  struct sockaddr sock_addr;
  dest_addr.vm_port.node_id = VMADDR_CID_ANY;
  dest_addr.vm_port.port_id = 1025;
  dest_addr.svm_family = AF_VSOCK;
  memcpy(&sock_addr,&dest_addr,sizeof(dest_addr));

  vm_sendto(fd1,buf,DATA_BUF_LEN,0,&sock_addr,sizeof(dest_addr));

  vm_socket_diagnostics();

  while(1)
  {
    VM_SOCKET_OSAL_PRINT_DEBUG("DEBUG: spin counter = %d\n", spincounter++);
    vm_socket_osal_thread_sleep(2);
  };
  return 0;
}



