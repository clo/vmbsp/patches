// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/mutex.h>
#include <linux/platform_device.h>
#include <linux/mod_devicetable.h>

#include "vm_list.h"
#include "vm_socket.h"
#include "vm_socket_osal.h"

#define VM_SOCKET_TEST_SERVER_PORT 0x5555
#define VM_SOCKET_TEST_LARGE_PKT_SIZE 900
#define MAX_MSG_SIZE 1024
#define CID_PEER 3

struct test_device {
	struct platform_device *pdev;

	struct kthread_worker kworker;
	struct kthread_work kwork;
	struct task_struct *task;
};
struct test_device tdev;

struct vm_list client_ports;
VM_LIST(client_ports);

typedef struct {
	unsigned int port_id;
	struct vm_list_item list_item;
}test_port_type;

static unsigned int assign_port_id()
{
	struct vm_list_item *node;
	test_port_type *new_port, *tmp_port;
	unsigned int port_id;

	new_port  = (test_port_type *) vm_socket_osal_malloc(sizeof(test_port_type));
	if (!new_port)
		return -ENOMEM;

	node = vm_list_last(&client_ports);
	if (!node)
		port_id = VM_SOCKET_MIN_PORT_ID;
	else {
		tmp_port = vm_list_entry(node, test_port_type, list_item);
		if (tmp_port->port_id > VM_SOCKET_MAX_PORT_ID || tmp_port->port_id < VM_SOCKET_MIN_PORT_ID)
			port_id = VM_SOCKET_MIN_PORT_ID;
		else
			port_id = tmp_port->port_id + 1;
	}

	new_port->port_id = port_id;
	vm_list_append(&client_ports, &new_port->list_item);

	return new_port->port_id;
}

/* Check open API argument santization */
static int open_fuzzing(int idx)
{
	int fd;

	fd = vm_socket(0xDEAD, SOCK_DGRAM, 0);
	if (fd > 0)
		return -EINVAL;

	fd = vm_socket(AF_VSOCK, 0xDEAD, 0);
	if (fd > 0)
		return -EINVAL;

	fd = vm_socket(AF_VSOCK, SOCK_DGRAM, 0xDEAD);
	if (fd > 0)
		return -EINVAL;

	fd = vm_socket(AF_VSOCK, SOCK_DGRAM, 0);
	if (fd <= 0)
		return -EINVAL;

	vm_close(fd);
	vm_socket_diagnostics();

	return 0;
}

/* Attempts to crash device by fuzzing close API */
static int close_fuzzing(int idx)
{
	vm_close(-1);
	vm_close(0XDEAD);
	vm_close(0);
	vm_close(1);

	return 0;
}

/* Check bind API argument santization */
static int bind_fuzzing(int idx)
{
	struct vm_sockaddr addr;
	int fd;
	int rc;

	fd = vm_socket(AF_VSOCK, SOCK_DGRAM, 0);
	if (fd <= 0)
		return -EINVAL;

	memset(&addr, 0, sizeof(addr));
	rc = vm_bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (!rc) {
		pr_err("failed empty bind test %d", rc);
		goto fail;
	}

	/* Try to bind with invalid port */
	memset(&addr, 0, sizeof(addr));
	addr.svm_family = AF_VSOCK;
	addr.vm_port.node_id = VMADDR_CID_LOCAL;
	addr.vm_port.port_id = 22;
	rc = vm_bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (!rc) {
		pr_err("failed invalid port bind test %d", rc);
		goto fail;
	}

	/* Try to bind with invalid node */
	memset(&addr, 0, sizeof(addr));
	addr.svm_family = AF_VSOCK;
	addr.vm_port.node_id = 22;
	addr.vm_port.port_id = assign_port_id();
	if (addr.vm_port.port_id == -ENOMEM) {
		pr_err("%s: failed to assign port\n", __func__);
		goto fail;
	}
	rc = vm_bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (!rc) {
		pr_err("failed invalid node bind test %d", rc);
		goto fail;
	}

	/* Try to bind to server port already bound */
	memset(&addr, 0, sizeof(addr));
	addr.svm_family = AF_VSOCK;
	addr.vm_port.node_id = VMADDR_CID_LOCAL;
	addr.vm_port.port_id = VM_SOCKET_TEST_SERVER_PORT;
	rc = vm_bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (!rc) {
		pr_err("failed duplicate bind test %d", rc);
		goto fail;
	}

	/* Try to bind to valid port, should succeed */
	memset(&addr, 0, sizeof(addr));
	addr.svm_family = AF_VSOCK;
	addr.vm_port.node_id = VMADDR_CID_LOCAL;
	addr.vm_port.port_id = assign_port_id();
	if (addr.vm_port.port_id == -ENOMEM) {
		pr_err("%s: failed to assign port\n", __func__);
		goto fail;
	}
	rc = vm_bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (rc) {
		pr_err("failed valid bind test %d", rc);
		goto fail;
	}

	/* Try to bind with anyport */
	memset(&addr, 0, sizeof(addr));
	addr.svm_family = AF_VSOCK;
	addr.vm_port.node_id = VMADDR_CID_LOCAL;
	addr.vm_port.port_id = VMADDR_PORT_ANY;
	rc = vm_bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (rc) {
		pr_err("failed any bind test %d", rc);
		goto fail;
	}

	vm_close(fd);
	return 0;

fail:
	vm_close(fd);
	return -EINVAL;
}

/* Starts a server and waits for one message */
static int client_hello(int idx)
{
	struct vm_sockaddr addr;
	socklen_t addrlen;
	ssize_t len;
	int ret;
	int fd;
	char *hello = "Hello from client";

	fd = vm_socket(AF_VSOCK, SOCK_DGRAM, 0);
	if (fd <= 0)
		return -EINVAL;

	addr.svm_family = AF_VSOCK;
	addr.vm_port.node_id = VMADDR_CID_LOCAL;
	addr.vm_port.port_id = assign_port_id();
	if (addr.vm_port.port_id == -ENOMEM) {
		pr_err("%s: failed to assign port\n", __func__);
		goto fail;
	}
	ret = vm_bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (ret) {
		pr_err("failed to bind to addr:[0x%x:0x%x]\n", addr.vm_port.node_id, addr.vm_port.port_id);
		goto fail;
	}

	addr.vm_port.node_id = CID_PEER;
	addr.vm_port.port_id = VM_SOCKET_TEST_SERVER_PORT;
	addrlen = sizeof(addr);
	len = vm_sendto(fd, hello, strlen(hello), 0, (struct sockaddr *)&addr, addrlen);
	if (len <= 0) {
		pr_err("failed to send to addr:[0x%x:0x%x] %d\n", addr.vm_port.node_id, addr.vm_port.port_id, len);
		goto fail;
	}
	pr_err("%s: Sent %u bytes to [0x%x:0x%x]\n", __func__, len, addr.vm_port.node_id, addr.vm_port.port_id);

	vm_close(fd);
	return 0;

fail:
	vm_close(fd);
	return -EINVAL;
}

static int client_fragment(int idx)
{
	struct vm_sockaddr addr;
	socklen_t addrlen;
	ssize_t len;
	int ret;
	int fd;
	char buf[VM_SOCKET_TEST_LARGE_PKT_SIZE];

	fd = vm_socket(AF_VSOCK, SOCK_DGRAM, 0);
	if (fd <= 0)
		return -EINVAL;

	addr.svm_family = AF_VSOCK;
	addr.vm_port.node_id = VMADDR_CID_LOCAL;
	addr.vm_port.port_id = assign_port_id();
	if (addr.vm_port.port_id == -ENOMEM) {
		pr_err("%s: failed to assign port\n", __func__);
		goto fail;
	}
	ret = vm_bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (ret) {
		pr_err("failed to bind to addr:[0x%x:0x%x]\n", addr.vm_port.node_id, addr.vm_port.port_id);
		goto fail;
	}

	addr.vm_port.node_id = CID_PEER;
	addr.vm_port.port_id = VM_SOCKET_TEST_SERVER_PORT;
	addrlen = sizeof(addr);
	get_random_bytes(buf, VM_SOCKET_TEST_LARGE_PKT_SIZE);
	len = vm_sendto(fd, buf, sizeof(buf), 0, (struct sockaddr *)&addr, addrlen);
	if (len <= 0) {
		pr_err("failed to send to addr:[0x%x:0x%x] %d\n", addr.vm_port.node_id, addr.vm_port.port_id, len);
		goto fail;
	}
	pr_err("%s: Sent %u bytes to [0x%x:0x%x]\n", __func__, len, addr.vm_port.node_id, addr.vm_port.port_id);

	vm_close(fd);
	return 0;

fail:
	vm_close(fd);
	return -EINVAL;
}

static int client_loopback(int idx)
{
	struct vm_sockaddr addr;
	char send_buf[VM_SOCKET_TEST_LARGE_PKT_SIZE];
	char recv_buf[VM_SOCKET_TEST_LARGE_PKT_SIZE];
	socklen_t addrlen;
	ssize_t send_len;
	ssize_t recv_len;
	int ret;
	int fd;

	fd = vm_socket(AF_VSOCK, SOCK_DGRAM, 0);
	if (fd <= 0)
		return -EINVAL;

	addr.svm_family = AF_VSOCK;
	addr.vm_port.node_id = VMADDR_CID_LOCAL;
	addr.vm_port.port_id = assign_port_id();
	if (addr.vm_port.port_id == -ENOMEM) {
		pr_err("%s: failed to assign port\n", __func__);
		goto fail;
	}
	ret = vm_bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (ret) {
		pr_err("failed to bind to addr:[0x%x:0x%x]\n", addr.vm_port.node_id, addr.vm_port.port_id);
		goto fail;
	}

	addr.vm_port.node_id = CID_PEER;
	addr.vm_port.port_id = VM_SOCKET_TEST_SERVER_PORT;
	addrlen = sizeof(addr);
	get_random_bytes(send_buf, VM_SOCKET_TEST_LARGE_PKT_SIZE);
	send_len = vm_sendto(fd, send_buf, sizeof(send_buf), 0, (struct sockaddr *)&addr, addrlen);
	if (send_len <= 0) {
		pr_err("failed to send to addr:[0x%x:0x%x] %d\n", addr.vm_port.node_id, addr.vm_port.port_id, send_len);
		goto fail;
	}
	pr_err("%s: Sent %u bytes to [0x%x:0x%x]\n", __func__, send_len, addr.vm_port.node_id, addr.vm_port.port_id);

	recv_len = vm_recvfrom(fd, recv_buf, MAX_MSG_SIZE, 0, (struct sockaddr *)&addr, &addrlen);
	pr_err("%s: Received %u bytes from [0x%x:0x%x]\n", __func__, recv_len, addr.vm_port.node_id, addr.vm_port.port_id);
	if (recv_len != send_len) {
		pr_err("server did not loopback correctly sent:%d recv:%d\n", send_len, recv_len);
		goto fail;
	}

	ret = memcmp(send_buf, recv_buf, VM_SOCKET_TEST_LARGE_PKT_SIZE);
	if (ret) {
		pr_err("memcmp failed ret:%d\n", ret);
		goto fail;
	}

	vm_close(fd);
	return 0;

fail:
	vm_close(fd);
	return -EINVAL;
}

static int (*test_cases[])(int) = {
	open_fuzzing,
	close_fuzzing,
	bind_fuzzing,
	client_hello,
	client_fragment,
	client_loopback,
};
static int num_tests = sizeof(test_cases) / sizeof(*test_cases);

static ssize_t test_case_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int i;

	for (i = 0; i < num_tests; i++) {
		pr_err("Testcase %d: %ps\n", i, test_cases[i]);
	}
	pr_err("\n");

	return 0;
}

static ssize_t test_case_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t len)
{
	u32 cmd;
	int ret;

	if (kstrtou32(buf, 0, &cmd)) {
		pr_err("%s: failed to read cmd from string\n", __func__);
		return len;
	}

	if (cmd >= num_tests) {
		pr_err("Invalid testcase entered\n");
		return len;
	}

	ret = (*test_cases[cmd])(cmd);
	if (ret)
		pr_err("Testcase %d: %ps: Fail!\n", cmd, test_cases[cmd]);
	else
		pr_err("Testcase %d: %ps: Success!\n", cmd, test_cases[cmd]);

	return len;
}
static DEVICE_ATTR_RW(test_case);

static int server_loop(void *data)
{
	struct vm_sockaddr addr;
	socklen_t addrlen;
	ssize_t len;
	int ret;
	int fd;
	char buf[MAX_MSG_SIZE];

	fd = vm_socket(AF_VSOCK, SOCK_DGRAM, 0);
	if (fd <= 0) {
		pr_err("failed to open socket for loopback server %d\n", fd);
		return -EINVAL;
	}

	addr.svm_family = AF_VSOCK;
	addr.vm_port.node_id = VMADDR_CID_LOCAL;
	addr.vm_port.port_id = VM_SOCKET_TEST_SERVER_PORT;
	ret = vm_bind(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (ret) {
		pr_err("failed to bind to addr:[0x%x:0x%x]\n", addr.vm_port.node_id, addr.vm_port.port_id);
		vm_close(fd);
		return -EINVAL;
	}

	while (1) {
		len = vm_recvfrom(fd, buf, MAX_MSG_SIZE, 0, (struct sockaddr *)&addr, &addrlen);
		pr_err("%s: Received %u bytes from [0x%x:0x%x]\n", __func__, len, addr.vm_port.node_id, addr.vm_port.port_id);
		if (len <= 0)
			continue;

		len = vm_sendto(fd, buf, len, 0, (struct sockaddr *)&addr, addrlen);
		if (len <= 0) {
			pr_err("failed to send to addr:[0x%x:0x%x] %d\n", addr.vm_port.node_id, addr.vm_port.port_id, len);
			continue;
		}
		pr_err("%s: Sent %u bytes to [0x%x:0x%x]\n", __func__, len, addr.vm_port.node_id, addr.vm_port.port_id);
	}
	vm_close(fd);

	return 0;
}

static int vmsocket_test_probe(struct platform_device *pdev)
{
	int ret;

	vm_socket_init();

	/* Initialize global state */
	tdev.pdev = pdev;
	tdev.task = kthread_run(server_loop, NULL, "vmsocket_lbserver");
	if (IS_ERR(tdev.task)) {
		pr_err("%s: failed to spawn kthread %ld\n", __func__, PTR_ERR(tdev.task));
		return PTR_ERR(tdev.task);
	}

	ret = device_create_file(&pdev->dev, &dev_attr_test_case);
	if (ret)
		dev_err(&pdev->dev, "Couldn't create sysfs attribute\n");

	return 0;
}

static const struct of_device_id match_tbl[] = {
	{.compatible = "qcom,vmsocket-test"},
	{},
};

static struct platform_driver vmsocket_test_driver = {
	.probe = vmsocket_test_probe,
	.driver = {
		.name = "vmsocket-test",
		.suppress_bind_attrs = true,
		.of_match_table = match_tbl,
	},
};
builtin_platform_driver(vmsocket_test_driver);

MODULE_LICENSE("Dual BSD/GPL");
