// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#ifndef _VM_LIST_H_
#define _VM_LIST_H_

#ifndef offsetof
#define offsetof(type, md) ((unsigned long)&((type *)0)->md)
#endif

#ifndef container_of
#define container_of(ptr, type, member) \
	((type *)((char *)(ptr) - offsetof(type, member)))
#endif

struct vm_list_item {
	struct vm_list_item *next;
	struct vm_list_item *prev;
};

struct vm_list {
	struct vm_list_item *head;
	struct vm_list_item *tail;
};

#define VM_LIST_INIT(name) { 0, 0 }

#define VM_LIST(name) \
	struct vm_list name = VM_LIST_INIT(name)

#define vm_list_entry(ptr, type, member) \
	container_of(ptr, type, member)

static inline void vm_list_init(struct vm_list *vm_list)
{
	vm_list->head = 0;
	vm_list->tail = 0;
}

static inline void vm_list_append(struct vm_list *vm_list, struct vm_list_item *item)
{
	item->next = 0;
	item->prev = vm_list->tail;
	if (vm_list->tail != 0)
		vm_list->tail->next = item;
	else
		vm_list->head = item;
	vm_list->tail = item;
}

static inline void vm_list_prepend(struct vm_list *vm_list, struct vm_list_item *item)
{
	item->prev = 0;
	item->next = vm_list->head;
	if (vm_list->head == 0)
		vm_list->tail = item;
	vm_list->head = item;
}

static inline void vm_list_insert(struct vm_list *vm_list, struct vm_list_item *after, struct vm_list_item *item)
{
	if (after == 0) {
		vm_list_prepend(vm_list, item);
		return;
	}
	item->prev = after;
	item->next = after->next;
	after->next = item;
	if (item->next)
		item->next->prev = item;
	if (vm_list->tail == after)
		vm_list->tail = item;
}

static inline void vm_list_remove(struct vm_list *vm_list, struct vm_list_item *item)
{
	if (item->next)
		item->next->prev = item->prev;
	if (vm_list->head == item) {
		vm_list->head = item->next;
		if (vm_list->head == 0)
			vm_list->tail = 0;
	} else {
		item->prev->next = item->next;
		if (vm_list->tail == item)
			vm_list->tail = item->prev;
	}
	item->prev = item->next = 0;
}

static inline struct vm_list_item *vm_list_pop(struct vm_list *vm_list)
{
	struct vm_list_item *item;
	item = vm_list->head;
	if (item == 0)
		return 0;
	vm_list_remove(vm_list, item);
	return item;
}

static inline struct vm_list_item *vm_list_last(struct vm_list *vm_list)
{
	return vm_list->tail;
}

static inline struct vm_list_item *vm_list_first(struct vm_list *vm_list)
{
	return vm_list->head;
}


static inline struct vm_list_item *vm_list_next(struct vm_list_item *item)
{
	return item->next;
}

#define vm_list_push vm_list_append

#define vm_list_for_each(_vm_list, _iter) \
	for (_iter = (_vm_list)->head; (_iter) != 0; _iter = (_iter)->next)

#define vm_list_for_each_after(_node, _iter) \
	for (_iter = (_node)->next; (_iter) != 0; _iter = (_iter)->next)

#define vm_list_for_each_safe(_vm_list, _iter, _bkup) \
	for (_iter = (_vm_list)->head; (_iter) != 0 && ((_bkup = (_iter)->next) || 1); _iter = (_bkup))

#define vm_list_for_each_safe_after(_node, _iter, _bkup) \
	for (_iter = (_node)->next; (_iter) != 0 && ((_bkup = (_iter)->next) || 1); _iter = (_bkup))

static inline unsigned int vm_list_count(struct vm_list *vm_list)
{
	struct vm_list_item *node;
	unsigned int count = 0;
	vm_list_for_each(vm_list, node)
	{
		count++;
	}
	return count;
}

#endif
