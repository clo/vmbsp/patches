// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#include "vm_socket.h"
#include "vm_osal.h"
#include "vm_socket_osal.h"
#include "vm_socket_transport.h"
#include "vm_socket_internal.h"
#include "vm_list.h"

static unsigned int _vm_socket_transport_pipe_len = 0;

typedef enum
{
  VM_SOCKET_BIND_ADDR_TYPE,
  VM_SOCKET_CONN_ADDR_TYPE
} addr_type_enum;

/*for active sockets book-keeping*/
typedef struct
{
  /*socket list related*/
  unsigned int num_active_sockets;
  VM_SOCKET_OSAL_MUTEX socket_list_mutex;
  struct vm_list active_sockets;
  unsigned int next_fd;

  VM_SOCKET_OSAL_MUTEX socket_tx_mutex;
} vm_sockets_info_type;

static vm_sockets_info_type vm_sockets_info;

static int _vm_socket_check_addr_validity
(
  vm_socket_type* socket,
  addr_type_enum addr_type
)
{
  if(!socket)
  {
    return VM_SOCKET_ERR_NO_SOCKET_FOUND;
  }

  if(addr_type == VM_SOCKET_BIND_ADDR_TYPE)
  {
    if(socket->bind_addr.node_id == VM_SOCKETS_OSAL_INVALID
       || socket->bind_addr.port_id == VM_SOCKETS_OSAL_INVALID)
    {
      return VM_SOCKET_ERR_INVALID_BIND_ADDR;
    }
  }
  else if(addr_type == VM_SOCKET_CONN_ADDR_TYPE)
  {
    if(socket->connected_addr.node_id == VM_SOCKETS_OSAL_INVALID
       || socket->connected_addr.port_id == VM_SOCKETS_OSAL_INVALID)
    {
      return VM_SOCKET_ERR_INVALID_CONN_ADDR;
    }
  }
  else //for default case
  {
    return VM_SOCKET_ERR_INVALID_BIND_ADDR;
  }

  return VM_SOCKET_ERR_SUCCESS;
}

static int _vm_socket_extract_header
(
  unsigned char* pkt,
  unsigned int   len,
  vm_socket_header_type* extracted_header
)
{
  if(!pkt
     || !extracted_header
     || len < sizeof(vm_socket_header_type))

  {
    VM_SOCKET_OSAL_PRINT_RX("RX: Error extracting header\n");
    VM_SOCKET_OSAL_MEMSET(extracted_header,0, sizeof(vm_socket_header_type));
    return -1;
  }

  VM_SOCKET_OSAL_MEMCPY(extracted_header,
                        pkt,
                        sizeof(vm_socket_header_type));
  return VM_SOCKET_ERR_SUCCESS;
}

static void _vm_socket_populate_header
(
  vm_socket_header_type* header,
  vm_socket_type* socket,
  unsigned long int len,
  vm_port_addr* dest_addr_override
)
{
  if(!header || !socket)
  {
    return;
  }

  header->version =0x1;
  header->type = 0x1;
  header->src_node_id = socket->bind_addr.node_id;
  header->src_port_id = socket->bind_addr.port_id;
  header->size = len;
  if(dest_addr_override)
  {
    VM_SOCKET_OSAL_PRINT_DEBUG("TX: using supplied address 0x%x:0x%x\n",dest_addr_override->node_id,dest_addr_override->port_id);
    header->dst_node_id = dest_addr_override->node_id;
    header->dst_port_id = dest_addr_override->port_id;
  }
  else //use the dest addr from socket
  {
    header->dst_node_id = socket->connected_addr.node_id;
    header->dst_port_id = socket->connected_addr.port_id;
  }
}

static int _vm_socket_is_port_id_available
(
  unsigned int port_id
)
{
  vm_socket_type *socket;
  struct vm_list_item *node;
  int res = 0;

  vm_osal_mutex_lock(&vm_sockets_info.socket_list_mutex);
  vm_list_for_each(&vm_sockets_info.active_sockets, node)
  {
    socket = vm_list_entry(node, vm_socket_type, list_item);

    if(socket->bind_addr.port_id == port_id)
    {
      VM_SOCKET_OSAL_PRINT_ERROR("ERROR: port_id %d is already taken by fd = %d not available \n",
                           port_id,socket->fd);
      res = -1;
      break;
    }
  }
  vm_osal_mutex_unlock(&vm_sockets_info.socket_list_mutex);
  return res;
}

static int _vm_socket_validate_addr_for_bind
(
  const struct sockaddr *addr,
  socklen_t addrlen
)
{
  struct vm_sockaddr* vm_sock = (struct vm_sockaddr*)addr;

  if(vm_sock)
  {
    if(addrlen != sizeof(*vm_sock))
    {
      return VM_SOCKET_ERR_INCORRECT_ADDR_LEN;
    }
    if(addr->sa_family != AF_VSOCK)
    {
      return VM_SOCKET_ERR_INVALID_ADDR_FAMILY;
    }

    //check for port id
    if((vm_sock->vm_port.port_id < VM_SOCKET_MIN_PORT_ID
       || vm_sock->vm_port.port_id > VM_SOCKET_MAX_PORT_ID)
       && vm_sock->vm_port.port_id != VMADDR_PORT_ANY)
    {
      VM_SOCKET_OSAL_PRINT_ERROR("error binding port for bind = %d, min:%d max:%d\n",vm_sock->vm_port.port_id, VM_SOCKET_MIN_PORT_ID, VM_SOCKET_MAX_PORT_ID);
      return VM_SOCKET_ERR_INVALID_PORT_ID;
    }

    /* check for node id. As of now, only
     * VMADDR_CID_ANY and VMADDR_CID_LOCAL supported
     */
    if(vm_sock->vm_port.node_id != VMADDR_CID_ANY
       && vm_sock->vm_port.node_id != VMADDR_CID_LOCAL)
    {
      VM_SOCKET_OSAL_PRINT_ERROR("error binding node for bind = %d\n",vm_sock->vm_port.node_id);
      return VM_SOCKET_ERR_INVALID_NODE_ID;
    }

  }

  return VM_SOCKET_ERR_SUCCESS;
}

static int _vm_socket_validate_addr_for_connect
(
  const struct sockaddr *addr,
  socklen_t addrlen
)
{
  struct vm_sockaddr* vm_sock = (struct vm_sockaddr*)addr;

  if(vm_sock)
  {
    if(addrlen != sizeof(*vm_sock))
    {
      return VM_SOCKET_ERR_INCORRECT_ADDR_LEN;
    }
    if(addr->sa_family != AF_VSOCK)
    {
      return VM_SOCKET_ERR_INVALID_ADDR_FAMILY;
    }

    //check for port id
    if(vm_sock->vm_port.port_id < VM_SOCKET_MIN_PORT_ID
       || vm_sock->vm_port.port_id > VM_SOCKET_MAX_PORT_ID)
    {
      VM_SOCKET_OSAL_PRINT_ERROR("error binding port for connect = %d\n",vm_sock->vm_port.port_id );
      return VM_SOCKET_ERR_INVALID_PORT_ID;
    }

    /* check for node id.
     * CID_ANY is not supported. for connected address, specific cid is needed
     * CID_LOCAL is not supported as of now since there is no loopback functionality
     * implemented
     */
    if(vm_sock->vm_port.node_id == VMADDR_CID_ANY)
    {
      VM_SOCKET_OSAL_PRINT_ERROR("error binding node for connect = %d\n",vm_sock->vm_port.node_id );
      return VM_SOCKET_ERR_INVALID_NODE_ID;
    }

  }

  return VM_SOCKET_ERR_SUCCESS;
}

void vm_socket_init
(
  void
)
{

  VM_SOCKET_OSAL_PRINT_DEBUG("DEBUG: vm_socket_init called\n");
  vm_osal_mutex_init(&vm_sockets_info.socket_list_mutex, NULL);
  vm_sockets_info.num_active_sockets = 0;
  vm_sockets_info.next_fd = 0;
  vm_list_init(&vm_sockets_info.active_sockets);

  vm_osal_mutex_init(&vm_sockets_info.socket_tx_mutex, NULL);

  /*initialize the vm_socket transport */
  vm_socket_transport_init();

  /*get the transport length*/
  _vm_socket_transport_pipe_len = vm_socket_get_transport_max_len(NULL);

  return;
}

int _vm_socket_fd_in_use
(
  int fd
)
{
  vm_socket_type *socket;
  struct vm_list_item *node;
  int res = 0;

  vm_osal_mutex_lock(&vm_sockets_info.socket_list_mutex);
  vm_list_for_each(&vm_sockets_info.active_sockets, node)
  {
    socket = vm_list_entry(node, vm_socket_type, list_item);
    if(socket->fd == fd)
    {
      VM_SOCKET_OSAL_PRINT_ERROR("ERROR: fd %d is already taken\n", fd);
      res = 1;
      break;
    }
  }
  vm_osal_mutex_unlock(&vm_sockets_info.socket_list_mutex);
  return res;
}

void vm_socket_inc_ref_count
(
  vm_socket_type* sock
)
{
  if(!sock) 
  {
    return;
  }
  vm_osal_mutex_lock(&sock->ref_count_mutex);
  sock->ref_count++;
  vm_osal_mutex_unlock(&sock->ref_count_mutex);

  return;
}

void vm_socket_dec_ref_count
(
  vm_socket_type** sock_param
)
{

  vm_socket_type* sock = *sock_param;
  if(! sock) 
  {
    return;  
  }

  vm_osal_mutex_lock(&sock->ref_count_mutex);
  if(sock->ref_count > 0) 
  {
    sock->ref_count--;
  }
  vm_osal_mutex_unlock(&sock->ref_count_mutex);

  if(sock->ref_count == 0) 
  {
    vm_osal_mutex_lock(&vm_sockets_info.socket_list_mutex);
    vm_list_remove(&vm_sockets_info.active_sockets, &sock->list_item);
    vm_socket_osal_free(sock);   
    *sock_param = NULL;
    if(vm_sockets_info.num_active_sockets) 
    {
      vm_sockets_info.num_active_sockets--;         
    }
    vm_osal_mutex_unlock(&vm_sockets_info.socket_list_mutex);    
  }

  return;
}

int vm_socket
(
  int family,
  int type,
  int protocol
)
{
  unsigned int fd_counter = 0;
  vm_socket_type* sock = NULL;

  if(AF_VSOCK != family || SOCK_DGRAM != type || protocol != 0 )
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Creation of socket failed. family = %d, type = %d, protocol = %d\n",
                         family, type, protocol);
    return VM_SOCKET_ERR_INVALID_ADDR_FAMILY;
  }


  /*malloc the socket and initialize the parameters*/
  sock = (vm_socket_type*)vm_socket_osal_malloc(sizeof(vm_socket_type));
  if(!sock)
  {
    return VM_SOCKET_ERR_NO_MEM;
  }

  do
  {
    vm_sockets_info.next_fd = ((vm_sockets_info.next_fd + 1) % VM_SOCKET_MAX_FD);
    if(vm_sockets_info.next_fd == 0)
    {
      vm_sockets_info.next_fd++;
    }
    fd_counter++;
  }while(_vm_socket_fd_in_use(vm_sockets_info.next_fd) && fd_counter < VM_SOCKET_MAX_FD);

  if(fd_counter == VM_SOCKET_MAX_FD)
  {
    vm_socket_osal_free(sock);
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Socket Limit exceeded\n");
    return VM_SOCKET_ERR_FDS_MAX_LIMIT;
  }

  /*see if the fd is in use or not*/
  sock->fd = vm_sockets_info.next_fd;
  sock->connected_addr.node_id = VM_SOCKETS_OSAL_INVALID;
  sock->connected_addr.port_id = VM_SOCKETS_OSAL_INVALID;
  sock->bind_addr.node_id = VM_SOCKETS_OSAL_INVALID;
  sock->bind_addr.port_id = VM_SOCKETS_OSAL_INVALID;
  vm_list_init(&sock->rx_q);
  vm_osal_sig_init(&sock->socket_rx_wait_cond, NULL);
  vm_osal_mutex_init(&sock->socket_rx_wait_cond_mutex,NULL);
  vm_osal_mutex_init(&sock->socket_rx_q_mutex, NULL);
  vm_osal_mutex_init(&sock->ref_count_mutex, NULL);

  vm_osal_mutex_lock(&vm_sockets_info.socket_list_mutex);
  vm_list_append(&vm_sockets_info.active_sockets, &sock->list_item);
  vm_sockets_info.num_active_sockets++;
  vm_osal_mutex_unlock(&vm_sockets_info.socket_list_mutex);

  //Take the ref count for the socket that is just opened
  vm_socket_inc_ref_count(sock);

  return sock->fd;
}

void vm_socket_diagnostics(void)
{
 vm_socket_type* socket;
 struct vm_list_item *node;

 vm_list_for_each(&vm_sockets_info.active_sockets, node)
 {
   socket = vm_list_entry(node, vm_socket_type, list_item);
   VM_SOCKET_OSAL_PRINT_DEBUG("DEBUG: socket fd = %d bind = %d:%d connected = %d:%d \n",
                              socket->fd, socket->bind_addr.node_id,
                              socket->bind_addr.port_id,
                              socket->connected_addr.node_id,
                              socket->connected_addr.port_id);
 }
}

static vm_socket_type* _vm_get_socket_for_addr
(
 addr_type_enum addr_type,
 vm_port_addr*  addr
)
{
  vm_socket_type* socket;
  struct vm_list_item *node;
  unsigned int found = 0;

  if(addr_type != VM_SOCKET_BIND_ADDR_TYPE
     && addr_type != VM_SOCKET_CONN_ADDR_TYPE)
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: returned _vm_get_socket_for_addr\n" );
    return NULL;
  }

  vm_osal_mutex_lock(&vm_sockets_info.socket_list_mutex);
  vm_list_for_each(&vm_sockets_info.active_sockets, node)
  {
   socket = vm_list_entry(node, vm_socket_type , list_item);

   VM_SOCKET_OSAL_PRINT_DEBUG("DEBUG: socket->bind_addr = %d:%d\n",
                        socket->bind_addr.node_id,
                        socket->bind_addr.port_id);
   if((addr_type == VM_SOCKET_BIND_ADDR_TYPE)
      && (socket->bind_addr.port_id == addr->port_id))
   {
     found = 1;
     break;
   }
   else if((addr_type == VM_SOCKET_CONN_ADDR_TYPE)
      && (socket->connected_addr.port_id == addr->port_id))
   {
     found = 1;
     break;
   }
  }
  vm_osal_mutex_unlock(&vm_sockets_info.socket_list_mutex);

  if(found)
  {
    return socket;
  }
  else
  {
    return NULL;
  }
}


static vm_socket_type* _vm_get_socket_from_fd
(
 int fd
)
{
  vm_socket_type* socket;
  struct vm_list_item *node;
  unsigned int found = 0;

  vm_osal_mutex_lock(&vm_sockets_info.socket_list_mutex);
  vm_list_for_each(&vm_sockets_info.active_sockets, node)
  {
   socket = vm_list_entry(node, vm_socket_type , list_item);
   if(socket->fd == fd)
   {
     found = 1;
     break;
   }
  }
  vm_osal_mutex_unlock(&vm_sockets_info.socket_list_mutex);

  if(found)
  {
    return socket;
  }
  else
  {
    return NULL;
  }
}


void vm_close(int fd)
{
  vm_socket_type* socket = NULL;
  
  socket = _vm_get_socket_from_fd(fd);

  //signal all waiting threads if any
  vm_osal_sig_set(&socket->socket_rx_wait_cond);

  if(socket) 
  {
    vm_socket_dec_ref_count(&socket);  
  }  
}

int vm_connect
(
  int sockfd,
  const struct sockaddr *addr,
  socklen_t addrlen
)
{
  vm_socket_type* socket = NULL;
  struct vm_sockaddr* addr_to_connect;
  int res;

  res = _vm_socket_validate_addr_for_connect(addr,addrlen);
  if(res != VM_SOCKET_ERR_SUCCESS)
  {
    return res;
  }

  addr_to_connect = (struct vm_sockaddr*)addr;

  socket = _vm_get_socket_from_fd(sockfd);

  if(! socket)
  {
    return VM_SOCKET_ERR_NO_SOCKET_FOUND;
  }

  socket->connected_addr.node_id = addr_to_connect->vm_port.node_id;
  socket->connected_addr.port_id = addr_to_connect->vm_port.port_id;

  return VM_SOCKET_ERR_SUCCESS;
}

int vm_bind
(
  int sockfd,
  const struct sockaddr *addr,
  socklen_t addrlen
)
{
  vm_socket_type* socket = NULL;
  struct vm_sockaddr* addr_to_bind;
  unsigned int temp_port_id;
  int res;

  res = _vm_socket_validate_addr_for_bind(addr,addrlen);
  if(res < 0)
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Could not validate address to bind\n");
    return res;
  }

  addr_to_bind = (struct vm_sockaddr*)addr;
  temp_port_id = addr_to_bind->vm_port.port_id;

  socket = _vm_get_socket_from_fd(sockfd);

  if(!socket)
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Could not find socket based on fd\n");
    return VM_SOCKET_ERR_NO_SOCKET_FOUND;
  }

  //if the port id specified is VMADDR_PORT_ANY, then find the next available
  //port id and bind to it.
  if(temp_port_id == VMADDR_PORT_ANY)
  {
    //start searching from min port id
    temp_port_id = VM_SOCKET_MIN_PORT_ID;
    while(temp_port_id <= VM_SOCKET_MAX_PORT_ID)
    {
      if(_vm_socket_is_port_id_available(temp_port_id)== 0)
      {
        //found a valid port id to bind, so break out
        break;
      }
      else
      {
        //check if the next port is available or not
        temp_port_id++;
      }
    }//end while

    if(temp_port_id > VM_SOCKET_MAX_PORT_ID )
    {
      //here meaning we could not find a valid port id to bind
      return VM_SOCKET_ERR_NO_FREE_PORT_AVAILABLE;
    }
  }
  else
  {

    res = _vm_socket_is_port_id_available(temp_port_id);
    if(res < 0)
    {
      VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Bind port 0x%x already taken\n", temp_port_id);
      return VM_SOCKET_ERR_PORT_ALREADY_BOUND;
    }
  }

  socket->bind_addr.node_id = addr_to_bind->vm_port.node_id;
  socket->bind_addr.port_id = temp_port_id;

  return VM_SOCKET_ERR_SUCCESS;
}

static ssize_t _vm_send_int
(
  vm_socket_type* socket,
  const void *buf,
  size_t len,
  int flags,
  vm_port_addr* dest_addr
)
{
  unsigned int frag_count, header_sent, temp_buffer_size,
               buf_rd_ptr,remaining_bytes,
               frag_size;
  unsigned char* temp_buffer = NULL;
  vm_socket_header_type header;
  buf_rd_ptr = 0;
  header_sent = 0;
  frag_count =0;

  if(!socket || !buf)
  {
    return VM_SOCKET_ERR_SUCCESS;
  }

  /*verify if the socket has a bind address*/
  if(socket->bind_addr.node_id == VM_SOCKETS_OSAL_INVALID
     || socket->bind_addr.node_id == VM_SOCKETS_OSAL_INVALID)
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: The socket did not have a bind address\n");
    return 0;
  }

   /*Total length to be sent */
  remaining_bytes = len + sizeof(vm_socket_header_type);
  VM_SOCKET_OSAL_PRINT_DEBUG("TX: Total bytes to tx %d\n",remaining_bytes);
  temp_buffer_size = MIN(remaining_bytes,_vm_socket_transport_pipe_len);
  temp_buffer = (unsigned char*)vm_socket_osal_malloc(temp_buffer_size);
  if(! temp_buffer )
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Could not allocate buffer \n");
    return VM_SOCKET_ERR_SUCCESS;
  }

  vm_osal_mutex_lock(&vm_sockets_info.socket_tx_mutex);

  while(remaining_bytes)
  {
    /*Keep track of fragments for diagnostic purposes*/
    frag_count++;

    /*Calculate fragment size*/
    frag_size = MIN(remaining_bytes, _vm_socket_transport_pipe_len);

    VM_SOCKET_OSAL_PRINT_DEBUG("TX: Frag %d, frag_size %d, header_sent %d\n",frag_count,frag_size,header_sent);

    if(!header_sent)
    {
      _vm_socket_populate_header(&header,socket,len,dest_addr);
      /*copy over the header*/
      VM_SOCKET_OSAL_MEMCPY(temp_buffer, &header, sizeof(header));

      VM_SOCKET_OSAL_MEMCPY(temp_buffer+sizeof(header),buf,frag_size-sizeof(header));
      buf_rd_ptr += frag_size-sizeof(header);
      header_sent = 1;
    }
    else
    {
      VM_SOCKET_OSAL_MEMCPY(temp_buffer,buf+buf_rd_ptr,frag_size);
      buf_rd_ptr += frag_size;
    }

    remaining_bytes -= frag_size;
    VM_SOCKET_OSAL_PRINT_DEBUG("TX: buf_rd_ptr = %d, remaining_bytes %d\n", buf_rd_ptr,remaining_bytes);
    vm_socket_transport_send(temp_buffer,frag_size);
  }//end while
  VM_SOCKET_OSAL_PRINT_TX("TX: Sent 0x%x bytes\n", (uint32_t)len);

  //Free the temp_buffer
  vm_socket_osal_free(temp_buffer);

  //Let go of TX MUTEX
  vm_osal_mutex_unlock(&vm_sockets_info.socket_tx_mutex);

  return len;
}

ssize_t vm_send
(
  int sockfd,
  const void *buf,
  size_t len,
  int flags
)
{
  vm_socket_type* socket = NULL;
  int res;

  socket = _vm_get_socket_from_fd(sockfd);

  if(!socket || !buf)
  {
    return VM_SOCKET_ERR_SUCCESS;
  }

  res = _vm_socket_check_addr_validity(socket, VM_SOCKET_BIND_ADDR_TYPE);

  if(res < VM_SOCKET_ERR_SUCCESS)
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Socket fd = %d does not have a bind address %d\n",sockfd,__LINE__);
    return 0;
  }

  res = _vm_socket_check_addr_validity(socket, VM_SOCKET_CONN_ADDR_TYPE);

  if(res < VM_SOCKET_ERR_SUCCESS)
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Socket fd = %d does not have a connected address\n",sockfd);
    return 0;
  }

  return _vm_send_int(socket, buf, len, flags, NULL);
}

ssize_t vm_sendto
(
  int sockfd,
  const void *buf,
  size_t len,
  int flags,
  const struct sockaddr *dest_addr,
  socklen_t addrlen
)
{
  vm_socket_type* socket = NULL;
  struct vm_sockaddr *vm_dest_addr = (struct vm_sockaddr *)dest_addr;
  vm_port_addr dest_port_addr = { 0 };

  socket = _vm_get_socket_from_fd(sockfd);

  if(!socket || !buf || !dest_addr)
  {
    return VM_SOCKET_ERR_SUCCESS;
  }

  //no need to check for conn addr, just check the bind addr
  if(_vm_socket_check_addr_validity(socket, VM_SOCKET_BIND_ADDR_TYPE) < 0)
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Socket fd = %d does not have a bind address %d\n",sockfd,__LINE__);
    return VM_SOCKET_ERR_SUCCESS;
  }

  //populate the dest addr and send it over
  if(vm_dest_addr->svm_family == AF_VSOCK)
  {
    //then cast the rest of the data to vm_port_addr
    dest_port_addr.node_id = vm_dest_addr->vm_port.node_id;
    dest_port_addr.port_id = vm_dest_addr->vm_port.port_id;
    VM_SOCKET_OSAL_PRINT_TX("TX: sendto dest_port_addr:0x%x:0x%x\n",
                            dest_port_addr.node_id, dest_port_addr.port_id);
  }

  return _vm_send_int(socket, buf, len, flags, &dest_port_addr);
}

static ssize_t _vm_recv_int
(
  vm_socket_type* socket,
  void *buf,
  size_t len,
  int flags,
  vm_port_addr* src_addr
)
{
  vm_socket_rx_q_element* rx_item;
  struct vm_list_item *node;
  ssize_t bytes_to_copy = 0;

  if(!buf || !socket)
  {
    return VM_SOCKET_ERR_SUCCESS;
  }

  //grab the rx_q mutex
  vm_osal_mutex_lock(&socket->socket_rx_q_mutex);
  while(socket != NULL) 
  {
    //check if socket's rx_q has any elements
    if(vm_list_count(&socket->rx_q) != 0)
    {
      //first check whether the buffer len supplied is sufficient
      node = vm_list_first(&socket->rx_q);
      rx_item = vm_list_entry(node, vm_socket_rx_q_element, list_item);

      /*if the item len is more than the buffer len and
      if MSG_TRUNC flag is not set, return and do not pop the item*/
      if(!rx_item
         || !rx_item->buf
         || ((rx_item->len > len) && !(flags & MSG_TRUNC)))
      {
         //done with mutex, so release it here
         vm_osal_mutex_unlock(&socket->socket_rx_q_mutex);

         VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Supplied buf len = %lu "
                                    "< Recv pkt len = %u \n",
                                    len, rx_item->len);

         //return 0 since the recvd packet is more than the
         //buffer len and we are not supposed to truncate it
         return 0;
      }
      vm_list_remove(&socket->rx_q, node);

      //done with mutex, so release it here
      vm_osal_mutex_unlock(&socket->socket_rx_q_mutex);

      //copy over to buf after truncating, if necessary
      bytes_to_copy = MIN(rx_item->len, len);
      VM_SOCKET_OSAL_PRINT_DEBUG("RX: bytes to copy = %lu\n",bytes_to_copy);
      VM_SOCKET_OSAL_MEMCPY(buf, rx_item->buf, bytes_to_copy);

      //if there is a source addr supplied, populate it
      if(src_addr)
      {
        VM_SOCKET_OSAL_MEMCPY(src_addr, &rx_item->src_addr, sizeof(vm_port_addr));
      }

      //Delete the memory pointed by rx_item->buf
      vm_socket_osal_free(rx_item->buf);

      //Delete the rx_ele itself
      vm_socket_osal_free(rx_item);

      break;

    }
    else //else if there is no element in the rx_q
    {
      //first unlock the rx_q_mutex
      vm_osal_mutex_unlock(&socket->socket_rx_q_mutex);

      VM_SOCKET_OSAL_PRINT_DEBUG("RX: no rx_q element found\n");

      /*if we are not supposed to wait, then bail out*/
      if(flags & MSG_DONTWAIT)
      {
        VM_SOCKET_OSAL_PRINT_RX("RX: MSG_NOWAIT set. Return %lu bytes\n", bytes_to_copy);
        break;
      }

      VM_SOCKET_OSAL_PRINT_DEBUG("RX: thread blocked waiting for message...\n");
      //if here then we are supposed to wait
      vm_socket_inc_ref_count(socket);
      vm_osal_sig_wait(&socket->socket_rx_wait_cond,
                              &socket->socket_rx_wait_cond_mutex,
                              (int *)&socket->rx_q_count);

      VM_SOCKET_OSAL_PRINT_DEBUG("RX: thread unblocked...\n");

      vm_socket_dec_ref_count(&socket);
      //It is possible that vm_socket_dec_ref_count might have closed the socket
      //so need to check here
      if(NULL == socket || 0 == socket->rx_q_count) 
      {
        bytes_to_copy = 0;
        break;
      }
    }

  } //end while

  return bytes_to_copy;
}

ssize_t vm_recv
(
  int sockfd,
  void *buf,
  size_t len,
  int flags
)
{
  vm_socket_type* socket = NULL;
  int res;

  if(!buf)
  {
    return VM_SOCKET_ERR_SUCCESS;
  }

  //get the socket from fd
  socket = _vm_get_socket_from_fd(sockfd);

  /*verify if the socket has a bind address*/
  res = _vm_socket_check_addr_validity(socket, VM_SOCKET_BIND_ADDR_TYPE);

  if(res < VM_SOCKET_ERR_SUCCESS)
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Socket fd = %d does not have a bind address %d\n",sockfd,__LINE__);
    return 0;
  }

  return _vm_recv_int(socket,buf,len,flags,NULL);
}

ssize_t vm_recvfrom
(
  int sockfd,
  void * buf,
  size_t len,
  int flags,
  struct sockaddr * src_addr_in,
  socklen_t * addrlen
)
{
  ssize_t bytes_recv;
  vm_socket_type* socket = NULL;
  vm_port_addr sender_src_addr;
  struct vm_sockaddr* src_addr = (struct vm_sockaddr*)src_addr_in;
  int res;

  if(!buf)
  {
    return VM_SOCKET_ERR_SUCCESS;
  }

  //get the socket from fd
  socket = _vm_get_socket_from_fd(sockfd);

  if(!socket)
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: did not find a socket \n");
  }

  //check whether the port is binded
  res = _vm_socket_check_addr_validity(socket, VM_SOCKET_BIND_ADDR_TYPE);

  if(res < VM_SOCKET_ERR_SUCCESS)
  {
    VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Socket fd = %d does not have a bind address %d\n",sockfd,__LINE__);
    return 0;
  }

  bytes_recv = _vm_recv_int(socket,buf,len,flags,&sender_src_addr);

  /*copy the source addr*/
  if(src_addr)
  {
    src_addr->svm_family = AF_VSOCK;
    src_addr->vm_port.node_id = sender_src_addr.node_id;
    src_addr->vm_port.port_id = sender_src_addr.port_id;

    if(addrlen)
    {
      *addrlen = sizeof(src_addr);
    }
  }
  return bytes_recv;
}

void vm_socket_process_rx_buffer
(
  unsigned char* buffer,
  unsigned int   len,
  vm_socket_curr_rx_pkt_info_type* curr_rx_pkt_info
)
{
  vm_socket_header_type header;
  unsigned int bytes_read = 0;
  unsigned int frag_len = 0;
  unsigned int bytes_remaining_in_this_packet = 0;
  vm_port_addr dst_addr;
  vm_socket_type* socket;

  VM_SOCKET_OSAL_PRINT_DEBUG("RX: len = %d, header_received = %d\n",
                              len,curr_rx_pkt_info->header_received);
  /*lock the rx mutex*/
  //check if the header is already received
  if(0 == curr_rx_pkt_info->header_received)
  {
    //This the first fragment of the packet.
    //extract the header
    if(_vm_socket_extract_header(buffer,len,&header) < 0)
    {
      VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Error extracting header\n");
      //error extracting header, we need discard the packet
      goto bail;
    }

    VM_SOCKET_OSAL_PRINT_DEBUG("RX: header addr check  1 src: 0x%x:0x%x dest: 0x%x:0x%x\n",
                                header.src_node_id,header.src_port_id,
                                header.dst_node_id,header.dst_port_id);

    //check for header integrity
    if(header.version != VM_SOCKET_PROTOCOL_VERSION)
    {
      VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Error in header version\n");
      //error extracting header, we need discard the packet
      goto bail;
    }

    //if valid header, then
    curr_rx_pkt_info->total_pkt_len = header.size;
    curr_rx_pkt_info->received_len = 0;

    VM_SOCKET_OSAL_PRINT_DEBUG("RX: Allocated buffer of %u to receive data from transport\n",header.size);
    curr_rx_pkt_info->curr_pkt_ptr =
        (unsigned char*)vm_socket_osal_malloc(curr_rx_pkt_info->total_pkt_len);

    if(!curr_rx_pkt_info->curr_pkt_ptr)
    {
      goto bail;
    }

    curr_rx_pkt_info->src_addr.node_id = header.src_node_id;
    curr_rx_pkt_info->src_addr.port_id = header.src_port_id;
    curr_rx_pkt_info->dest_addr.node_id = header.dst_node_id;
    curr_rx_pkt_info->dest_addr.port_id = header.dst_port_id;
    curr_rx_pkt_info->header_received = 1;

    bytes_read = sizeof(vm_socket_header_type);
  }

  //This is the data fragment length (without the the header, if it is the first packet)
  frag_len = len - bytes_read;

  //These are the bytes remaining to to received for this data packet
  //This is the total bytes that we know from the header - bytes already received till now
  bytes_remaining_in_this_packet = curr_rx_pkt_info->total_pkt_len -
                                   curr_rx_pkt_info->received_len;
  VM_SOCKET_OSAL_PRINT_DEBUG("RX: frag len = %d, remaining_bytes_in_this_packet = %d\n",frag_len,bytes_remaining_in_this_packet);


   //copy over this fragment
  VM_SOCKET_OSAL_MEMCPY( (void*)(curr_rx_pkt_info->curr_pkt_ptr+ curr_rx_pkt_info->received_len),
                           buffer+bytes_read,
                           frag_len);
  curr_rx_pkt_info->received_len += frag_len;
  VM_SOCKET_OSAL_PRINT_DEBUG("RX: Recd = %d bytes till now\n",curr_rx_pkt_info->received_len);

  //check whether the length of the packet is in one frag
  if(frag_len >= bytes_remaining_in_this_packet)
  {

    //The complete packet was received.
    //VM_SOCKET_OSAL_PRINT_RX("RX: Packet completely received\n");

    //try to find the socket whose bind address is the same as destination address
    dst_addr.node_id = curr_rx_pkt_info->dest_addr.node_id;
    dst_addr.port_id = curr_rx_pkt_info->dest_addr.port_id;
    VM_SOCKET_OSAL_MEMSET(&socket, 0, sizeof(socket));
    socket = _vm_get_socket_for_addr(VM_SOCKET_BIND_ADDR_TYPE,&dst_addr);

    if(socket)
    {
      vm_socket_rx_q_element* rx_element;
      //get the mutex for socket's rx_q
      vm_osal_mutex_lock(&socket->socket_rx_q_mutex);

      rx_element = (vm_socket_rx_q_element*)vm_socket_osal_malloc(sizeof(vm_socket_rx_q_element));
      rx_element->buf = (void*)curr_rx_pkt_info->curr_pkt_ptr;
      rx_element->len = (unsigned int)curr_rx_pkt_info->total_pkt_len;
      rx_element->src_addr.node_id = curr_rx_pkt_info->src_addr.node_id;
      rx_element->src_addr.port_id = curr_rx_pkt_info->src_addr.port_id;

      //queue it in the socket's rx_q
      vm_list_append(&socket->rx_q, &rx_element->list_item);
      socket->rx_q_count = vm_list_count(&socket->rx_q);

      //signal the socket thread
      vm_osal_sig_set(&socket->socket_rx_wait_cond);

      vm_osal_mutex_unlock(&socket->socket_rx_q_mutex);
      VM_SOCKET_OSAL_PRINT_RX("RX: Queued buffer port 0x%x len = %d\n",
                              dst_addr.port_id, rx_element->len);
    }
    else //in case we cannot find the socket, we need to free the memory
    {
      //Debug messages
      VM_SOCKET_OSAL_PRINT_RX("RX: ERROR: Could not find a socket structure for %d:%d address",
                            curr_rx_pkt_info->dest_addr.node_id,
                            curr_rx_pkt_info->dest_addr.port_id);

      //in case we do not find the socket, we need free the memory that was allocated to
      //receive the buffer
      if(curr_rx_pkt_info->curr_pkt_ptr)
      {
        vm_socket_osal_free(curr_rx_pkt_info->curr_pkt_ptr);
      }
    }

    //since we received the full packet, memset all variables in curr_rx_pkt_info to zero
    //so that we can start fresh the next time.
    VM_SOCKET_OSAL_MEMSET(curr_rx_pkt_info,0,sizeof(vm_socket_curr_rx_pkt_info_type));
  }

bail:
    return;
}
