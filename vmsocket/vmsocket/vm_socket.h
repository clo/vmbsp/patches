// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#ifndef _VM_SOCKET_H_
#define _VM_SOCKET_H_

#include <stddef.h>
#include "vm_socket_osal.h"
#include "vm_socket_transport.h"

/*=== VM SOCK related defines ====*/
#define AF_VSOCK   1
#define SOCK_DGRAM 2        /* datagram (conn.less) socket	*/
#define PF_UNSPEC  0        /* Unspecified.  */

/*vm sock supported flags*/
#define MSG_TRUNC    0x20
#define MSG_DONTWAIT 0x40   /* don't wait for message */

#define VMADDR_PORT_ANY -1      /* Bind to a random available port. */
#define VMADDR_CID_ANY -1       /* Bind to any CID. This seems to work inside VMs only. */
#define VMADDR_CID_HYPERVISOR	0	/* The hypervisor's CID. */
#define VMADDR_CID_LOCAL 1      /* The local VMs CID */
#define VMADDR_CID_HOST	2       /* The host's CID. */

#define VM_SOCKET_MIN_CID 1
#define VM_SOCKET_MAX_CID 128
#define VM_SOCKET_MIN_PORT_ID 0x4000
#define VM_SOCKET_MAX_PORT_ID 0x7fff
/*=== END VM SOCK related defines ====*/

/*=== VM SOCKET ERROR CODES ====*/
#define VM_SOCKET_ERR_SUCCESS                  0
#define VM_SOCKET_ERR_INVALID_HANDLE          -1
#define VM_SOCKET_ERR_INCORRECT_ADDR_LEN      -2
#define VM_SOCKET_ERR_INVALID_ADDR_FAMILY     -3
#define VM_SOCKET_ERR_INVALID_PORT_ID         -4
#define VM_SOCKET_ERR_INVALID_NODE_ID         -5
#define VM_SOCKET_ERR_INVALID_BIND_ADDR       -6
#define VM_SOCKET_ERR_INVALID_CONN_ADDR       -7
#define VM_SOCKET_ERR_NO_SOCKET_FOUND         -8
#define VM_SOCKET_ERR_NO_FREE_PORT_AVAILABLE  -9
#define VM_SOCKET_ERR_PORT_ALREADY_BOUND      -10
#define VM_SOCKET_ERR_NO_MEM                  -11
#define VM_SOCKET_ERR_FDS_MAX_LIMIT           -12
/*=== END VM SOCKET ERROR CODES ====*/

typedef unsigned int sa_family_t;
typedef unsigned int socklen_t;

struct  __attribute__((__packed__)) sockaddr
{
    sa_family_t   sa_family;       /*address family*/
    char          sa_data[14];     /*socket address (variable-length data)*/
};

typedef struct
{
  unsigned int node_id;
  unsigned int port_id;
} vm_port_addr;

/* VM_SOCK addr type */
struct __attribute__((__packed__)) vm_sockaddr
{
  sa_family_t    svm_family;     /* Address family: AF_VSOCK */
  unsigned short svm_reserved1;
  vm_port_addr   vm_port;

  unsigned char  svm_zero[sizeof(struct sockaddr) -
                          sizeof(sa_family_t) -
                          sizeof(unsigned short) -
                          sizeof(vm_port_addr)];
};

/*
vm_socket_init
Initializes socket book-keeping variables.
This function should be called only once at boot up
in initialization sequence.
Typical users do not need to call this function.

Dependency -- Underlying transport should be initialized
              before this function is called.

Input params: None

Return value: None
*/
void vm_socket_init
(
  void
);


/*
vm_socket
Opens a socket for AF_VSOCK family of sockets.

Input params:
  int family - AF_VSOCK family of sockets supported
  int type - SOCK_DGRAM (Datagram) sockets supported
  int protocol - 0

Return value:
  int - Returns a handle/file descriptor to the
         underlying socket. This handle/fd is used
         for any further socket operations.

         Value of > 0 indicates a successful socket
                      open operation

         Value of < 0 indicates an error.
*/
int vm_socket
(
  int family,
  int type,
  int protocol
);

/*
vm_close
Closes the underlying socket of specified handle/file descriptor.

Input params:
  int -  handle/file descriptor to the
         underlying socket.

Return value:
  void.
*/
void vm_close
(
  int fd
);

/*
vm_connect
Updates the socket's connected address to specified destination address.
This connected address is used during the vm_send operation.
vm_connect call shall be called before vm_send operation.
It is optional to call vm_connect if vm_sendto is used.

Input params:
  int -  handle/file descriptor to the
         underlying socket.
  const struct sockaddr *addr - Pointer to address variable which has
                                the destination address         ,
  socklen_t addrlen -- length of the above struct sockaddr variable

Return value:
  int -
        Value of 0 indicates a successful setting of connected address
        Value of < 0 indicates a failure either due to invalid address
                     or invalid socket fd.
*/
int vm_connect
(
  int sockfd,
  const struct sockaddr *addr,
  socklen_t addrlen
);

/*
vm_bind
Updates the socket's bind address to specified source address.
This bind address is used during the vm_recv and recv_from operations.
vm_bind call shall be called before vm_send, vm_sendto, vm_recv and recv_from
operations.

Input params:
  int -  handle/file descriptor to the
         underlying socket.
  const struct sockaddr *addr - Pointer to address variable which has
                                the bind address. Make sure that the
                                bind address being provided is not already
                                in use, else this function will return an
                                error.         ,
  socklen_t addrlen -- length of the above struct sockaddr variable

Return value:
  int -
        Value of 0 indicates a successful setting of bind address
        Value of < 0 indicates a failure either due to
                        invalid address
                     or already binded address provided as an input
                     or invalid socket fd
*/
int vm_bind
(
  int sockfd,
  const struct sockaddr *addr,
  socklen_t addrlen
);

/*
vm_send
Sends data in user buffer to the destination socket.
vm_connect and vm_bind call shall be called before vm_send.
This call is a blocking call till all data in user buffer is
transmitted. Transmitting larger data than the underlying
transport Maximum Transmission Unit (MTU) will result in
sending user data as multiple fragments and this call will
be blocking call till all fragments are transmitted.

Input params:
  int -  handle/file descriptor to the
         underlying socket.
  const void *buf - Pointer to user data buffer
  size_t len - length of the user data buffer
  int flags - 0. No flags supported.

Return value:
  int -
        Value > 0 indicates a number of bytes successfully transmitted
        Value of 0 indicates an error either due to
            invalid connected address (if vm_connect is not called)
            invalid bind address (if vm_bind is not called)
            any memory allocation failures during the operation
            invalid socket handle
            NULL user data buffer pointer.
*/
ssize_t vm_send
(
  int sockfd,
  const void *buf,
  size_t len,
  int flags
);

/*
vm_sendto
Sends data in user buffer to the destination socket.
vm_bind call shall be called before vm_sendto.
This call is a blocking call till all data in user buffer is
transmitted. Transmitting larger data than the underlying
transport Maximum Transmission Unit (MTU) will result in
sending user data as multiple fragments and this call will
be blocking call till all fragments are transmitted.

Input params:
  int -  handle/file descriptor to the
         underlying socket.
  const void *buf - Pointer to user data buffer
  size_t len - length of the user data buffer
  int flags - 0. No flags supported.
  const struct sockaddr *dest_addr - destination address
  socklen_t addrlen - length of above destination address variable

Return value:
  int -
        Value > 0 indicates a number of bytes successfully transmitted
        Value of 0 indicates an error either due to
            invalid bind address (if vm_bind is not called)
            any memory allocation failures during the operation
            invalid socket handle
            NULL user data buffer pointer.
*/
ssize_t vm_sendto
(
  int sockfd,
  const void *buf,
  size_t len,
  int flags,
  const struct sockaddr *dest_addr,
  socklen_t addrlen
);


/*
vm_recv
Receives data in user supplied buffer from source socket.
vm_bind call shall be called before vm_recv.
This call is blocking call by default. To make it a non-blocking
call, set the input parameter flags value to MSG_DONTWAIT.

Input params:
  int -  handle/file descriptor to the
         underlying socket.
  const void *buf - Pointer to user data buffer
  size_t len - length of the user data buffer
  int flags -
              MSG_TRUNC - This flag will truncate the
                          incoming data to the user supplied
                          buffer length, in case user buffer
                          length is lesser than the length of
                          incoming data.
              MSG_DONTWAIT - This flag will not block vm_recv
                             function in case there is no data
                             in the underlying socket receive
                             queue.

Return value:
  int -
        Value > 0 indicates a number of bytes successfully received
        Value of 0 indicates an error either due to
            invalid bind address (if vm_bind is not called)
            any memory allocation failures during the operation
            invalid socket handle
            NULL user data buffer pointer.
*/
ssize_t vm_recv
(
  int sockfd,
  void *buf,
  size_t len,
  int flags
);

/*
vm_recvfrom
Receives data in user supplied buffer from source socket.
vm_bind call shall be called before vm_recvfrom.
This call is blocking call by default. To make it a non-blocking
call, set the input parameter flags value to MSG_DONTWAIT.

Input params:
  int -  handle/file descriptor to the
         underlying socket.
  const void *buf - Pointer to user data buffer
  size_t len - length of the user data buffer
  int flags -
              MSG_TRUNC - This flag will truncate the
                          incoming data to the user supplied
                          buffer length, in case user buffer
                          length is lesser than the length of
                          incoming data.
              MSG_DONTWAIT - This flag will not block vm_recv
                             function in case there is no data
                             in the underlying socket receive
                             queue.
  struct sockaddr *  src_addr - pointer to struct sockaddr variable.
                                       Address of source socket will be populated
                                       in this variable.
  socklen_t *  addrlen - Size of source address which is filled in variable
                                pointed to by struct sockaddr * in the above input
                                parameter.

Return value:
  int -
        Value > 0 indicates a number of bytes successfully received
        Value of 0 indicates an error either due to
            invalid bind address (if vm_bind is not called)
            any memory allocation failures during the operation
            invalid socket handle
            NULL user data buffer pointer.
*/
ssize_t vm_recvfrom
(
  int sockfd,
  void *buf,
  size_t len,
  int flags,
  struct sockaddr *src_addr,
  socklen_t *addrlen
);

void vm_socket_diagnostics(void);

#endif
