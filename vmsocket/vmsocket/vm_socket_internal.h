// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#ifndef _VM_SOCKET_INTERNAL_H_
#define _VM_SOCKET_INTERNAL_H_

#include "vm_osal.h"
#include "vm_socket.h"
#include "vm_list.h"

#define VM_SOCKET_MAX_FD 5 /*max number of fds supported*/

/*define the version*/
#define VM_SOCKET_PROTOCOL_VERSION 1

typedef struct __attribute__((__packed__)){
  uint8_t version;
  uint8_t type;
  uint8_t flags;
  uint8_t optlen;
  uint32_t size;
  uint32_t src_node_id;
  uint32_t src_port_id;
  uint32_t dst_node_id;
  uint32_t dst_port_id;
} vm_socket_header_type;

typedef struct
{
  unsigned int header_received;
  unsigned int total_pkt_len;
  unsigned int received_len;
  unsigned char *curr_pkt_ptr;
  vm_port_addr src_addr;
  vm_port_addr dest_addr;
} vm_socket_curr_rx_pkt_info_type;

void vm_socket_process_rx_buffer
(
  unsigned char *buffer,
  unsigned int   len,
  vm_socket_curr_rx_pkt_info_type *curr_rx_pkt_info
);

typedef struct
{
  void* buf;
  unsigned int len;
  vm_port_addr   src_addr;
  struct vm_list_item list_item;
}vm_socket_rx_q_element;

typedef struct
{
    int fd;
    vm_port_addr connected_addr;
    vm_port_addr bind_addr;  

    struct vm_list rx_q;
    unsigned int rx_q_count;
    VM_SOCKET_OSAL_MUTEX socket_rx_q_mutex;
    
    VM_SOCKET_OSAL_MUTEX socket_rx_wait_cond_mutex;
    VM_SOCKET_OSAL_SIG  socket_rx_wait_cond;

    struct vm_list_item list_item;

    unsigned int ref_count;
    VM_SOCKET_OSAL_MUTEX ref_count_mutex;


}vm_socket_type;

#endif
