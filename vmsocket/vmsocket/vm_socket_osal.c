// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#include "vm_socket_transport.h"

#ifdef __KERNEL__
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/slab.h>

void* vm_socket_osal_malloc(ssize_t x)
{
  return kzalloc(x, GFP_KERNEL);
}

void vm_socket_osal_free(void* x)
{
  kfree(x);
}

struct vmsocket_linux_isr_ctxt {
  void (*isr)(void *);
};

static irqreturn_t vmsocket_msgq_linux_isr(int irq, void *data)
{
  struct vmsocket_linux_isr_ctxt *ctxt = data;

  ctxt->isr(NULL);

  return IRQ_HANDLED;
}

#define MSGQ_TX_IDX 0
#define MSGQ_RX_IDX 1
#define VMSOCKET_MSGQ_LINUX_LABEL 3

int vm_socket_osal_get_msgq(uint64_t *send_cap, uint64_t *recv_cap,
                     void (*tx_isr)(void *data),
                     void (*rx_isr)(void *data))
{
  const char *compat = "qcom,gunyah-message-queue";
  struct vmsocket_linux_isr_ctxt *tx_ctxt;
  struct vmsocket_linux_isr_ctxt *rx_ctxt;
  struct device_node *tx_np = NULL;
  struct device_node *rx_np = NULL;
  struct device_node *np = NULL;
  int send_irq, recv_irq;
  u32 label;
  int ret;


  if (!send_cap || !recv_cap || !tx_isr || !rx_isr) {
    pr_err("invalid params\n");
    return -1;
  }

  while ((np = of_find_compatible_node(np, NULL, compat))) {
    bool is_recv = false;
    bool is_send = false;

    if (tx_np && rx_np)
      break;

    ret = of_property_read_u32(np, "qcom,label", &label);
    if (ret || label != VMSOCKET_MSGQ_LINUX_LABEL) {
      of_node_put(np);
      continue;
    }
    is_recv = of_property_read_bool(np, "is-receiver");
    if (is_recv && !rx_np) {
      rx_np = np;
      continue;
    }

    is_send = of_property_read_bool(np, "is-sender");
    if (is_send && !tx_np) {
      tx_np = np;
      continue;
    }

    of_node_put(np);
  }

  tx_ctxt = kzalloc(sizeof(*tx_ctxt), GFP_KERNEL);
  if (!tx_ctxt) {
    pr_err("tx_ctxt alloc failed\n");
    goto put_nodes;
  }
  tx_ctxt->isr = tx_isr;

  rx_ctxt = kzalloc(sizeof(*rx_ctxt), GFP_KERNEL);
  if (!rx_ctxt) {
    pr_err("rx_ctxt alloc failed\n");
    kfree(tx_ctxt);
    goto put_nodes;
  }
  rx_ctxt->isr = rx_isr;

  /* Get TX MSGQ information */
  send_irq = of_irq_get(tx_np, 0);
  if (send_irq <= 0) {
    pr_err("failed to parse send irq from node %d\n", send_irq);
    goto error;
  }
  ret = of_property_read_u64_index(tx_np, "reg", 0, send_cap);
  if (ret) {
    pr_err("failed to parse send cap id from node %d\n", ret);
    goto error;
  }

  /* Get RX MSGQ information */
  recv_irq = of_irq_get(rx_np, 0);
  if (recv_irq <= 0) {
    pr_err("failed to parse send irq from node %d\n", recv_irq);
    goto error;
  }
  ret = of_property_read_u64_index(rx_np, "reg", 0, recv_cap);
  if (ret) {
    pr_err("failed to parse send cap id from node %d\n", ret);
    goto error;
  }

  /* Request the interrupts */
  ret = request_irq(send_irq, vmsocket_msgq_linux_isr, 0,
                    "vmsocket_tx_irq", tx_ctxt);
  if (ret) {
    pr_err("failed to request tx isr %d\n", ret);
    goto error;
  }

  ret = request_irq(recv_irq, vmsocket_msgq_linux_isr, 0,
                    "vmsocket_rx_irq", rx_ctxt);
  if (ret) {
    pr_err("failed to request rx isr %d\n", ret);
    goto error;
  }

  of_node_put(tx_np);
  of_node_put(rx_np);
  return 0;

error:
  kfree(tx_ctxt);
  kfree(rx_ctxt);
put_nodes:
  of_node_put(tx_np);
  of_node_put(rx_np);

  return -1;
}

#else // ================= LINUX_KERNEL_SPACE_COMPILATION end ==============================

void* vm_socket_osal_malloc(ssize_t x)
{
  return malloc(x);
}

void vm_socket_osal_free(void* x)
{
  return free(x);
}

int vm_socket_osal_get_msgq(uint64_t *send_cap, uint64_t *recv_cap,
                     void (*tx_isr)(void *data),
                     void (*rx_isr)(void *data))
{
  return 0;
}
#endif //else
