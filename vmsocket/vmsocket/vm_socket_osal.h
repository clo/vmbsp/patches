// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#ifndef _VM_SOCKET_OSAL_H_
#define _VM_SOCKET_OSAL_H_

#ifdef __KERNEL__
#include <linux/kthread.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/wait.h>

#define VM_SOCKET_OSAL_PRINT_DEBUG pr_debug
#define VM_SOCKET_OSAL_PRINT_ERROR pr_err
#define VM_SOCKET_OSAL_PRINT_TX pr_debug
#define VM_SOCKET_OSAL_PRINT_RX pr_err

#else // __KERNEL__ end
#define _GNU_SOURCE
#include <stdio.h>    //std input output
#include <stdint.h>
#include <pthread.h> //thread related
#include "vm_list.h"
#include <malloc.h> //for malloc
#include <string.h> // memcpy
#include <unistd.h>
#include <fcntl.h>

/*output and logging*/
#define VM_SOCKET_OSAL_PRINT_DEBUG printf
#define VM_SOCKET_OSAL_PRINT_ERROR printf
#define VM_SOCKET_OSAL_PRINT_TX printf
#define VM_SOCKET_OSAL_PRINT_RX printf

#endif //else

/*general defines*/
#define VM_SOCKETS_OSAL_INVALID 0xFFFFFFFE
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define VM_SOCKET_OSAL_MEMCPY memcpy
#define VM_SOCKET_OSAL_MEMSET memset

void *vm_socket_osal_malloc(ssize_t x);
void vm_socket_osal_free(void *x);


int vm_socket_osal_get_msgq(uint64_t *send_cap, uint64_t *recv_cap,
                     void (*tx_isr)(void *data),
                     void (*rx_isr)(void *data));

#endif //_VM_SOCKET_OSAL_H_
