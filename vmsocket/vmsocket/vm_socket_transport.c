// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#include "vm_socket_transport.h"
#include "vm_socket_internal.h"

#ifdef VM_SOCKET_TRANSPORT_PIPE
#include "vm_socket_transport_pipe.h"
#elif VM_SOCKET_TRANSPORT_GUNYAH
#include "vm_socket_transport_gunyah.h"
#endif

unsigned int vm_socket_transport_send
(
  unsigned char* buf,
  unsigned int len
)
{
  unsigned int bytes_written = 0;

  #ifdef VM_SOCKET_TRANSPORT_PIPE
  bytes_written = vm_socket_transport_pipe_send(buf,len);
  #elif VM_SOCKET_TRANSPORT_GUNYAH
  bytes_written = vm_socket_transport_gunyah_send(buf,len);
  #endif

  return bytes_written;
}

void vm_socket_transport_init
(
  void
)
{
  #ifdef VM_SOCKET_TRANSPORT_PIPE
  vm_socket_transport_pipe_init();
  #elif VM_SOCKET_TRANSPORT_GUNYAH
  vm_socket_transport_gunyah_init();
  #endif

  return;
}

unsigned int vm_socket_get_transport_max_len
(
  void* dummy
)
{
  #ifdef VM_SOCKET_TRANSPORT_PIPE
  return vm_socket_transport_pipe_get_len();
  #elif VM_SOCKET_TRANSPORT_GUNYAH
  return vm_socket_transport_gunyah_get_len();
  #endif
}
