// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#ifndef _VM_SOCKET_TRANSPORT_H_
#define _VM_SOCKET_TRANSPORT_H_

#include "vm_socket_osal.h"

void vm_socket_transport_init
(
  void
);

unsigned int vm_socket_transport_send
(
  unsigned char* buf,
  unsigned int len
);

unsigned int vm_socket_get_transport_max_len
(
  void* dummy
);

#endif
