// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#include <guest_interface.h>
#include <linux/err.h>
#include <linux/errno.h>
#include <linux/gunyah/gh_msgq.h>
#include "vm_socket_internal.h"
#include "vm_osal.h"
#include "vm_socket_osal.h"
#include "vm_socket_transport_gunyah.h"

#define MAX_VM_SOCKET_TRANSPORT_GUNYAH_PIPE_LEN 0xF0
#define VMSOCKET_MSGQ_LINUX_LABEL 3

static void *vmsocket_gh_msgq_hdl;

typedef struct
{
  /*rx thread related*/
  VM_SOCKET_OSAL_THREAD rx_thread_handle;

  /*max pipe length*/
  unsigned int pipe_max_len;
} vm_socket_transport_gunyah_info_type;

static vm_socket_transport_gunyah_info_type vm_socket_transport_gunyah_info;

unsigned int vm_socket_transport_gunyah_send
(
  unsigned char* buf,
  unsigned int len
)
{
  ssize_t bytes_written = 0;
  int iteration = 0;
  size_t tx_len;
  int ret;

  //try to send to the msgq
  while(len > 0)
  {
    tx_len = (len > GH_MSGQ_MAX_MSG_SIZE_BYTES ? GH_MSGQ_MAX_MSG_SIZE_BYTES : len);
    ret = gh_msgq_send(vmsocket_gh_msgq_hdl, buf, tx_len, GH_MSGQ_TX_PUSH);
    if(ret) {
      VM_SOCKET_OSAL_PRINT_TX("%s: gh_msgq_send failed: %d\n", ret);
      return ret;
    }
    len -= tx_len;
    bytes_written += tx_len;
    buf = buf + tx_len;
    //VM_SOCKET_OSAL_PRINT_TX("TX: write iteration %d, bytes_written = %lu, remaining = %d\n",iteration, bytes_written, len);
    iteration++;
  }

  VM_SOCKET_OSAL_PRINT_TX("TX: Wrote %lu bytes to the pipe\n", bytes_written);

  return bytes_written;
}

static void* _vm_socket_transport_gunyah_rx_thread(void* ctxt)
{
  size_t bytes_read = 0;
  unsigned char* rx_working_buffer;
  vm_socket_curr_rx_pkt_info_type* curr_rx_pkt_info;
  int rc;

  //Allocate buffer of max pipe rx size as a working buffer
  rx_working_buffer = (unsigned char*)vm_socket_osal_malloc(vm_socket_transport_gunyah_info.pipe_max_len);
  if(!rx_working_buffer)
  {
    VM_SOCKET_OSAL_PRINT_RX("RX: Could not allocate memory for rx\n");
    return NULL;
  }

  //Allocate
  curr_rx_pkt_info = (vm_socket_curr_rx_pkt_info_type *)vm_socket_osal_malloc(sizeof(vm_socket_curr_rx_pkt_info_type));
  if(!curr_rx_pkt_info)
  {
    VM_SOCKET_OSAL_PRINT_RX("RX: Could not allocate memory for curr rx pkt info\n");
    return NULL;
  }

  /* TODO:wait until everything is initialized */
  while(1)
  {
	//wait on msgq receive
	rc = gh_msgq_recv(vmsocket_gh_msgq_hdl, rx_working_buffer,
										vm_socket_transport_gunyah_info.pipe_max_len, &bytes_read,
										GH_MSGQ_TX_PUSH);
	if (rc)
			continue;

	//VM_SOCKET_OSAL_PRINT_RX("RX: Received %d bytes on rx pipe..\n", bytes_read);
	//give this buffer to the vm_socket_layer
	vm_socket_process_rx_buffer(rx_working_buffer, bytes_read, curr_rx_pkt_info);
  }

  return NULL;
}

void vm_socket_transport_gunyah_init(void)
{
  int ret;

  //initialize the max pipe length
  vm_socket_transport_gunyah_info.pipe_max_len = MAX_VM_SOCKET_TRANSPORT_GUNYAH_PIPE_LEN;

  /*Create a receiving thread*/
  //VM_SOCKET_OSAL_PRINT_DEBUG("DEBUG: Creating a Gunyah RX thread..\n");
  vm_osal_thread_create(&vm_socket_transport_gunyah_info.rx_thread_handle, _vm_socket_transport_gunyah_rx_thread, NULL, "vm_socket_rx");

  //VM_SOCKET_OSAL_PRINT_DEBUG("DEBUG: Inside gunyah tranport init\n");
  vmsocket_gh_msgq_hdl = gh_msgq_register(VMSOCKET_MSGQ_LINUX_LABEL);
  if (IS_ERR(vmsocket_gh_msgq_hdl)) {
    ret = PTR_ERR(vmsocket_gh_msgq_hdl);
    if (ret != -EPROBE_DEFER)
      pr_err("%s: Message queue registration failed: %d\n", __func__, ret);
  }
  else
    VM_SOCKET_OSAL_PRINT_DEBUG("%s: DEBUG: msgq %d registered\n", __func__, VMSOCKET_MSGQ_LINUX_LABEL);

  return;
}

unsigned int vm_socket_transport_gunyah_get_len
(
  void
)
{
  return vm_socket_transport_gunyah_info.pipe_max_len;
}
