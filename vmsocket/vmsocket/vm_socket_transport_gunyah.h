// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#ifndef _VM_SOCKET_GUNYAH_H_
#define _VM_SOCKET_GUNYAH_H_

unsigned int vm_socket_transport_gunyah_send(unsigned char* buf, unsigned int len);
void vm_socket_transport_gunyah_init(void);
unsigned int vm_socket_transport_gunyah_get_len(void);

#endif
