// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#include "vm_socket_transport.h"
#include "vm_socket_internal.h"

#define MAX_VM_SOCKET_TRANSPORT_PIPE_LEN 0xF0
#define VM_SOCKET_TRANSPORT_PIPE_BLOCKING 1

/*for active sockets book-keeping*/
typedef struct
{
  int pipefd[2];
  int pipe_write_fd;
  int pipe_read_fd;
  unsigned int pipe_read_max_size;
  unsigned int pipe_write_max_size;

  unsigned int pipe_max_len;


}vm_socket_transport_pipe_info_type;

static vm_socket_transport_pipe_info_type vm_socket_transport_pipe_info;

unsigned int vm_socket_transport_pipe_send
(
  unsigned char* buf,
  unsigned int len
)
{
  ssize_t bytes_written = 0;
  int iteration = 0;

  //try to send to the pipe
  while(bytes_written != len && len != 0)
  {
      bytes_written = write(vm_socket_transport_pipe_info.pipe_write_fd,
             buf,
             len);

      VM_SOCKET_OSAL_PRINT_TX("TX: write iteration %d, bytes_written = %lu, len = %d\n",iteration,bytes_written,len);
      iteration++;
      //vm_osal_thread_sleep(1);
  }

  VM_SOCKET_OSAL_PRINT_TX("TX: Wrote %lu bytes to the pipe\n",bytes_written);

   //block here if pipe full

   //send when pipe non-full

   //return the number of bytes written

   return bytes_written;
}


static int _vm_socket_transport_pipe_rx_thread
(
  void* dummy
)
{
  unsigned int bytes_read = 0;

  //Allocate buffer of max pipe rx size as a working
  //buffer
  unsigned char* rx_working_buffer;

  rx_working_buffer = (unsigned char*)vm_socket_osal_malloc(vm_socket_transport_pipe_info.pipe_max_len);
  if(! rx_working_buffer)
  {
    VM_SOCKET_OSAL_PRINT_RX("RX: Could not allocate memory for rx\n");
    return 0;
  }

  //Allocate
  vm_socket_curr_rx_pkt_info_type* curr_rx_pkt_info = (vm_socket_curr_rx_pkt_info_type *)
                                                      vm_socket_osal_malloc(sizeof(vm_socket_curr_rx_pkt_info_type));

  if(!curr_rx_pkt_info)
  {
    VM_SOCKET_OSAL_PRINT_RX("RX: Could not allocate memory for curr rx pkt info\n");
    return 0;
  }

  VM_SOCKET_OSAL_PRINT_RX("RX: Created RX thread..\n");

  while(1)
  {

      //wait on pipe receive
      bytes_read = read(vm_socket_transport_pipe_info.pipe_read_fd,
                        rx_working_buffer,
                        vm_socket_transport_pipe_info.pipe_max_len);
      VM_SOCKET_OSAL_PRINT_RX("RX: Received %d bytes on rx pipe..\n", bytes_read);

      if(bytes_read <= 0 || bytes_read > vm_socket_transport_pipe_info.pipe_max_len )
      {
        VM_SOCKET_OSAL_PRINT_ERROR("ERROR: in reading bytes. Got %d bytes from pipe. Discarding.\n",
                             bytes_read);

        /*go back to the start of the while loop*/
        continue;
      }

      //give this buffer to the vm_socket_layer
      vm_socket_process_rx_buffer(rx_working_buffer, bytes_read, curr_rx_pkt_info);

      //vm_osal_thread_sleep(1);
  }

  return 0;

}

void vm_socket_transport_pipe_init
(
  void
)
{
  int ret;
  int oldflags = 0;

  VM_SOCKET_OSAL_PRINT_DEBUG("DEBUG: Inside tranport init\n");

  //create pipe
  if(pipe2(vm_socket_transport_pipe_info.pipefd, O_DIRECT) < 0)
  {
     VM_SOCKET_OSAL_PRINT_ERROR("ERROR: Pipe creation failed\n");
  }

  //Set the pipe max length
  vm_socket_transport_pipe_info.pipe_max_len = MAX_VM_SOCKET_TRANSPORT_PIPE_LEN;

  //set the max size for write pipe
  fcntl(vm_socket_transport_pipe_info.pipe_write_fd,
        F_SETPIPE_SZ,
        vm_socket_transport_pipe_info.pipe_max_len);

  //Set the pipe to non-blocking if needed
  if(!VM_SOCKET_TRANSPORT_PIPE_BLOCKING)
  {
    oldflags = fcntl (vm_socket_transport_pipe_info.pipe_write_fd, F_GETFL, 0);
    oldflags |= O_NONBLOCK;
    VM_SOCKET_OSAL_PRINT_DEBUG("DEBUG: write pipe set to non_blocking\n");
  }

  //once the pipes are created, then assign the read and write pipe
  vm_socket_transport_pipe_info.pipe_read_fd = vm_socket_transport_pipe_info.pipefd[0];
  vm_socket_transport_pipe_info.pipe_write_fd = vm_socket_transport_pipe_info.pipefd[1];

  /*Create a receiving thread*/
  VM_SOCKET_OSAL_PRINT_DEBUG("DEBUG: Creating a RX thread..\n");
  ret = vm_osal_thread_create(&_vm_socket_transport_pipe_rx_thread,
                              NULL,
                             "Transport_Linux_Pipe_Rx_Thread");

  if(ret < 0)
  {
    return;
  }

  return;
}


unsigned int vm_socket_transport_pipe_get_len
(
  void
)
{
  return vm_socket_transport_pipe_info.pipe_max_len;
}
