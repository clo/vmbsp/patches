// Copyright (c) 2021 The Linux Foundation. All rights reserved. 
// 
// SPDX-License-Identifier: BSD-3-Clause
#ifndef _VM_SOCKET_TRANSPORT_PIPE_H_
#define _VM_SOCKET_TRANSPORT_PIPE_H_

#include "vm_socket_osal.h"

#define VM_SOCKET_MAX_PIPE_LEN 0x400
#define VM_SOCKET_TRANSPORT_PIPE_BLOCKING 0

void vm_socket_transport_pipe_init
(
  void
);

unsigned int vm_socket_transport_pipe_send
(
  unsigned char* buf,
  unsigned int len
);

unsigned int vm_socket_transport_pipe_get_len
(
  void
);

#endif
